<?php

if(!defined('STDIN'))
    define('STDIN',fopen("php://stdin","r"));

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth'), function() {
    Route::get('logout', ['as' => 'site.logout', 'uses' => 'Frontend\HomeController@logout']);
});

Route::group(array('namespace' => 'Frontend', 'before'=>'check_promo_period'), function() {

    /*
     * New for McD Route 1
     * Filter untuk development
     */
    Route::group(array('prefix' => 'route1', 'before' => '', 'namespace' => 'Facebook'), function(){
        Route::group(array('prefix' => 'register', 'before' => 'validcode'), function(){
            Route::get('/', ['as' => 'route1.register', 'uses' => 'RegisterController@index']);
            Route::post('/', ['as' => 'route1.register', 'before' => 'csrf.ajax', 'uses' => 'RegisterController@store']);
        });

        Route::group(array('before' => 'auth'), function(){
            Route::get('friend', ['as' => 'route1.friend.select', 'uses' => 'FriendController@index']);
            Route::post('friend', ['as' => 'route1.friend.select', 'before' => 'csrf', 'uses' => 'FriendController@store']);
        });
    });

    // New for McD Route 2
    Route::group(array('prefix' => '', 'namespace' => 'Email'), function(){
        Route::get('/', ['as' => 'route2.home', 'uses' => 'InvitationController@index']);
        Route::post('/invite/check', ['as' => 'invite.check', 'before' => 'csrf', 'uses' => 'InvitationController@check']);

        Route::group(array('prefix' => 'register', 'before' => 'validcode'), function(){
            Route::get('/', ['as' => 'route2.register', 'uses' => 'RegisterController@viaEmail']);
            Route::post('/', ['as' => 'route2.register', 'before' => 'csrf', 'uses' => 'RegisterController@postEmail']);
        });

        Route::group(array('before' => 'auth'), function(){
            Route::get('friend', ['as' => 'route2.friend.select', 'uses' => 'FriendController@writeEmail']);
            Route::post('friend', ['as' => 'route2.friend.select', 'before' => 'csrf', 'uses' => 'FriendController@sendEmail']);

            Route::get('ecard', ['as' => 'route2.ecard', 'uses' => 'EcardController@write']);
            Route::post('ecard', ['as' => 'route2.ecard', 'before' => 'csrf', 'uses' => 'EcardController@store']);

            Route::get('finish', ['as' => 'route2.finish', 'uses' => 'FriendController@finish']);

            // redeemer
            Route::post('/shared', ['as' => 'shared', 'before' => 'csrf.ajax', 'uses' => 'RedeemController@shared']);
        });

        Route::get('/redeem/{link}', ['as' => 'route2.redeem.link', 'uses' => 'RedeemController@link']);
        // Route::post('/fb-connect', ['as' => 'route2.fb.connect', 'before' => 'csrf.ajax', 'uses' => 'RegisterController@fbConnect']);

        // connect with fb route 1
        Route::post('/redeemer-login', ['as' => 'route1.redeem.login', 'before' => 'csrf.ajax', 'uses' => 'RedeemController@login']);

        Route::get('/verify', ['as' => 'verify', 'uses' => 'RedeemController@verify']);
        Route::get('/thanks', ['as' => 'thanks', 'uses' => 'RedeemController@thanks']);

        Route::get('preview/invitation/{code}', ['uses' => 'FriendController@inviteFriendPreview']);
        Route::get('preview/voucher/{code}', ['uses' => 'RedeemController@preview']);
    });

    Route::group(array('prefix' => 'mcd', 'before' => 'dev'), function(){
        Route::controller('/', 'McdFrontendController');
    });

});

Route::group(array('namespace' => '', 'prefix' => 'test'), function() {
    Route::any('{something}', ['uses' => 'TestController@index']);
});

Route::group(array('namespace' => 'Admin', 'prefix' => 'paladin'), function() {

    Route::get('login', ['as' => 'admin.login', 'uses' => 'SiteController@getLogin']);
    Route::post('login', ['as' => 'admin.login.do', 'uses' => 'SiteController@postLogin']);

    Route::group(array('before' => 'admin'), function(){
        Route::get('logout', ['as' => 'admin.logout', 'uses' => 'SiteController@logout']);

        Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index']);

        Route::get('voucher', ['as' => 'admin.voucher', 'uses' => 'VoucherController@index']);
        Route::get('voucher/collected', ['as' => 'admin.voucher.collected', 'uses' => 'VoucherController@collected']);
        Route::get('voucher/redeemed', ['as' => 'admin.voucher.redeemed', 'uses' => 'VoucherController@redeemed']);
        Route::post('voucher/redeemed', ['as' => 'admin.voucher.redeemed.collect', 'uses' => 'VoucherController@collect']);
        // Route::post('voucher/collect', ['as' => 'admin.voucher.collect', 'uses' => 'VoucherController@collect']);
        Route::get('voucher/generated', ['as' => 'admin.voucher.generated', 'uses' => 'VoucherController@generated']);
        Route::get('voucher/generated/export', ['as' => 'admin.voucher.generated.export', 'uses' => 'VoucherController@generatedExport']);

        Route::get('unlock-code/index', ['as' => 'admin.code', 'uses' => 'UnlockCodeController@index']);
        Route::get('unlock-code/used', ['as' => 'admin.code.used', 'uses' => 'UnlockCodeController@used']);
        Route::put('unlock-code/release/{id}', ['as' => 'admin.code.release', 'before' => 'csrf', 'uses' => 'UnlockCodeController@release']);
        Route::get('unlock-code/import', ['as' => 'admin.code.import', 'uses' => 'UnlockCodeController@getImport']);
        Route::post('unlock-code/import', ['as' => 'admin.code.import.do', 'uses' => 'UnlockCodeController@postImport']);

        Route::get('voucher/import', ['as' => 'admin.voucher.import', 'uses' => 'VoucherController@getImport']);
        Route::post('voucher/import', ['as' => 'admin.voucher.import.do', 'uses' => 'VoucherController@postImport']);

        Route::get('user', ['as' => 'admin.user', 'uses' => 'UserController@index']);
        Route::get('user/add', ['as' => 'admin.user.add', 'uses' => 'UserController@add']);
        Route::post('user/store', ['as' => 'admin.user.store', 'uses' => 'UserController@store']);
        Route::post('user/remove', ['as' => 'admin.user.remove', 'uses' => 'UserController@remove']);

        Route::get('ecard', ['as' => 'admin.ecard', 'uses' => 'EcardController@index']);
        Route::get('ecard/export', ['as' => 'admin.ecard.export', 'uses' => 'EcardController@export']);

        Route::get('setting', ['as' => 'admin.setting', 'uses' => 'SettingController@index']);
        Route::post('setting', ['as' => 'admin.setting.save', 'uses' => 'SettingController@save']);
        Route::get('restart', ['as' => 'admin.restart', 'uses' => 'SettingController@getRestart']);
        Route::post('restart', ['as' => 'admin.restart.do', 'uses' => 'SettingController@postRestart']);

        Route::group(array('prefix' => 'me'), function(){
            Route::get('password', ['as' => 'admin.password', 'uses' => 'MeController@getPassword']);
            Route::post('password', ['as' => 'admin.password.save', 'uses' => 'MeController@postPassword']);
        });
    });
});
