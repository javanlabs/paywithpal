<?php

use Illuminate\Console\Command;
use Metro\Voucher\VoucherModel;

class GenerateVoucherCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'app:generate-voucher';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate vouchers.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$vouchers = [
            ['name' => 'Beef Burger & McFloat', 'total' => 3000, 'type' => 1],
            ['name' => 'Chicken Burger', 'total' => 9850, 'type' => 2],
            ['name' => 'Spicy Chicken Bites', 'total' => 39400, 'type' => 3],
            ['name' => 'Sundae Choco', 'total' => 59100, 'type' => 4],
            ['name' => 'Coke Float', 'total' => 88650, 'type' => 5],
        ];

        $this->info('Truncate vouchers table');
        DB::table('vouchers')->truncate();

		$this->info('Truncate redeems table');
        DB::table('redeems')->truncate();

        $counter = 0;
        foreach($vouchers as $voucher){
            $this->info("Generate vouchers: {$voucher['name']}");

            foreach(range(1, $voucher['total']) as $i){
                DB::table('vouchers')->insert([
//                    'code' => Str::quickRandom(14),
                    // default lenght 14
                    'code' => VoucherModel::getCode(14),
                    'discount' => 0,
                    'label' => $voucher['name'],
                    'type' => $voucher['type']
                ]);

                $counter++;
            }
        }

        $this->info("Total vouchers: {$counter}");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
