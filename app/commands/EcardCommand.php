<?php

use Illuminate\Console\Command;
use Metro\Ecard;

class EcardCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'app:send-ecard';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send ecard to email.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();

        Theme::init('mcd2');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if ($this->input->getOption('test')) {
            $ecards = Metro\Ecard\EcardModel::join('users', 'ecards.user_id', '=', 'users.id')
                        ->select('users.name as sender', 'ecards.id', 'ecards.friend_name', 'ecards.friend_email', 'ecards.message', 'ecards.image_path')
                        ->where('friend_email', '=', 'uyab.exe@gmail.com')->get();

        } else {
            $ecards = Metro\Ecard\EcardModel::join('users', 'ecards.user_id', '=', 'users.id')
                        ->select('users.name as sender', 'ecards.id', 'ecards.friend_name', 'ecards.friend_email', 'ecards.message', 'ecards.image_path')
                        ->where('is_sent', 0)->orderByRaw('RAND()')->take(500)->get();
        }

        $this->info('Mengambil  ' . count($ecards) . ' data ecard dari database.');

        $counter = 0;
        foreach ($ecards as $ecard) {
            $this->info('Mengirim ecard ke ' . $ecard->friend_email);

            $message = $ecard->message;
            if(!$message) {
                $message = "Selamat Hari Raya Idul Fitri 1436 H \nTaqabbalallahu Minna Wa Minkum";
            }

            try {
                $data = [
                    'message' => $message,
                    'src' => $this->generateFramedEcardUrl($ecard)
                ];

                Mail::send('emails.ecard', ['data' => $data], function ($message) use ($ecard) {
                    $message->to($ecard->friend_email, $ecard->friend_name)->subject('Kartu Ucapan Dari ' . $ecard->sender);
                });

                if( ! $this->input->getOption('test')) {
                    DB::table('ecards')->where('id', $ecard->id)->update(['is_sent' => 1]);
                }

                $counter++;

            } catch (Exception $e) {
                $this->error($e->getMessage());
            }
        }

        $this->info('Berhasil mengirim ecard sebanyak ' . $counter . '/' . count($ecards));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(

        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('test', null, \Symfony\Component\Console\Input\InputOption::VALUE_NONE, 'Only send to tester recipient'),
        );
    }

    protected function generateFramedEcardUrl($ecard)
    {
        $framedImage = new \Metro\Ecard\EcardImageWithFrameGenerator($ecard->sender, $ecard->friend_name, $ecard->message, $ecard->image_path);
        return asset('ecards/generate/' . $framedImage->getFilename());
    }
}
