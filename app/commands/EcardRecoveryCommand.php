<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EcardRecoveryCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'app:ecard-recovery';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Ecard Recovery.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$ecards = DB::table('ecards')->whereIn('image_path', [
            'ecard1.jpg', 'ecard2.jpg', 'ecard3.jpg', 'ecard4.jpg'
        ])->get();

        $this->info('Mengambil daftar ecard');

        $counter = 0;
        foreach($ecards as $ecard){

            $ecardImage = new \Metro\Ecard\EcardImageGenerator($ecard->friend_name, $ecard->image_path, uniqid());

            DB::table('ecards')->where('id', $ecard->id)->update(['image_path' => $ecardImage->getFilename()]);

            $counter++;
        }

        $this->info('Mengupdate '.$counter.' ecard');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
