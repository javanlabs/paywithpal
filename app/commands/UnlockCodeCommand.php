<?php

use Illuminate\Console\Command;
use Metro\Invitation\InvitationRepositoryInterface;

class UnlockCodeCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'app:insert-unlock-code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert unlock code.';
    /**
     * @var InvitationRepositoryInterface
     */
    private $repo;

    /**
     * Create a new command instance.
     *
     */
    public function __construct(InvitationRepositoryInterface $repo)
    {
        $this->repo = $repo;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $file = storage_path('app/unlock-code.csv');
        if(!\Illuminate\Support\Facades\File::exists($file))
        {
            $this->error('Silakan upload file unlock code dalam format csv melalui halaman admin terlebih dahulu');
            return false;
        }

        \Illuminate\Support\Facades\DB::table('invitations')->truncate();
        $i = 1;

        Excel::filter('chunk')->load($file)->chunk(1000, function($results) use (&$i)
        {
            $report = $this->repo->bulkInsert($results->toArray());
            $this->info($i++ . '. Code inserted: ' . $report[0]);
            if($report[1])
            {
                $this->error('Found ' . $report[1] . 'duplicate code: ' . implode(' ', $report[2]));
            }
            sleep(1);
        });

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
