<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Metro Pay With Pal Voucher</title>
<style>
a{text-decoration: none; color: #000}
</style>
</head>
<body style="margin:0; padding: 0;font-family: 'Helvetica Neue', Arial, sans-serif;background-color:#eee">
    <div style="background: #ccc url('{{ theme_asset('skins/tile.png') }}') repeat; width:600px; margin:0 auto; text-align: center">
        <div style="padding: 20px 0">
            <img src="{{ theme_asset('skins/print-voucher.png') }}" alt=""/>
            @if(isset($message))
            <img src="<?php echo $message->embed($file); ?>">
            @else
            <img src="{{ asset('vouchers/facebook/eyJpdiI6ImJFRWgyY2JOT2tcL2dvb2ozeERkOFJnPT0iLCJ2YWx1ZSI6ImNkdlJqRlFhVGZTVGhQRnRtdHJMTGc9PSIsIm1hYyI6IjQ0ZTJkNTJhYzdkZWJlM2RlODQ3ZmIwMzZiZDU3M2NmNDE2MWEwMGNkN2U1Y2Q5ZTY1NDM3YWNkNjAwMjBiOGEifQ==.png') }}" alt=""/>
            @endif
        </div>

        <div style="text-align: center; background-color: #ffffff; padding: 20px 0">
            {{ $footer }}
        </div>

    </div>

</body>
</html>
