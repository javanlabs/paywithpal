{{ Form::open(['url' => 'test/email']) }}
    <label for="">Driver {{ Form::text('driver', Config::get('mail.driver'), Input::old('driver')) }} </label>
    <br><label for="">Host {{ Form::text('host', Config::get('mail.host'), Input::old('host')) }} </label>
    <br><label for="">Port {{ Form::text('port', Config::get('mail.port'), Input::old('port')) }} </label>
    <br><label for="">Encryption {{ Form::text('encryption', Config::get('mail.encryption'), Input::old('encryption')) }} </label>
    <br><label for="">Username {{ Form::text('username', Config::get('mail.username'), Input::old('username')) }} </label>
    <br><label for="">Password {{ Form::input('password', 'password') }} </label>
    <br>
    <br><label for="">To {{ Form::text('to') }} </label>
    <br><label for="">Message {{ Form::text('message', 'test email') }} </label>
    <br>{{ Form::submit('Test Email') }}
{{ Form::close() }}