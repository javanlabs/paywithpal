<?php

return array(
    'valid_in'      => "SELURUH OUTLET METRO \nDI SELURUH INDONESIA",
    'valid_until'   => '31 AGUSTUS 2014',
    'url'           => 'paywithpal.metroindonesia.com',
    'scores'        => array(
        1   => '< 100',
        2   => '101 - 300',
        3   => '301 - 500',
        4   => '501 - 700',
        5   => '> 700',
    ),
    'messages'  => array(
        'facebook'  => array(
            'Test kedekatan lo dan teman lo disini dan dapatkan voucher belanja! http://on.fb.me/1yxBslB',
            'Teman lo bisa ditukarkan jadi voucher belanja lhoo, coba disini! http://on.fb.me/1yxBslB',
            'Lo bisa juga dapet voucher belanja dengan mengukur kedekatan lo dan teman lo disini! http://on.fb.me/1yxBslB'
        ),
        0   => array(
            'Boro-boro nge-like. User name loe dia kagak tau. Foto-foto loe dia kagak peduli. Bahkan muke loe aja dia ngak ngeh!',
            'eh, sebaiknya temen loe dikasih tau gunanya tombol like deh! di klik doang bisa buat dapet diskon sekarang. Coba lagi sama temen yang lain yah!',
            'Asli ya kalian berdua itu kompaaaak banget. Kalian sekompak sandal jepit. Tapi sayangnya kiri semua! Klik teman yang lain deh…',
            'Kalo telapak tangan kalian berdua dibaca secara intensif oleh peramal terkenal, hasilnya sama! Anda belom beruntung!'
        ),
        100   => array(
            'Beneran nggak percuma loe berteman dengan {name}, karena ternyata jumlah like dia ke FB loe bisa dituker dengan voucher sebesar…',
            '3 hal di dunia ini yang nggak bisa dihitung; jumlah bintang di langit, ikan di laut dan jumlah like dia di FB kamu! Makanya kamu dapet voucher!',
            'Jumlah like and komen dia di FB loe itu kayak rambut and kuku manusia!. Meski dipotong bakal tumbuh terus menerus!',
            'Karena persahabatan kalian mirip kacang and kulitnya, mirip botol dan tutupnya, mirip sendok dan garpunya, maka kamu dapet…'
        ),
        200   => array(
            'Ejieeee, setelah dihitung-hitung, loe dan {name}.sohib banget nih kayak gula and semut,. Dari dia, loe bakal dapet voucher…',
            'Kalo kopi and kafein sepaket dan sepatu dan kaos kaki itu sepasang, maka loe dan dia adalah paket sohib yang bakal menghasilkan…',
            'Pantesan dia sering nggak ada di dunia nyata, orang dia lebih sering hadir di FB page kamu untuk nge-like semua status kamu. Dari dia, kamu berhak dapet voucher …',
            'Kamu pernah nggak nyobain ngitung jumlah wijen di sebutir onde-onde? Nah itu masih kalah sama jumlah like dia and komen dia ke FB kamu! Congrats ya!'
        ),
        300   => array(
            'Karena jumlah like-nya melebihi jumlah bintang di langit dan kalo comment lebih seru dari komentator bola, kamu dapet voucher…',
            'Bersatu kita teguh, berdua dengan sohib tersahih loe, loe dapet sahabat baik plus bonus voucher…. Seneng kan loe?',
            'Apaaa? Jumlah like dia ke kamu sebanyak pasir di laut dan sebanyak ikan di pasar?? Fine!!! Karena dia, kamu bakal dapet…',
            'Jumlah penduduk Jakarta dikali 8 aja masih kurang dibanding jumlah like and comment dia di FB page kamu, hazeg! Kamu dapet…'
        ),
    )
);