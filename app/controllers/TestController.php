<?php

class TestController extends \Frontend\FrontendController{

    public function index($something)
    {
        $method = $something;
        return $this->{$method}();
    }

    public function voucherPrint()
    {
        $imageGenerator = App::make('voucherGenerator');
        $param = [
            'user_name'     => $this->authUser()->name,
            'user_avatar'   => $this->authUser()->avatar,
            'friend_avatar' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAMgAyAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+W/hSqpcXUzLubzI46+nvCejLPDCrL8v8TV8x/DFVgs7qZvvfaF2q3+7Xqmm/GvVtBhVbXR7S6+b+Jmr5PMKUq1T3T7jK69PD0Y8x9RaPoNiln5kG1pFX5d0lVfEGkQy2cjLtlX+8u2vMfBv7Rl5qP2GzuNP0+2mupljaJZG3R/NXSePPjD/AMI9Ha2ssdmq3Ct/rWavAlhpRlyyPpYYqMo+0XwnC+JLX7PcfuN3y/3a4Px4q6v4B1qF4/8AVwtcR/7LL81U9U+OepXmoSRyaTZxQqzKs/mN8y1HJr3/AAkfhHUFZUg86G4j/dtu2/LXrYShOjKPOeHjsTSrxlGB4DuopoRsD5aK+vPgz1r4e6NDFbyWc8zQXkzLNDA33pI2X5Wr0az+E9w+5n+2LGq/e8ta8jj16Ofwnot9a/LqWmt9luv7zL/C3/fNfUXwx8eTatodvIrR/NGv+s/ir5bFSqRlzH22BhRqR5TznS/AH9l+JrG48y5ZYbiOT5o/lrpPit4Z/tu401pWlg8tW+6v97a1ddqHi21l1TbPJbWenwybZGb5WmZv7v8A8VU3xQ8QaP8AYY2sNQtlvo1WSNZ2Xayr/DXmynUlOMj1vZ04wlGJ8/33wvZoWZJLtvm/55rU1v4ej0TwveefNJFHGrbmlVV27vlX/vpq9cuvFay+GVvIFTbt3NE33l/2a898Sazat4T8zV13QzMt1Iv/AEzj+Zf/AB7bXZRq1KkuU8zE0KVOJ8/vpCxOyOJVdTtYbeh70VjXdzPe3U1wRzK7SH8Tmivq1H+8fF81L+U09N1f+y7e8j8vd9oVfm/usrV794J1mGy0mGGCTyreRt27+6rfMy184yV2ngHxLGitpt6vm2si7fmb7tefjKHNHmPSy7Eypy5T3rxh4c8B+Pmt7rxN5Vn5cPkqzXjQ/Kv3flVv4a5W4+GXwt0m4ja11D+1brb8rT6gzbY/7q1zNn4S02W6muLOxgvrjbtaDVJmk/75b+Gq+reHI7iz8m48I6bpC/8APeK6kkZv92vKjH3eWMj6SUlL3pRXMehapr1jdfubPbF/yxaL+9/drzH4xeKP+JpdaGi7oY7W3hX/AKZ/N5jf+y1qeE00/S9QZpWl8mGPzJpZ5N22Na8t1zVpPEGuX2pSrta6maTb/dX+Ff8Avmu7B4aMZcx4mPxUpRjEo7KKfRXtnzo2r/hb/kJR/wDXSiisa/8ADOnDfxYn0Vo8SfbrT5F5t+eOtX/GsSf2dD8i/lRRXyn2kfcfYZ5tqkSf8I34r+ReLNu3+7XjqUUV9Jhf4Z8fjf4gUUUV3HnH/9k=',
            'name'          => 'Suprapto Effendi',
            'discount'      => Input::get('discount', 100),
            'code'          => 'ABCDEFEGH',
            'level'         => 1
        ];

        $image = $imageGenerator->forPrint($param);

        header('Content-Type: image/png');
        echo $image->encode('png');

    }

    public function voucherFacebook()
    {
        $imageGenerator = App::make('voucherGenerator');
        $param = [
            'user_name'     => $this->authUser()->name,
//            'user_avatar'   => $this->authUser()->avatar,
            'friend_name'   => 'Suprapto Effendi',
            'name'          => 'Suprapto Effendi',
//            'friend_avatar' => 'http://ngomik.com/img/noimage/200/200',
            'discount'      => Input::get('discount', 100),
            'code'          => 'ABCDEFEGH',
            'message'       => "Lorem ipsum dirgahayu Indonesia"
        ];
        $image = $imageGenerator->forFacebook($param);

        header('Content-Type: image/png');
        echo $image->encode('png');
    }

    public function voucherEmail()
    {
        $footer = \Setting::get('email.footer');
        return View::make('emails.vouchers.redeem', compact('footer'));
    }

    public function barcode()
    {
        echo DNS1D::getBarcodeHTML("OSV7m0xPOD2rDE", "C39", 1);
        echo DNS1D::getBarcodeHTML("OSV7m0xPOD2rDE", "C93", 1);
        echo DNS1D::getBarcodeHTML("OSV7m0xPOD2rDE", "C128", 1);
        echo DNS1D::getBarcodeHTML("OSV7m0xPOD2rDE", "C128B", 1);
    }

    public function feed()
    {
        $helper = new \Metro\FacebookHelper();
        d($helper->getFriendsScore($this->authUser()->facebook_access_token));
    }

    public function post()
    {
        $helper = new \Metro\FacebookHelper();
        $helper->postPhoto($this->authUser()->facebook_access_token, base_path('assets/img/voucher-facebook-template.png'), 'sample message', 'AaJ0P96mUvgzc3c24FZQZeIggIoGNByrpB7IJAxmm8pjRdXp96NbbXtolmekoToOUZFfU06voSKmZC5dsGimws21bVyAfIwixmahGgtxo35Ytw');
    }

    public function email()
    {
        if($_POST)
        {
            $email = Input::get('to');
            $subject = 'Email Test from Pay With Pal';

            //override mail config
            Config::set('mail.driver', Input::get('driver'));
            Config::set('mail.host', Input::get('host'));
            Config::set('mail.port', Input::get('port'));
            Config::set('mail.encryption', Input::get('encryption'));
            Config::set('mail.username', Input::get('username'));
            Config::set('mail.password', Input::get('password'));

            Mail::send('emails.test', [], function($message) use ($email, $subject)
            {
                $message->to($email)->subject($subject);
            });
        }

        return View::make('tests.email');
    }

    public function ecard()
    {
        $message = "Dear gembul,

Enjoy the treat ;)

Love,
Gendut.";

        $ecard = new \Metro\Ecard\EcardImageWithFrameGenerator('Diaana Eka Saraswati', 'Bayu Hendra Winata Notokusumo', $message, '55a2ff3458a80.png');
        echo "<img src='" . asset('ecards/generate/' . $ecard->getFilename()) . "' />";
    }
}
