<?php namespace Frontend;

use Theme, View;

class McdFrontendController extends FrontendController{

    public function __construct()
    {
        parent::__construct();
        Theme::init('mcd2');
    }

    // Route 1

//    public function getIndex()
//    {
//        return View::make('site.index');
//    }

//    public function getStep1()
//    {
//        return View::make('steps.register.facebook');
//    }

//    public function getStep2()
//    {
//        return View::make('steps.friends.select');
//    }

//    public function getStep3()
//    {
//        return View::make('steps.ecard');
//    }

//    public function getStep4()
//    {
//        return View::make('steps.finish');
//    }

    // Redeem Route 1

    public function getConnect()
    {
        return View::make('redeem.fb_connect');
    }

    // Route 2 (Email)

//    public function getRoute2Step1()
//    {
//        return View::make('steps.register.email');
//    }

//    public function getRoute2Step2()
//    {
//        return View::make('steps.friends.email');
//    }

    // Redeem

//    public function getVerify()
//    {
//        return View::make('redeem.verifytest');
//    }

//    public function getThanks()
//    {
//        return View::make('redeem.thanks');
//    }

    // Email

//    public function getEmailLink()
//    {
//        return View::make('emails.uniquelink');
//    }

//    public function getEmailVoucher()
//    {
//        return View::make('emails.voucher');
//    }

}