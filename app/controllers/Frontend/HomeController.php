<?php

namespace Frontend;

use Illuminate\Support\Facades\Redirect;
use Input;
use Auth;
use View;
use Session;
use Metro\Activity\ActivityRepositoryInterface;
use Metro\User\FriendsPaginator;
use Metro\User\UserRepositoryInterface;

class HomeController extends FrontendController
{

    /**
     * @var \Metro\User\UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var \Metro\User\FriendsPaginator
     */

    private $friendPaginator;

    /**
     * @var \Metro\Activity\ActivityRepositoryInterface
     */
    private $activityRepo;

    public function __construct(UserRepositoryInterface $userRepo, ActivityRepositoryInterface $activityRepo, FriendsPaginator $friendPaginator)
    {
        parent::__construct();

        $this->userRepo = $userRepo;
        $this->friendPaginator = $friendPaginator;
        $this->activityRepo = $activityRepo;
    }

    public function index()
    {
        if (Input::has('from_facebook'))
        {
            return Redirect::route('gate');
        }
        return View::make('home.index');
    }

    public function login()
    {
        $json = ['status' => 0];
        $user = $this->userRepo->loginOrCreate(Input::all());

        if($user)
        {
            $authUser = Auth::loginUsingId($user['id']);
            if($authUser){
                $json['status'] = 1;
                $json['token_limit_exceeded'] = $this->activityRepo->isTokenExceeded($this->authUser()->id);
                $json['next_date']   = \Carbon\Carbon::tomorrow()->format('Y/m/d');
                //$json['used_friends']   = json_encode($this->activityRepo->usedFriends($this->authUser()));
                $json['used_friends']   = [];
            }
        }

        return $this->json($json);
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();

        return $this->redirect->to('/');
    }

    public function friends()
    {
        $friends = $this->userRepo->friendsCached($this->authUser());
        $friends = $this->friendPaginator->paginate($friends, Input::get('page'), Input::get('keyword'));

        $json = [
            'status' => 1,
            'html'  => View::make('friend.list', compact('friends'))->render()
        ];
        return $this->json($json);
    }

    public function saveFriendsSelection()
    {
        try
        {
            $exceeded = $this->activityRepo->isTokenExceeded($this->authUser()->id);

            if($exceeded)
            {
                throw new \Metro\Exception\DailyTokenExceededException();
            }

            $this->userRepo->saveSelection(Input::get('selection'));
            $this->activityRepo->logSelection(Auth::user(), Input::get('selection'));
        }
        catch(\Metro\Exception\DailyTokenExceededException $e)
        {
            return $this->redirect->route('voucher.exceeded');
        }

        return $this->redirect->route('voucher.pick');
    }
}
