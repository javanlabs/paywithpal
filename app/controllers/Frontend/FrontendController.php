<?php


namespace Frontend;

use Auth;
use Config;
use Theme;
use Input;
use Session;

class FrontendController extends \BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->view->share('isAuth', Auth::check());
        $this->view->share('isFacebook', $this->isFacebook());

        Theme::init(Config::get('app.theme'));
    }

    protected function isFacebook()
    {
        return Session::has('signed_request') || Input::has('signed_request');
    }
}