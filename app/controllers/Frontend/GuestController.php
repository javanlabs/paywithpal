<?php

namespace Frontend;


use Input;
use View;
use Metro\FacebookHelper;
use Auth;
use Setting;
use Session;

class GuestController extends FrontendController
{
    protected $facebookHelper;

    public function __construct(FacebookHelper $facebookHelper)
    {
        parent::__construct();
        $this->facebookHelper = $facebookHelper;
    }

    public function gate()
    {
        // user mengakses melalui facebook page

        if(Input::has('signed_request'))
        {
            Session::get('signed_request', Input::get('signed_request'));
            $data = $this->facebookHelper->parseSignedRequest(Input::get('signed_request'));

            if($data)
            {
                if( isset($data["page"]) )
                {
                    // cek apakah user sudah like page
                    if(isset($data['page']['liked']) && $data['page']['liked'] === true)
                    {
                        return $this->redirect->route('voucher.index');
                    }
                }
                else
                {
                    // halaman dibuka via canvas, redirect to voucher page
                    return $this->redirect->route('voucher.index');
                }
            }
        }

        // jika user sudah like, lempar ke halaman voucher
        if(Auth::check() && $this->facebookHelper->isLiked(Auth::user()->facebook_access_token, Setting::get('facebook.pageId')))
        {
            return $this->redirect->route('voucher.index');
        }

        return View::make('guest.gate');
    }

}