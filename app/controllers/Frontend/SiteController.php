<?php


namespace Frontend;


class SiteController extends FrontendController
{
    public function getTerm()
    {
        return \View::make('site.term');
    }

    public function getPrivacy()
    {
        return \View::make('site.privacy');
    }

    public function getSupport()
    {
        return \View::make('site.support');
    }
}