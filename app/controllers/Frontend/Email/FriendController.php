<?php namespace Frontend\Email;

use Event, View, Input, Mail, Setting, Crypt, Auth, Session, DB;
use Frontend\McdFrontendController;
use Illuminate\Support\Facades\File;
use Metro\Invitation\InvitationRepositoryInterface;
use Metro\Voucher\ImageGenerator\McdVoucher;
use Metro\Voucher\VoucherModel;
use Metro\Voucher\VoucherRepositoryInterface;

class FriendController extends McdFrontendController{
    /**
     * @var VoucherRepositoryInterface
     */
    private $voucherRepo;

    private $invitationRepo;


    /**
     * FriendController constructor.
     * @param VoucherRepositoryInterface $voucherRepo
     */
    public function __construct(VoucherRepositoryInterface $voucherRepo, InvitationRepositoryInterface $invitationRepo)
    {
        $this->voucherRepo = $voucherRepo;
        $this->invitationRepo = $invitationRepo;

        parent::__construct();
    }

    public function writeEmail()
    {
        Event::fire('user.step', 2);

        return View::make('steps.friends.email');
    }

    public function sendEmail()
    {
        $input = Input::only('name', 'email');
        $user = $this->authUser();

        // ambil unlock code dari session
        $unlockCode = Session::get('invitecode');

        $invitation = $this->invitationRepo->findByCode($unlockCode);

        $voucher = $this->voucherRepo->pickOne();
        $code = VoucherModel::getUniqueCode();

        // update voucher
        DB::transaction(function() use($voucher, $user, $input, $code, $invitation) {
            $voucher->friend_name = $input['name'];
            $voucher->friend_email = $input['email'];
            $voucher->unique_code = $code;
            $voucher->reserved_by = $user->id;
            $voucher->unlock_code_id = $invitation->id;
            $voucher->save();
        });

        $data = [
            'sender' => $user->name,
            'receiver' => $input['name'],
            'link' => route('route2.redeem.link', $code),
        ];

        // generate image voucher
        $image = (new McdVoucher())->uniqueLink($data['link'], $data['sender'], $data['receiver']);
        $targetDir = public_path('unique_link');

        if(!File::exists($targetDir))
        {
            File::makeDirectory($targetDir);
        }

        $imagePath = $targetDir . '/' . $code . '.png';
        $image->save($imagePath);
        $data['image'] = asset('unique_link/' . $code . '.png');

        Mail::send('emails.alt.link', $data, function($message) use($input){
            $message->to($input['email'], $input['name'])->subject(Setting::get('email.subject'));
        });

        Session::put('friend_name', $input['name']);
        Session::put('friend_email', $input['email']);
        Session::put('voucher_label', $voucher->label);
        Session::put('voucher_type', $voucher->type);
        Session::put('unique_code', $code);

        // Hapus unlock code setelah mengirimkan link ke teman
        $this->invitationRepo->delete($unlockCode);

        return $this->redirect->route('route2.ecard');
    }

    public function finish()
    {
        Event::fire('user.step', 4);

        Auth::logout();
        Session::forget('invitecode');
        Session::forget('friend_email');
        Session::forget('unique_code');

        return View::make('steps.finish');
    }

    public function inviteFriendPreview($code)
    {
        $voucher = VoucherModel::whereUniqueCode($code)->first();
        $user = $voucher->user;
        $data = [
            'sender' => $user->name,
            'receiver' => $voucher['friend_name'],
            'link' => route('route2.redeem.link', $code)
        ];

        $image = (new McdVoucher())->uniqueLink($data['link'], $data['sender'], $data['receiver']);

        $targetDir = public_path('unique_link');
        if(!File::exists($targetDir))
        {
            File::makeDirectory($targetDir);
        }
        $imagePath = $targetDir . '/' . $code . '.png';
        $image->save($imagePath);

        $data['image'] = asset('unique_link/' . $code . '.png');

        return View::make('emails.alt.link', $data);
    }

}
