<?php namespace Frontend\Email;

use Theme;
use Frontend\FrontendController;

class FrontendRoute2Controller extends FrontendController{

    public function __construct()
    {
        parent::__construct();
        Theme::init('mcd');
    }

}