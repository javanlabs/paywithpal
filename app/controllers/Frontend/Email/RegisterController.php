<?php namespace Frontend\Email;

use Event, Input, View, Session, Auth;
use Frontend\McdFrontendController;
use Metro\FacebookHelper;
use Metro\Invitation\InvitationRepositoryInterface;
use Metro\User\UserRepositoryInterface;

class RegisterController extends McdFrontendController{

    private $userRepo;
    private $invitationRepo;

    public function __construct(InvitationRepositoryInterface $invitationRepo, UserRepositoryInterface $userRepo)
    {
        parent::__construct();

        $this->userRepo = $userRepo;
        $this->invitationRepo = $invitationRepo;
    }

    public function viaEmail()
    {
        Event::fire('user.step', 1);

        return View::make('steps.register.email');
    }

    public function postEmail()
    {
        $input = Input::only('name', 'email');
        $code = Session::get('invitecode', null);

        $invitation = $this->invitationRepo->findByCode($code);

        if($invitation->user){
            if($invitation->user->email != $input['email']){
                return $this->redirect->route('route2.home')->withError('Kode telah digunakan oleh pengguna lain');
            }
        }

        $user = $this->userRepo->findOrCreateViaEmail($input, $this->invitationRepo);
        $this->invitationRepo->reservedBy($user, $code);

        $authUser = Auth::loginUsingId($user->id);

        if($authUser){
            return $this->redirect->route('route2.friend.select');
        }

        return $this->redirect->route('route2.home')->withError('Gagal Login');
    }

    public function fbConnect()
    {
        $facebookHelper = new FacebookHelper();

        $profile = $facebookHelper->getProfile(Input::get('authResponse')['accessToken']);

        $dataProfile = [
            'name'                  => $profile->getProperty('name'),
            'email'                 => $profile->getProperty('email'),
            'facebook_id'           => $profile->getProperty('id')
        ];

        $json = [
            'status' => 1,
            'profile' => $dataProfile
        ];

        return $this->json($json);
    }

}