<?php namespace Frontend\Email;

use Frontend\McdFrontendController;
use Illuminate\Support\Facades\File;
use Input, View, Crypt, App, Session, DB, Response, Mail, Setting, Auth;
use Metro\Activity\ActivityRepositoryInterface;
use Metro\FacebookHelper;
use Metro\User\UserRepositoryInterface;
use Metro\Voucher\ImageGenerator\McdVoucher;
use Metro\Voucher\RedeemModel;
use Metro\Voucher\VoucherModel;

class RedeemController extends McdFrontendController{

    private $userRepo;
    private $activityRepo;

    public function __construct(UserRepositoryInterface $userRepo, ActivityRepositoryInterface $activityRepo)
    {
        parent::__construct();

        $this->userRepo = $userRepo;
        $this->activityRepo = $activityRepo;
    }

    public function link($code)
    {
        $length = strlen($code);

        if($length == 5){
            // redeem route 1
            $voucherRedeemer = VoucherModel::where('unique_code', $code)->count() > 0 ? true : false;

            if($voucherRedeemer){
                return View::make('redeem.fb_connect', compact('code'));
            }
        }

        return $this->redirect->route('verify')->with('token', $code);
    }

    // connect with fb redeemer route 1
    public function login()
    {
        $code = Input::get('code');

        $facebookHelper = new FacebookHelper();

        $profile = $facebookHelper->getProfile(Input::get('authResponse')['accessToken']);

        $voucherOwner = VoucherModel::where('unique_code', $code)->where('friend_name', $profile->getProperty('name'))->first();

        if($voucherOwner){
            // update friend email berdasarkan email fb
            $voucherOwner->friend_email = $profile->getProperty('email');
            $voucherOwner->save();

            Session::put('token', $code);

            $json = [
                'status' => 1,
                'redirect_url' => route('verify')
            ];
        }else{
            $json = [
                'status' => 0,
                'redirect_url' => url('/'),
                'message' => 'Kamu tidak dapat me-redeem voucher ini.'
            ];
        }

        return $this->json($json);
    }

    public function verify()
    {
        $tokenCode = Session::get('token');

        if(!isset($tokenCode)){
            return $this->redirect->to('/');
        }

        $isVerified = false;

        $voucherRedeemer = VoucherModel::where('unique_code', $tokenCode)->first();

        if($voucherRedeemer){

            $redeemer = $this->userRepo->findOrCreateViaEmail(['name' => $voucherRedeemer->friend_name, 'email' => $voucherRedeemer->friend_email]);

            $authUser = Auth::loginUsingId($redeemer->id);

            $user = $this->authUser();

            $voucher = DB::table('vouchers')->where('code', $voucherRedeemer->code)
                ->join('users', 'vouchers.reserved_by', '=', 'users.id')
                ->select('users.name as sender', 'vouchers.id', 'vouchers.code', 'vouchers.discount')->first();

            $redeemVoucher = RedeemModel::whereCode($voucher->code)->first();

            if(!$redeemVoucher){
                // simpan redeem
                $redeemVoucher = new RedeemModel;
                $redeemVoucher->user_id = $user->id;
                $redeemVoucher->facebook_friend_name = $user->name;
                $redeemVoucher->voucher_id = $voucher->id;
                $redeemVoucher->code = $voucher->code;
                $redeemVoucher->discount = $voucher->discount;
                $redeemVoucher->is_shared = 0;
                $redeemVoucher->save();
            }

            $redeem = ['sender' => $voucher->sender, 'is_shared' => $redeemVoucher->is_shared];
            $isVerified = true;
            $thanksCode = $tokenCode;
        }

        return View::make('redeem.new_verify', compact('isVerified', 'redeem', 'thanksCode'));
    }

    public function shared()
    {
        $token = Input::get('token');
        $type = Input::get('type');
        $user = $this->authUser();

        $json['status'] = 0;

        $voucher = VoucherModel::where('unique_code', $token)->first();
        $redeem = RedeemModel::whereCode($voucher->code)->with('voucher.user')->first();

        if($type == 'email'){
            $image = (new McdVoucher())->shareToEmail($user->name, $redeem->voucher->user->name);

            $targetDir = public_path('email_share');
            if(!File::exists($targetDir))
            {
                File::makeDirectory($targetDir);
            }
            $imagePath = $targetDir . '/' . $voucher->code . '.png';
            $image->save($imagePath);

            $data['image'] = asset('email_share/' . $voucher->code . '.png');

            Mail::send('emails.alt.share', $data, function($message) use($redeem){
                $message->to($redeem->voucher->user->email, $redeem->voucher->user->name)->subject(Setting::get('email.subject'));
            });
        }

        $isShared = $redeem->is_shared == 1 ? true : false;

        if(!$isShared){
            $redeem->is_shared = 1;
            $redeem->save();

            $data = [
                'receiver' => $user->name,
                'sender' => $redeem->voucher->user->name,
                'voucher' => $redeem->code,
                'type'  => $voucher->type,
            ];

            $image = (new McdVoucher())->voucherCode($data['voucher'], $voucher->label, $data['type'], Setting::get('voucher.valid_hour_start'), Setting::get('voucher.valid_hour_finish'));

            $targetDir = public_path('unique_link');
            if(!File::exists($targetDir))
            {
                File::makeDirectory($targetDir);
            }
            $imagePath = $targetDir . '/' . $voucher->code . '.png';
            $image->save($imagePath);

            $data['image'] = asset('unique_link/' . $voucher->code . '.png');

            Mail::send('emails.alt.voucher', $data, function($message) use($user){
                $message->to($user->email, $user->name)->subject(Setting::get('email.subject'));
            });
        }

        $json['status'] = 1;
        $json['redirect_url'] = route('thanks', ['c' => $token]);

        Auth::logout();

        return Response::json($json);
    }

    public function thanks()
    {
        $token = Input::get('c');

        try{
            $voucher = VoucherModel::where('unique_code', $token)->first();
            $redeem = RedeemModel::whereCode($voucher->code)->with('user', 'voucher.user')->first();

            return View::make('redeem.thanks', compact('redeem'));
        }catch (\Exception $e){
            App::abort(417, 'Invalid link');
        }
    }

    public function preview($code)
    {
        $voucher = VoucherModel::where('unique_code', $code)->first();
        $redeem = RedeemModel::whereCode($voucher->code)->with('voucher.user')->first();

        $data = [
            'receiver' => $redeem['facebook_friend_name'],
            'sender' => $redeem->voucher->user->name,
            'voucher' => $redeem->code,
            'type'  => 1
        ];

        $image = (new McdVoucher())->voucherCode($data['voucher'], $voucher['label'], $voucher['type'], Setting::get('voucher.valid_hour_start'), Setting::get('voucher.valid_hour_finish'));

        $targetDir = public_path('unique_link');
        if(!File::exists($targetDir))
        {
            File::makeDirectory($targetDir);
        }
        $imagePath = $targetDir . '/' . $code . '.png';
        $image->save($imagePath);

        $data['image'] = asset('unique_link/' . $code . '.png');

        return View::make('emails.alt.voucher', $data);

    }
}
