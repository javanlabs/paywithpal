<?php namespace Frontend\Email;

use Frontend\McdFrontendController;
use Illuminate\Support\Facades\Config;
use Input, Event, View, Auth, Setting, Session, File;
use Metro\Ecard\EcardImageGenerator;
use Metro\Ecard\EcardModel;

class EcardController extends McdFrontendController{

    public function write()
    {
        Event::fire('user.step', 3);

        $assetsPath = asset('ecards');
        $pathEcards = public_path('ecards');

        $files = File::files($pathEcards);

        $ecards = [];

        foreach($files as $file){
            $basename = basename($file);
            $ecards[] = [
                'thumb_src' => $assetsPath.'/thumbs/'.$basename,
                'src' => $assetsPath.'/'.$basename,
            ];
        }

        // devide 6
        $ecardChunks = array_chunk($ecards, 6);

        $eCardRows = [];

        foreach($ecardChunks as $i => $ecards){
            $eCardRows[$i]['ecards'] = $ecards;
            if($i == 0)
                $eCardRows[$i]['active'] = 'active';
        }

        $cities = Config::get('cities');
        $cities = array_combine($cities, $cities);

        return View::make('steps.ecard', compact('eCardRows', 'cities'));
    }

    public function store()
    {
        // ambil unique code dari link voucher
        $uniqueCode = Session::get('unique_code');

        $input = Input::except('ecard_tpl', '_token');
        $ecardExplode = explode('/', Input::get('ecard_tpl'));
        $ecardTpl = end($ecardExplode);

        $eCard = new EcardImageGenerator($input['friend_name'], $ecardTpl, $uniqueCode);

        EcardModel::create([
            'friend_name' => $input['friend_name'],
            'friend_email' => $input['friend_email'],
            'message' => $input['message'],
            'location' => $input['location'],
            'image_path' => $eCard->getFilename(),
            'user_id' => Auth::id()
        ]);

        return $this->redirect->route('route2.finish');
    }
}
