<?php namespace Frontend\Email;

use Frontend\McdFrontendController;
use View, Input, Session;
use Metro\Invitation\InvitationRepositoryInterface;

class InvitationController extends McdFrontendController
{

    private $repo;

    public function __construct(InvitationRepositoryInterface $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    public function index()
    {
//        Session::forget('validcode');

        return View::make('site.index'); // view unlock code
    }

    public function check()
    {
        $code = Input::get('code');
        $loginVia = Input::get('register-via');

        $isValidCode = $this->repo->isValidCode($code);

        if($isValidCode){
            Session::put('validcode', $isValidCode);
            Session::put('invitecode', $code);

            if($loginVia == 'facebook'){
                return $this->redirect->route('route1.register');
            }else{
                return $this->redirect->route('route2.register');
            }
        }

        Session::flash('error', 'Kode tidak valid !');
        return $this->redirect->route('route2.home')->withInput();
    }

}
