<?php namespace Frontend\Facebook;

use Event, View, Input, Auth, Session;
use Frontend\McdFrontendController;
use Metro\Invitation\InvitationRepositoryInterface;
use Metro\User\UserRepositoryInterface;

class RegisterController extends McdFrontendController{

    private $userRepo;
    private $invitationRepo;

    public function __construct(InvitationRepositoryInterface $invitationRepo, UserRepositoryInterface $userRepo)
    {
        parent::__construct();

        $this->userRepo = $userRepo;
        $this->invitationRepo = $invitationRepo;
    }

    public function index()
    {
        Event::fire('user.step', 1);

        return View::make('steps.register.facebook');
    }

    public function store()
    {
        $code = Session::get('invitecode', null);

        $invitation = $this->invitationRepo->findByCode($code);

        $json = ['status' => 0];

        $user = $this->userRepo->loginOrCreate(Input::get('authResponse'));

        if($invitation->user){
            if($invitation->user->email != $user['email']){
                Session::put('error', 'Kode telah digunakan oleh pengguna lain');
                $json['status'] = 1;
                $json['redirect_url'] = url('/');
            }
        }

        // abaikan jika status 1 karena kode telah digunakan
        if($user && $json['status'] == 0){
            $authUser = Auth::loginUsingId($user['id']);
            if($authUser){
                $this->invitationRepo->reservedBy($this->authUser(), $code);

                $json['status'] = 1;
                $json['redirect_url'] = route('route1.friend.select');
            }
        }

        return $this->json($json);
    }

}