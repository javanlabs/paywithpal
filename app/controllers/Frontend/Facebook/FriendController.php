<?php namespace Frontend\Facebook;

use Frontend\McdFrontendController;
use Metro\FacebookHelper;
use Metro\Invitation\InvitationRepositoryInterface;
use Metro\Voucher\ImageGenerator\McdVoucher;
use Metro\Voucher\VoucherModel;
use Metro\Voucher\VoucherRepositoryInterface;
use View, Event, Input, Image, Session, File, DB;
use Metro\User\FriendsPaginator;
use Metro\User\UserRepositoryInterface;

class FriendController extends McdFrontendController{

    private $userRepo;
    private $invitationRepo;
    private $voucherRepo;

    private $friendPaginator;

    public function __construct(UserRepositoryInterface $userRepo, InvitationRepositoryInterface $invitationRepo, VoucherRepositoryInterface $voucherRepo, FriendsPaginator $friendPaginator)
    {
        parent::__construct();

        $this->userRepo = $userRepo;
        $this->invitationRepo = $invitationRepo;
        $this->voucherRepo = $voucherRepo;
        $this->friendPaginator = $friendPaginator;
    }

    public function index()
    {
        Event::fire('user.step', 2);

        return View::make('steps.friends.facebook');
    }

    public function store()
    {
        $input = Input::all();
        $user = $this->authUser();

        // ambil unlock code dari session
        $unlockCode = Session::get('invitecode');

        $invitation = $this->invitationRepo->findByCode($unlockCode);

        $voucher = $this->voucherRepo->pickOne();
        $code = VoucherModel::getUniqueCode(5);

        // update voucher
        DB::transaction(function() use($voucher, $user, $input, $code, $invitation) {
            $voucher->friend_name = $input['friend_name'];
            $voucher->unique_code = $code;
            $voucher->reserved_by = $user->id;
            $voucher->unlock_code_id = $invitation->id;
            $voucher->save();
        });

        $link = route('route2.redeem.link', $code);
        $sender = $user->name;
        $receiver = $input['friend_name'];

        // generate image voucher
        $image = (new McdVoucher())->uniqueLink($link, $sender, $receiver);
        $targetDir = public_path('unique_link');

        if(!File::exists($targetDir))
        {
            File::makeDirectory($targetDir);
        }

        $imagePath = $targetDir . '/' . $code . '.png';
        $image->save($imagePath);

        $facebookHelper = new FacebookHelper;

        $message = "Voucher Ramadhan";

        $fbResponse = $facebookHelper->postPhoto($this->authUser()->facebook_access_token, $imagePath, $message, $input['friend_id']);

        if($fbResponse){
            Session::put('friend_name', $input['friend_name']);
//            Session::put('friend_avatar', $input['friend_avatar']);
            Session::put('voucher_label', $voucher->label);
            Session::put('voucher_type', $voucher->type);
            Session::put('unique_code', $code);

            // Hapus unlock code setelah mengirimkan link ke teman
            $this->invitationRepo->delete($unlockCode);

            return $this->redirect->route('route2.ecard');
        }

        return $this->redirect->back();
    }

//    private function generatePhotoPost($params)
//    {
//        $img = Image::make(base_path("assets/img/photo-fb-skin.jpg"));
//        $img->resize(550, 470);
//
//        $watermark = Image::make($params['friend_avatar']);
//
//        $img->insert($watermark, "top", 0, 25);
//
//        $fontStyleLight = base_path('assets/fonts/Gotham-Light.ttf');
//        $fontStyleMedium = base_path('assets/fonts/Gotham-Medium.otf');
//
//        $words = [
//            "Kamu baru saja memberikan",
//            $params['friend_name'],
//            "Voucher Ramadhan"
//        ];
//
//        $distance = 0;
//        foreach($words as $word){
//            $img->text($word, 275, (380 + $distance), function($font) use($fontStyleMedium, $fontStyleLight, $distance){
//                $font->file($distance == 30 ? $fontStyleMedium : $fontStyleLight);
//                $font->size(24);
//                $font->color('#000000');
//                $font->align('center');
//            });
//
//            $distance += 30;
//        }
//
//        return $img;
//    }

}