<?php

namespace Frontend;
use Illuminate\Support\Facades\Redirect;
use Image;
use Metro\Activity\ActivityModel;
use Metro\Activity\ActivityRepositoryInterface;
use Metro\Exception\DailyTokenExceededException;
use Metro\FacebookHelper;
use Metro\Voucher\VoucherMailer;
use Metro\Voucher\VoucherRepositoryInterface;
use App;
use Metro\User\UserRepositoryInterface;
use Input;
use Crypt;
use View;

class VoucherController extends FrontendController
{
    /**
     * @var \Metro\Voucher\VoucherRepositoryInterface
     */
    private $repo;

    /**
     * @var \Metro\User\UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var \Metro\FacebookHelper
     */
    private $facebookHelper;

    /**
     * @var \Metro\Voucher\VoucherMailer
     */
    private $mailer;
    /**
     * @var ActivityRepositoryInterface
     */
    private $activityRepo;

    public function __construct(
        VoucherRepositoryInterface $repo,
        UserRepositoryInterface $userRepo,
        FacebookHelper $facebookHelper,
        VoucherMailer $mailer,
        ActivityRepositoryInterface $activityRepo
    ) {
        parent::__construct();

        $this->repo           = $repo;
        $this->userRepo       = $userRepo;
        $this->facebookHelper = $facebookHelper;
        $this->mailer         = $mailer;
        $this->activityRepo   = $activityRepo;
    }

    public function index()
    {
        $voucherCount = $this->repo->countRedeemed();
        $vouchers = $this->repo->all(Input::get('page', 1), Input::get('keyword'));
        $usedFriends = $this->activityRepo->usedFriends(\Auth::user());

        if($this->request->ajax())
        {
            $json['html'] = View::make('voucher.list', compact('vouchers'))->render();
            $json['status'] = 1;
            return $this->json($json);
        }
        else
        {
            $route = route('voucher.index');
            return View::make('voucher.index', compact('voucherCount', 'vouchers', 'route', 'usedFriends'));
        }
    }

    public function my()
    {
        $voucherCount = $this->repo->countRedeemed($this->authUser());
        $vouchers = $this->repo->my($this->authUser(), Input::get('page', 1), Input::get('keyword'));
        $usedFriends = $this->activityRepo->usedFriends(\Auth::user());

        if($this->request->ajax())
        {
            $json['html'] = View::make('voucher.list', compact('vouchers'))->render();
            $json['status'] = 1;
            return $this->json($json);
        }
        else
        {
            $route = route('voucher.my');
            return View::make('voucher.index', compact('voucherCount', 'vouchers', 'route', 'usedFriends'));
        }
    }

    public function pick()
    {
        $selections = $this->userRepo->loadSelection();
        if(empty($selections))
        {
            return $this->redirect->route('voucher.index')->withInfo('Silakan pilih teman dulu');
        }
        return View::make('voucher.pick', compact('selections'));
    }

    public function selection()
    {
        $json['status'] = 0;

        try {
            $selections = $this->userRepo->loadSelection();
            //$feedback   = $this->facebookHelper->getStatusFeedback(
            //    $this->authUser()->facebook_access_token
            //);
            //$scores     = $this->repo->getFriendsScore($feedback);
            $scores = [];

            $vouchers   = $this->repo->loadFromSelection(
                $this->authUser(),
                $selections,
                $scores
            );

            if (!$vouchers) {
                $json['message'] = 'Maaf, saat ini tidak ada voucher yang cocok untuk kamu. Coba lagi nanti ya.';
            } else {

                $this->postToFacebook($vouchers);

                $json['status'] = 1;
                $json['html'] = View::make(
                    'voucher.selection',
                    compact('vouchers')
                )->render();
            }

            return $this->json($json);

        } catch (\Facebook\FacebookSDKException $e) {
            $json['message'] = 'ERROR KONEKSI KE FACEBOOK: ' . $e->getMessage();
            return $this->json($json);
        } catch (DailyTokenExceededException $e) {
            $json['message'] = 'Jatah voucher kamu untuk hari ini sudah habis.';
            return $this->json($json);
        }
    }

    public function redeem()
    {
        $json = ['status' => 0];
        $voucher = $this->repo->find(Input::get('id'));

        if($voucher)
        {
            $imageGenerator = App::make('voucherGenerator');
            $filename       = Crypt::encrypt($voucher['id']) . '.png';
            $voucherPath    = 'vouchers/facebook/' . $filename;
            $printPath      = 'vouchers/print/' . $filename;

            $params = [
                'tag_id'       => Input::get('tag_id'),
                'avatar'       => Input::get('avatar'),
                'name'         => Input::get('name'),
                'image_path'   => $voucherPath
            ];

            $voucherData = [
                'user_name'     => $this->authUser()->name,
                'user_avatar'   => $this->authUser()->avatar,
                'friend_avatar' => Input::get('avatar'),
                'friend_name'   => $params['name'],
                'name'          => $params['name'],
                'code'          => $voucher['code'],
                'discount'      => $voucher['discount'],
                'message'       => strip_tags(Input::get('message'))
            ];

            try
            {

                $message = $this->repo->getFacebookPostMessage();
                $imageGenerator->forFacebook($voucherData)->save(public_path($voucherPath));
                $this->facebookHelper->postPhoto($this->authUser()->facebook_access_token, public_path($voucherPath), $message, $params['tag_id']);

                $imageGenerator->forPrint($voucherData)->save(public_path($printPath));
                $this->mailer->redeem($this->authUser(), $voucher, public_path($printPath));
            }
            catch(\Exception $e)
            {
                // @todo: handle if sending email failed
                // @todo: handle if generate image failed
            }

            $this->repo->redeem($this->authUser(), $voucher, $params);

            $json['voucher_url'] = asset($printPath);
            $json['status'] = 1;

            $this->userRepo->clearSelection();
        }

        return $this->json($json);
    }

    public function exceeded()
    {
        return View::make('voucher.exceeded');
    }

    public function regenerate($id)
    {
        $redeem = \Metro\Voucher\RedeemModel::findOrFail($id);
        $user = $redeem->user;

        $imageGenerator = App::make('voucherGenerator');
        $filename       = Crypt::encrypt($redeem->voucher_id) . '.png';
        $printPath      = 'vouchers/print/' . $filename;
        $message        = $this->repo->getMessageByDiscount($redeem['discount']);

        $friendImage = false;
        $temp = explode('base64,', $redeem['facebook_friend_avatar']);
        if(isset($temp[1]))
        {
            $friendImage = imagecreatefromstring(base64_decode($temp[1]));
        }

        $voucherData = [
            'user_name'     => $user->name,
            'user_avatar'   => $user->avatar,
            'friend_avatar' => $friendImage,
            'friend_name'   => $redeem['facebook_friend_name'],
            'name'          => $user->name,
            'code'          => $redeem['code'],
            'discount'      => $redeem['discount'],
            'message'       => $message
        ];

        try
        {

            $imageGenerator->forPrint($voucherData)->save(public_path($printPath));
            $redeem->image_path = $printPath;
            $redeem->save();
            $this->mailer->redeem($user, $redeem, public_path($printPath));
        }
        catch(\Exception $e)
        {
            dd($voucherData);
            // @todo: handle if sending email failed
            // @todo: handle if generate image failed
            return $e->getMessage();
        }
        return Redirect::to(asset($printPath));
    }

    protected function postToFacebook($vouchers)
    {
        foreach ($vouchers as $voucher)
        {
            if ($voucher['pivot']['discount'] == 0)
            {
                $imageGenerator = App::make('voucherGenerator');
                $filename       = Crypt::encrypt(microtime()) . '.png';
                $voucherPath    = 'vouchers/facebook/' . $filename;


                $params = [
                    'name'       => $voucher['name'],
                    'tag_id'     => $voucher['tag_id'],
                    'avatar'     => $voucher['avatar'],
                    'image_path' => $voucherPath
                ];
                $this->repo->redeem($this->authUser(), $voucher['pivot'], $params);

                $voucherData = [
                    'user_name'     => $this->authUser()->name,
                    'user_avatar'   => $this->authUser()->avatar,
                    'friend_avatar' => $voucher['avatar'],
                    'friend_name'   => $voucher['name'],
                    'name'          => $voucher['name'],
                    'discount'      => $voucher['pivot']['discount'],
                    'message'       => strip_tags($voucher['message'])
                ];

                $message = $this->repo->getFacebookPostMessage();
                $imageGenerator->forFacebook($voucherData)->save(public_path($voucherPath));

                $this->facebookHelper->postPhoto($this->authUser()->facebook_access_token, public_path($voucherPath), $message, $voucher['tag_id']);
            }
        }
    }
}
