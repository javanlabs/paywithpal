<?php

class BaseController extends Controller {

    protected $view;
    protected $redirect;
    protected $request;
    protected $input;

    public function __construct()
    {

        $this->view = App::make('view');
        $this->redirect = App::make('redirect');
        $this->request = App::make('request');
        $this->input = App::make('request');

    }

    protected function authUser()
    {
        if(Auth::check())
        {
            return Auth::user();
        }

        throw new \Exception('user not authenticated');
    }

    protected function json($data = array(), $status = 200, array $headers = array(), $options = 0)
    {
        return Response::json($data, $status, $headers, $options);
    }

}