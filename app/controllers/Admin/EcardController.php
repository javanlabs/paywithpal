<?php namespace Admin;

use Excel;
use View;
use Input;
use Metro\Ecard\EcardModel;

class EcardController extends AdminController{

    public function index()
    {
        $sortType = 'asc';

        if(Input::has('sortType')){
            $sortType = Input::get('sortType');
        }

        $ecards = EcardModel::with('user')->orderBy('created_at', $sortType)->paginate(10);

        return View::make('admin.ecard.index', compact('ecards'));
    }

    public function export()
    {
        $sortType = 'asc';

        if(Input::has('sortType')){
            $sortType = Input::get('sortType');
        }

        $ecards = EcardModel::with('user')->orderBy('created_at', $sortType)->get();

        $ecardsArray = [];

        foreach($ecards as $i => $ecard){
            $ecardsArray[$i] = [
                'Pengirim' => $ecard->sender_name.' ('.$ecard->sender_email.')',
                'Penerima' => $ecard->friend_name.' ('.$ecard->friend_email.')',
                'Pesan' => $ecard->message,
                'Kota' => $ecard->location,
                'Tanggal' => $ecard->generated_at,
                'Status' => $ecard->status_for_human,
            ];
        }

        Excel::create('status_pengiriman_ecards_' . date('dmy'), function ($excel) use ($ecardsArray) {
            $excel->setTitle('Ecards');
            $excel->sheet('Ecards', function ($sheet) use ($ecardsArray) {
                $sheet->fromArray($ecardsArray);
            });
        })->download('csv');
    }

}
