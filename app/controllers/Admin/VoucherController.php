<?php


namespace Admin;

use Excel;
use Metro\Voucher\VoucherDbRepository;
use Input;
use Metro\Voucher\RedeemModel;
use Metro\Voucher\VoucherModel;
use View;
use Auth;

class VoucherController extends AdminController
{

    /**
     * @var \Metro\Voucher\VoucherDbRepository
     */
    private $repo;

    public function __construct(VoucherDbRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    public function index()
    {
        $vouchers = $this->repo->paginateUnused();
        $availableCount = $this->repo->countUnused();
        $usedCount = $this->repo->countUsed();

        return View::make('admin.voucher.index', compact('vouchers', 'availableCount', 'usedCount'));
    }

    public function redeemed()
    {
        $vouchers = $this->repo->paginateRedeemed(Input::get('keyword'));
        $availableCount = $this->repo->countUnused();
        $redeemedCount = $this->repo->countRedeemed();
        $collectedCount = $this->repo->countCollected();
        $generatedCount = $this->repo->countGenerated();

        return View::make('admin.voucher.redeemed', compact('vouchers', 'availableCount', 'redeemedCount', 'collectedCount', 'generatedCount'));
    }

    public function collected()
    {
        $vouchers = $this->repo->paginateCollected(Input::get('keyword'));
        $availableCount = $this->repo->countUnused();
        $redeemedCount = $this->repo->countRedeemed();
        $collectedCount = $this->repo->countCollected();
        $generatedCount = $this->repo->countGenerated();

        return View::make('admin.voucher.collected', compact('vouchers', 'availableCount', 'redeemedCount', 'collectedCount', 'generatedCount'));
    }

    public function collect()
    {
        $this->repo->set_collected(Input::get('code'));
        // $vouchers = $this->repo->paginateRedeemed();
        $vouchers = $this->repo->paginateRedeemed(Input::get('q'));
        $availableCount = $this->repo->countUnused();
        $redeemedCount = $this->repo->countRedeemed();
        $collectedCount = $this->repo->countCollected();

        return $this->redirect->back()->withInfo('voucher collected');
    }

    public function getImport()
    {
        return View::make('admin.voucher.import');
    }

    public function postImport()
    {
        $user = Auth::user()->toArray();
        if ($user['email'] == "acep.kosim@id.leoburnett.com") {
            $file = Input::file('file')->getRealPath();

            $inserted = 0;
            Excel::load($file, function ($reader) use (&$inserted) {
                $inserted = $this->repo->bulk($reader->toArray());
            })->get();

            return $this->redirect->back()->withInfo($inserted . ' vouchers inserted');
        } else {
            return $this->redirect->back()->withErrors($inserted . 'Permission denied');
        }
    }

    public function generated()
    {
        $availableCount = $this->repo->countUnused();
        $usedCount = $this->repo->countUsed();

        $sortType = 'asc';

        if(Input::has('sortType')){
            $sortType = Input::get('sortType');
        }

        $vouchers = $this->repo->paginateUsed($sortType);

        return View::make('admin.voucher.generated', compact('vouchers', 'availableCount', 'usedCount'));
    }

    public function generatedExport()
    {
        $sortType = 'asc';

        if(Input::has('sortType')){
            $sortType = Input::get('sortType');
        }

        $vouchers = $this->repo->allUsed($sortType);

        $redeemsArray = [];

        foreach($vouchers as $i => $voucher){
            $redeemsArray[$i] = [
                'Unlock Code' => $voucher->unlock_code,
                'Pengirim' => $voucher->sender_name.' ('.$voucher->sender_email.')',
                'Penerima' => $voucher->friend_name.' ('.$voucher->friend_email.')',
                'Voucher' => $voucher->label,
                'Status'  => $voucher->status,
                'Tanggal Unlock Code' => $voucher->unlock_date,
                'Tanggal' => $voucher->generated_at
            ];
        }

        Excel::create('generated_vouchers_' . date('dmy'), function ($excel) use ($redeemsArray) {
            $excel->setTitle('Generated Voucher');
            $excel->sheet('Generated Voucher', function ($sheet) use ($redeemsArray) {
                $sheet->fromArray($redeemsArray);
            });
        })->download('csv');
    }

}
