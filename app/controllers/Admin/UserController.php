<?php


namespace Admin;


use Metro\Exception\ValidationException;
use Metro\User\UserRepositoryInterface;
use Input;
use View;
use Auth;

class UserController extends AdminController{

    /**
     * @var UserRepositoryInterface
     */
    private $repo;

    function __construct(UserRepositoryInterface $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    public function index()
    {
        $users = $this->repo->paginate();
        $userCount = $this->repo->countShopper();
        $adminCount = $this->repo->countAdmin();
        return View::make('admin.user.index', compact('users', 'userCount', 'adminCount'));
    }

    public function add()
    {
        return View::make('admin.user.add');
    }

    public function store()
    {
        $input = Input::all();

        try
        {
            $this->repo->addAdmin($input);
            return $this->redirect->route('admin.user')->withInfo('New Admin Added');
        }
        catch (ValidationException $e)
        {
            return $this->redirect->back()->withInput()->withErrors($e->getErrors());
        }
    }

    public function remove()
    {
        $id = Input::get('id');

        try
        {
            $this->repo->removeAdmin(Auth::user(), $id);
            return $this->redirect->back()->withInfo('Admin Removed');
        }
        catch (ValidationException $e)
        {
            return $this->redirect->back()->withErrors($e->getErrors());
        }
    }
}
