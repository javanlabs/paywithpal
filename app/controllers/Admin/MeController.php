<?php


namespace Admin;

use Input;
use Metro\Exception\ValidationException;
use Metro\User\UserRepositoryInterface;
use View;
use Auth;

class MeController extends AdminController
{

    /**
     * @var UserRepositoryInterface
     */
    protected  $userRepo;

    public function __construct(UserRepositoryInterface $userRepo)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
    }

    public function getPassword()
    {
        return View::make('admin.me.password');
    }

    public function postPassword()
    {
        $input = Input::all();

        try
        {
            $this->userRepo->updatePassword(Auth::user(), $input);
            return $this->redirect->back()->withInfo('Password changed');
        }
        catch (ValidationException $e)
        {
            return $this->redirect->back()->withErrors($e->getErrors());
        }
    }
}