<?php

#test added to push

namespace Admin;

use Auth;
use Theme;
use Config;

class AdminController extends \BaseController{

    public function __construct()
    {
        parent::__construct();

        if(Auth::check())
        {
            $this->view->share('authUser', Auth::user()->toArray());
        }

        Theme::init('metro');
    }

}
