<?php


namespace Admin;

use Illuminate\Support\Facades\File;
use Metro\Invitation\InvitationRepositoryInterface;
use Input;
use View;
use Auth;

class UnlockCodeController extends AdminController{

    private $repo;

    public function __construct(InvitationRepositoryInterface $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    public function index()
    {
        $codes = $this->repo->paginateUnused();
        $unusedCount = $this->repo->countUnused();
        $usedCount = $this->repo->countUsed();

        return View::make('admin.unlock_code.index', compact('codes', 'unusedCount', 'usedCount'));
    }

    public function used()
    {
        $codes = $this->repo->paginateUsed();
        $unusedCount = $this->repo->countUnused();
        $usedCount = $this->repo->countUsed();

        return View::make('admin.unlock_code.used', compact('codes', 'unusedCount', 'usedCount'));
    }

    public function release($id)
    {
        $result = $this->repo->release($id);

        if($result){
            return $this->redirect->route('admin.code.used')->with('success', 'Berhasil me-<em>release</em> unlock code');
        }

        return $this->redirect->route('admin.code.used')->with('error', 'Gagal me-<em>release</em> unlock code');
    }

    public function getImport()
    {
        return View::make('admin.unlock_code.import');
    }

    public function postImport()
    {
        $file = Input::file('file');
        $destination = storage_path('app/unlock-code.csv');
        if(File::exists($destination))
        {
            File::delete($destination);
        }

        $file->move(storage_path('app'), 'unlock-code.csv');

        return $this->redirect->back()->withInfo('File uploaded. You need to run "php artisan app:insert-unlock-code" manually via terminal.');
    }

}
