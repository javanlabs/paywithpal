<?php


namespace Admin;

use Metro\User\UserRepositoryInterface;
use Input;
use View;

class SiteController extends AdminController{


    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    function __construct(UserRepositoryInterface $userRepo)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
    }

    public function getLogin()
    {
        return View::make('admin.site.login');
    }

    public function postLogin()
    {
        if ($this->userRepo->login(Input::get('email'), Input::get('password')))
        {
            return $this->redirect->intended(route('admin.dashboard'));
        }

        return $this->redirect->route('admin.login')->withError('Invalid Credentials')->withInput();
    }

    public function logout()
    {
        $this->userRepo->logout($this->authUser());
        return $this->redirect->route('admin.login')->withInfo('Bye... thank for being cute today ;)');
    }
}