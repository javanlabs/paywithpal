<?php


namespace Admin;

use View;

class DashboardController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return View::make('admin.dashboard.index');
    }
}