<?php


namespace Admin;

use Setting;
use Input;
use View;
use DB;
use Auth;

class SettingController extends AdminController{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $facebook    = Setting::get('facebook');
        $voucher     = Setting::get('voucher');
        $email       = Setting::get('email');
        $pages       = Setting::get('pages');
        $route      = Setting::get('route');

        if(!isset($voucher['type1'])) $voucher['type1'] = 8;

        $percentages = [];

        $type1 = 0;
        $type2 = 100;
        foreach(range(0, 10) as $i){
            $percentages[10 - $i] = "{$type1}% Type 1 - {$type2}% Type 2";
            $type1 += 10;
            $type2 -= 10;
        }

        return View::make('admin.setting.index', compact('route', 'facebook', 'voucher', 'email', 'pages', 'percentages'));
    }

    public function save()
    {
        Setting::set('email', Input::get('email'));
        Setting::set('pages', Input::get('pages'));
        Setting::set('route', Input::get('route'));

        Setting::set('voucher.valid_in', Input::get('voucher.valid_in'));
        Setting::set('voucher.valid_until', Input::get('voucher.valid_until'));
        Setting::set('voucher.valid_hour_start', Input::get('voucher.valid_hour_start'));
        Setting::set('voucher.valid_hour_finish', Input::get('voucher.valid_hour_finish'));
        Setting::set('voucher.url', Input::get('voucher.url'));
        Setting::set('voucher.type1', Input::get('voucher.type1'));

        return $this->redirect->back()->withInfo('Settings updated');
    }

    public function getRestart()
    {
        return View::make('admin.setting.restart');
    }

    public function postRestart()
    {
        \Artisan::call('db:seed', ['--class' => 'UserSeeder']);
        \Artisan::call('db:seed', ['--class' => 'VoucherTableSeeder']);
        \Artisan::call('db:seed', ['--class' => 'RedeemTableSeeder']);
        DB::table('activities')->truncate();

        return $this->redirect->back()->withInfo('Application Data Restarted');
    }
}
