<?php

use Metro\User\UserModel as User;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call('InvitationTableSeeder');
        $this->call('EcardTableSeeder');
//        $this->call('RedeemTableSeeder');
        $this->call('AdminSeeder');
        $this->call('UserSeeder');
    }

}

class InvitationTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('invitations')->truncate();

        for ($i = 0; $i < 100; $i++) {
            DB::table('invitations')->insert([
                'code' => Str::quickRandom(5)
            ]);
        }
    }

}

class VoucherTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('vouchers')->truncate();
        $discounts = [0, 100, 200, 300];

        for ($i = 0; $i < 100; $i++) {
            DB::table('vouchers')->insert(
                array('code' => Str::quickRandom(14), 'discount' => $discounts[array_rand($discounts)])
            );
        }
    }
}

class RedeemTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('redeems')->truncate();
        DB::table('vouchers')->whereNotNull('deleted_at')->delete();
        $availableDiscounts = [0, 100, 200, 300];
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $code = Str::quickRandom(14);
            $discount = $availableDiscounts[array_rand($availableDiscounts)];
            $now = date('Y-m-d H:i:s');

            $voucherId = DB::table('vouchers')->insertGetId(
                array(
                    'code' => $code,
                    'discount' => $discount,
                    'reserved_by' => 1,
                    'created_at' => $now,
                    'updated_at' => $now,
                    'deleted_at' => $now
                )
            );

            DB::table('redeems')->insert(
                array(
                    'voucher_id' => $voucherId,
                    'user_id' => 2,
                    'code' => $code,
                    'facebook_friend_name' => $faker->name,
                    'facebook_friend_tag_id' => 999999,
                    'facebook_friend_avatar' => 'data:image/gif;base64,R0lGODdhZABkAIAAAMzMzJaWliwAAAAAZABkAAACc4SPqcvtD6OctNqLs968+w+G4kiW5omm6sq27gvH8kzX9o3n+s73/g8MCofEovGITCqXzKbzCY1Kp9Sq9YrNarfcrvcLDovH5LL5jE6r1+y2+w2Py+f0uv2Oz+v3/L7/DxgoOEhYaHiImKi4yNjo+AhpWAAAOw==',
                    'discount' => $discount,
                    'created_at' => $now,
                    'updated_at' => $now,
                    'is_shared' => 1
                )
            );
        }
    }
}

class AdminSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->whereRole(User::ROLE_ADMIN)->delete();

        $now = date('Y-m-d H:i:s');

        DB::table('users')->insert(
            array(
                'facebook_id' => 10204597869326479,
                'email' => 'uyab.exe@gmail.com',
                'name' => 'Bayu Hendra Winata',
                'password' => Hash::make('4dm1npwpm3tr0'),
                'role' => User::ROLE_ADMIN,
                'created_at' => $now,
                'updated_at' => $now
            )
        );
    }
}

class UserSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->whereRole(User::ROLE_SHOPPER)->delete();

        $faker = \Faker\Factory::create();
        $now = date('Y-m-d H:i:s');

        for ($i = 0; $i < 20; $i++) {
            DB::table('users')->insert(
                array(
                    'facebook_id' => rand(1000000, 9999999),
                    'email' => $faker->email,
                    'name' => $faker->name,
                    'password' => Hash::make($faker->word),
                    'role' => User::ROLE_SHOPPER,
                    'created_at' => $now,
                    'updated_at' => $now
                )
            );
        }
    }
}

class EcardTableSeeder extends Seeder{
    public function run()
    {
        DB::table('ecards')->truncate();
    }
}