<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVouchersChangeType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE `vouchers` CHANGE `type` `type` ENUM('1','2','3','4','5') NOT NULL default 1;");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement("ALTER TABLE `vouchers` CHANGE `type` `type` ENUM('1','2') NOT NULL default 1;");
	}

}
