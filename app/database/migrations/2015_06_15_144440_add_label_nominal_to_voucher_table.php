<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLabelNominalToVoucherTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->string('label')->after('type');
            $table->integer('nominal')->after('label');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->dropColumn('label');
            $table->dropColumn('nominal');
		});
	}

}
