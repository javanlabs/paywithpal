<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSharedToRedeemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('redeems', function(Blueprint $table)
		{
			$table->enum('is_shared', [0, 1])->default(0)->after('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('redeems', function(Blueprint $table)
		{
			$table->dropColumn('is_shared');
		});
	}

}
