<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailToRedeemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('redeems', function(Blueprint $table)
		{
			$table->string('email', 50)->nullable()->after('code');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('redeems', function(Blueprint $table)
		{
			$table->dropColumn('email');
		});
	}

}
