<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function ($table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('email')->nullable()->unique();
            $table->string('password')->nullable();
            $table->bigInteger('facebook_id')->nullable()->unsigned()->unique();
            $table->string('facebook_access_token')->nullable();
            $table->string('facebook_link')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->enum('role', ['shopper', 'admin']);
            $table->rememberToken();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('users');
	}

}
