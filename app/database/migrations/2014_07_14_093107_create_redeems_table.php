<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedeemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('redeems', function ($table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->text('facebook_friend_name');
            $table->text('facebook_friend_tag_id');
            $table->text('facebook_friend_avatar');
            $table->unsignedInteger('voucher_id')->unique();
            $table->text('image_path');
            $table->string('code')->unique();
            $table->smallInteger('discount', false, true);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('redeems');
	}

}
