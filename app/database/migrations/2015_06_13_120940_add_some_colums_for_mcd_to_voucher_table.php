<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeColumsForMcdToVoucherTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->string('friend_name', 50)->nullable()->after('reserved_by');
            $table->string('friend_email', 50)->nullable()->after('friend_name');
            $table->string('unique_code')->unique()->nullable()->after('friend_email');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vouchers', function(Blueprint $table)
		{
			$table->dropColumn('friend_name');
            $table->dropColumn('friend_email');
            $table->dropColumn('unique_code');
		});
	}

}
