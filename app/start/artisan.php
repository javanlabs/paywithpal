<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new EcardCommand);
Artisan::add(new UnlockCodeCommand(new \Metro\Invitation\InvitationRepository(new \Metro\Invitation\InvitationModel())));
Artisan::add(new GenerateVoucherCommand);
Artisan::add(new EcardRecoveryCommand);
