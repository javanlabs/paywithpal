<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::to('/');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

Route::filter('fan', function()
{
    $fbHelper = new \Metro\FacebookHelper();

    if(Input::has('signed_request') && Auth::check())
    {
        $fan = $fbHelper->isLiked(Auth::user()->facebook_access_token, Setting::get('facebook.pageId'));

        if(!$fan)
        {
            return Redirect::route('home');
        }

    }

});
/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/*
|--------------------------------------------------------------------------
| Admin Protection Filter
|--------------------------------------------------------------------------
|
|
*/

Route::filter('admin', function()
{
    $isAdmin = Auth::check() && Auth::user()->is_admin;
    if (!$isAdmin)
        return Redirect::route('admin.login')->withError('Yu no login ?');
});

/*
|--------------------------------------------------------------------------
| Redirect all http to https
|--------------------------------------------------------------------------
|
|
*/
Route::filter('force.ssl', function()
{
    if(!Request::secure() && $_ENV['APP_FORCE_SSL'])
    {
        return Redirect::secure(Request::getRequestUri());
    }
});

Route::filter('csrf.ajax', function()
{
    $token = Request::ajax() ? Request::header('X-CSRF-Token') : Input::get('_token');
    if (Session::token() != $token)
        throw new Illuminate\Session\TokenMismatchException;
});

Route::filter('validcode', function()
{
    if(!Session::has('validcode')){
        return Redirect::to('/')->withError('Kamu belum memasukkan invitation code!');
    }
});

Route::filter('dev', function(){
    if (App::environment('production')){
        return Redirect::to('/');
    }
});

Route::filter('check_promo_period', function()
{
    $promoEnded = promo_ended();

    //bypass home
    if(Request::is('/')) {

    } else {

        if ($promoEnded) {
            return Redirect::to('/');
        }
    }

});
