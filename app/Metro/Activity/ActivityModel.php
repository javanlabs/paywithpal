<?php


namespace Metro\Activity;

use Eloquent;

class ActivityModel extends Eloquent {

    const FACEBOOK_CONNECT = 'facebook-connect';
    const USER_LOGOUT = 'user-logout';
    const FRIEND_SELECT = 'friend-select';
    const FRIEND_SELECT_DETAIL = 'friend-select-detail';
    const VOUCHER_REDEEM = 'voucher-redeem';
    const VOUCHER_OUT_OF_STOCK = 'voucher-out-of-stock';
    const ADMIN_LOGIN = 'admin-login';
    const ADMIN_LOGIN_FAILED = 'admin-login-failed';
    const ADMIN_LOGOUT = 'admin-logout';
    const ADMIN_UNAUTHORIZED = 'admin-unauthorized';
    const ADMIN_IMPORT_EXCEL = 'admin-import-excel';
    const ADMIN_UPDATE_SETTING = 'admin-update-setting';
    const FRIEND_SELECTION_SCORE = 'friend-selection-score';
    const DB_QUERY = 'db-query';

    protected $table = 'activities';
}