<?php


namespace Metro\Activity;


use Carbon\Carbon;
use DB;

class ActivityDbRepository implements ActivityRepositoryInterface
{

    /**
     * @var ActivityModel
     */
    private $model;

    function __construct(ActivityModel $model)
    {

        $this->model = $model;
    }

    public function log($subject, $predicate, $object = null, $annotation = null, $parentId = null)
    {
        return $this->model->insertGetId([
            'subject'       => $subject,
            'predicate'     => $predicate,
            'object'        => $object,
            'annotation'    => $annotation,
            'parent_id'     => $parentId,
            'created_at'    => new Carbon()
        ]);
    }

    public function logSelection($user, $selections)
    {
        $id = $this->log($user->id, ActivityModel::FRIEND_SELECT);
        foreach($selections as $friend)
        {
            $this->log($user->id, ActivityModel::FRIEND_SELECT_DETAIL, $friend['name'], null, $id);
        }
    }

    public function isTokenExceeded($subject)
    {
        $today = Carbon::today()->format('Y-m-d');
        $predicate = (string) ActivityModel::FRIEND_SELECT;

        return $this->model->whereSubject($subject)
            ->where('predicate', $predicate)
            ->whereRaw("DATE(created_at) = '$today'")
            ->count() >= \Setting::get('score.limit', 1);
    }

    public function isAlreadySelected($item, $user)
    {
        return $this->model->whereSubject($user->id)->wherePredicate(ActivityModel::FRIEND_SELECTION_SCORE)->whereObject($item['name'])->exists();
    }

    public function usedFriends($user)
    {
        if(!$user)
        {
            return array();
        }
        return $this->model->whereSubject($user->id)->wherePredicate(ActivityModel::FRIEND_SELECTION_SCORE)->lists('object');
    }
}