<?php


namespace Metro\Activity;


interface ActivityRepositoryInterface {

    public function log($subject, $predicate, $object = null, $annotation = null);

    public function usedFriends($user);
}