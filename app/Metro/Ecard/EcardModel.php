<?php namespace Metro\Ecard;

use Eloquent;

class EcardModel extends Eloquent{

    protected $table = 'ecards';

    protected $fillable = ['friend_name', 'friend_email', 'message', 'location', 'image_path', 'user_id'];

    public function user()
    {
        return $this->belongsTo('\Metro\User\UserModel', 'user_id');
    }

    protected function getSenderNameAttribute()
    {
        if($this->user){
            return $this->user->name;
        }

        return false;
    }

    protected function getSenderEmailAttribute()
    {
        if($this->user){
            return $this->user->email;
        }

        return false;
    }

    public function getGeneratedAtAttribute()
    {
        if($this->created_at)
        {
            return $this->created_at->format('d F Y H:i:s');
        }

        return false;
    }

    protected function getMessageAttribute()
    {
        if(!$this->attributes['message']) {
            return "Selamat Hari Raya Idul Fitri 1436 H \nTaqabbalallahu Minna Wa Minkum";
        }

        return $this->attributes['message'];
    }

    public function getStatusForHumanAttribute()
    {
        $status = 'Belum Dikirim';
        if($this->attributes['is_sent'] == 1) {
            $status = 'Terkirim';
        }

        return $status;
    }

    public function getStatusHtmlAttribute()
    {
        $status = 'Belum Dikirim';
        if($this->attributes['is_sent'] == 1) {
            $status = '<span class="label label-success">Terkirim</span>';
        }

        return $status;
    }

    public function getUrlAttribute()
    {
        $temp = explode('.', $this->attributes['image_path']);
        $filename = $temp[0] . '_frame' . '.' . $temp[1];
        $image = public_path('ecards/generate/' . $filename);
        if(is_file($image))
        {
            return asset('ecards/generate/' . $filename);
        }
    }
}
