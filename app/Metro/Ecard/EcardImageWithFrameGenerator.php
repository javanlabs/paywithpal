<?php namespace Metro\Ecard;

use Image;

class EcardImageWithFrameGenerator
{

    private $filename;
    private $from;
    private $message;
    private $code;
    private $to;
    private $template;
    /**
     * @var null
     */
    private $generatedImage;

    /**
     * @param $from
     * @param $to
     * @param $message
     * @param null $generatedImage
     */
    public function __construct($from, $to, $message, $generatedImage = null)
    {
        $this->from = $from;
        $this->to = $to;

        $this->message = $message;
        if(!$this->message) {
            $this->message = 'Selamat Hari Raya Idul Fitri 1436H';
        }

        $this->generatedImage = $generatedImage;

        $this->create();
    }

    public function getFilename()
    {
        return $this->filename;
    }

    private function create()
    {
        $to = 'Halo ' . $this->to . ',';
        $fontRegular = public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-Regular.otf");
        $fontBoldItalic = public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCondItalic.otf");

        // GENERATE FILENAME
        $temp = explode('.', $this->generatedImage);
        $filename = $temp[0] . '_frame' . '.' . $temp[1];

        // FRAME
        $frameTemplate = public_path('ecards/template/frame.png');
        $frame = Image::make($frameTemplate);

        // PENERIMA
        $frame->text(
            str_limit($to),
            45, // x position
            795, // y position
            function ($font) use ($fontBoldItalic) {
                $font->file($fontBoldItalic);
                $font->size(18);
                $font->color('#6E9E43');
                $font->align('left');
                $font->valign('top');
            }
        );

        // PENERIMA
        $frame->text(
            str_limit(strtoupper($this->to), 20, '...'),
            245, // x position
            295, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(32);
                $font->color('#764416');
                $font->align('left');
                $font->valign('top');
            }
        );

        // PESAN
        $temp = explode("\n", $this->message);

        $lines = [];
        foreach ($temp as $line) {
            $wrap = wordwrap($line, 80, "\n");

            foreach (explode("\n", $wrap) as $text) {
                if($text) {
                    $lines[] = $text;
                }
            }

        }

        $y = 825;
        foreach ($lines as $line) {

            $frame->text(
                $line,
                45, // x position
                $y, // y position
                function ($font) use ($fontRegular) {
                    $font->file($fontRegular);
                    $font->size(14);
                    $font->color('#666666');
                    $font->align('left');
                    $font->valign('top');
                }
            );
            $y += 15;

        }

        $frame->text(
            'Salam,' . "\n" . $this->from,
            45, // x position
            $y + 10, // y position
            function ($font) use ($fontRegular) {
                $font->file($fontRegular);
                $font->size(14);
                $font->color('#666666');
                $font->align('left');
                $font->valign('top');
            }
        );

        $img = Image::make(public_path('ecards/generate/' . $this->generatedImage));
        $img->widen('550');
        $frame->insert($img, 'top', 0, 370);

        $frame->save(public_path('ecards/generate/' . $filename));


        $this->filename = $filename;
    }

}
