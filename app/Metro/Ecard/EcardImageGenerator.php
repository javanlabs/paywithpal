<?php namespace Metro\Ecard;

use Image;

class EcardImageGenerator
{

    private $filename;

    public function __construct($to, $eCardTpl, $code)
    {
        $this->to = $to;
        $this->eCardTpl = $eCardTpl;

        $this->create($to, $eCardTpl, $code);
    }

    public function getFilename()
    {
        return $this->filename;
    }

    private function create($to, $ecardTpl, $code)
    {
        $positions = [
            'ecard1.jpg' => ['x' => 91, 'y' => 55],
            'ecard2.jpg' => ['x' => 94, 'y' => 48],
            'ecard3.jpg' => ['x' => 85, 'y' => 47],
            'ecard4.jpg' => ['x' => 91, 'y' => 51],
            'ecard5.jpg' => ['x' => 86, 'y' => 38],
        ];

        $ecardTpls = array_keys($positions);

        if(in_array($ecardTpl, $ecardTpls)){
            $position = $positions[$ecardTpl];
        }else{
            // default x & y position
            $position = $positions['ecard3.jpg'];
        }

        $template = public_path('ecards/' . $ecardTpl);
        $img = Image::make($template);

        $fontStyle = base_path('assets/fonts/arial.ttf');

        $img->text($to, $position['x'], $position['y'], function ($font) use ($fontStyle) {
            $font->file($fontStyle);
            $font->size(18);
            $font->color(array(5, 104, 60, 0.9));
        });

        $filename = $code . '.png';

        $img->save(public_path('ecards/generate/' . $filename));

        $this->filename = $filename;
    }

}
