<?php


namespace Metro\Voucher;

use Eloquent;

class RedeemModel extends Eloquent{

    protected $table = 'redeems';

    protected $appends = ['user_name', 'user_avatar', 'friend_avatar', 'friend_name', 'date'];

    public function user()
    {
        return $this->belongsTo('\Metro\User\UserModel', 'user_id');
    }

    public function voucher()
    {
        return $this->belongsTo('\Metro\Voucher\VoucherModel', 'voucher_id');
    }

    protected function getUserAvatarAttribute()
    {
        if($this->user)
        {
            $facebookId = $this->user->facebook_id;
            return "https://graph.facebook.com/$facebookId/picture?width=200&height=200";
        }

        return false;
    }

    protected function getUserNameAttribute()
    {
        if ($this->user) {
            return $this->user->name;
        }

        return false;
    }

    protected function getFriendAvatarAttribute()
    {
        return $this->facebook_friend_avatar;
    }

    protected function getFriendNameAttribute()
    {
        return $this->facebook_friend_name;
    }

    protected function getDateAttribute()
    {
        return $this->created_at->format('d-m-Y');
    }

    protected function getUnlockCodeAttribute()
    {
        if($this->voucher->unlock){
            return $this->voucher->unlock->code;
        }

        return false;
    }

    protected function getVoucherSenderNameAttribute()
    {
        if($this->voucher->user){
            return $this->voucher->user->name;
        }

        return false;
    }

    protected function getVoucherSenderEmailAttribute()
    {
        if($this->voucher->user){
            return $this->voucher->user->email;
        }

        return false;
    }

    protected function getVoucherReceiverNameAttribute()
    {
        if($this->voucher){
            return $this->voucher->friend_name;
        }

        return false;
    }

    protected function getVoucherReceiverEmailAttribute()
    {
        if($this->voucher){
            return $this->voucher->friend_email;
        }

        return false;
    }

    protected function getVoucherLabelAttribute()
    {
        if($this->voucher){
            return $this->voucher->label;
        }

        return false;
    }

    public function getGeneratedAtAttribute()
    {
        if($this->created_at)
        {
            return $this->created_at->format('d F Y H:i:s');
        }

        return false;
    }
}