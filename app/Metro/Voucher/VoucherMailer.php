<?php


namespace Metro\Voucher;

use Metro\Mail\Mailer;
use Setting;

class VoucherMailer extends Mailer{

    public function redeem($user, $voucher, $file)
    {
        $email = $user->email;
        $subject = Setting::get('email.subject');;
        $view = 'emails.vouchers.redeem';
        $data['voucher'] = $voucher;
        $data['file']   = $file;
        $data['footer'] = Setting::get('email.footer');
        $this->sendTo($email, $subject, $view, $data);
    }
}