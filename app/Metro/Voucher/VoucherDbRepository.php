<?php


namespace Metro\Voucher;

use Crypt;
use Illuminate\Database\QueryException;
use Metro\Activity\ActivityDbRepository;
use Metro\Activity\ActivityModel;
use Metro\Exception\DailyTokenExceededException;
use Metro\Exception\EmptySelectionException;
use Metro\FacebookHelper;
use Metro\Voucher\Calculator\CalculatorInterface;
use DB;
use Carbon\Carbon;
use App;
use Image;

class VoucherDbRepository implements VoucherRepositoryInterface{

    protected $voucherModel;

    protected $redeemModel;

    protected $messages;

    /**
     * @var FacebookHelper
     */
    private $facebookHelper;

    /**
     * @var Calculator\CalculatorInterface
     */
    private $calculator;

    /**
     * @var Setting
     */
    private $setting;
    /**
     * @var ActivityDbRepository
     */
    private $activity;

    function __construct(
        VoucherModel $voucherModel,
        RedeemModel $redeemModel,
        FacebookHelper $facebookHelper,
        CalculatorInterface $calculator,
        ActivityDbRepository $activity
    ) {
        $this->voucherModel   = $voucherModel;
        $this->redeemModel    = $redeemModel;
        $this->facebookHelper = $facebookHelper;
        $this->calculator     = $calculator;
        $this->setting        = App::make('setting');
        $this->activity       = $activity;
    }

    public function find($id)
    {
        $voucher = $this->voucherModel->find($id);

        if($voucher)
        {
            return $voucher->toArray();
        }

        return false;
    }

    public function all($page = 1, $keyword = null, $perPage = 9)
    {
        $page = (int) $page;
        $query = $this->redeemModel->select('redeems.*', 'users.name', 'users.facebook_id')->join('users', 'users.id', '=', 'user_id')->orderBy('redeems.created_at', 'desc')->forPage($page, $perPage);

        if ($keyword) {
            $query->where(function($query2) use ($keyword) {
                $query2->where('facebook_friend_name', 'like', "%$keyword%");
                $query2->orWhere('users.name', 'like', "%$keyword%");
            });
        }

        return $query->get()->toArray();
    }

    public function my($user, $page = 1, $keyword = null, $perPage = 9)
    {
        $page = (int) $page;

        $query = $this->redeemModel->select('redeems.*', 'users.name', 'users.facebook_id')->where('user_id', '=', $user->id)->join('users', 'users.id', '=', 'user_id')->orderBy('redeems.created_at', 'desc')->forPage($page, $perPage);

        if ($keyword) {
            $query->where(function($query2) use ($keyword) {
                    $query2->where('facebook_friend_name', 'like', "%$keyword%");
                    $query2->orWhere('users.name', 'like', "%$keyword%");
                });
        }

        return $query->get()->toArray();
    }

    public function pickOne()
    {
        $x = rand(0, 10);
        $voucher = null;

        // ambil tipe 1
        if($x >= $this->setting->get('voucher.type1'))
        {
            $voucher = $this->voucherModel->whereNull('reserved_by')->whereType(1)->first();
        }
        // ambil tipe 2
        else
        {
            $voucher = $this->voucherModel->whereNull('reserved_by')->where('type', '<>', 1)->orderByRaw('RAND()')->first();
        }

        // jika tidak ada (mungkin sudah habis), ambil sembarang voucher
        if(!$voucher)
        {
            $voucher = $this->voucherModel->whereNull('reserved_by')->orderByRaw('RAND()')->first();
        }

        return $voucher;
    }

    public function loadFromSelection($user, $selection, $scores)
    {
        $this->clearReserved($user);

        if(!is_array($selection))
        {
            throw new EmptySelectionException;
        }

        //if (!$this->isAllowRedeem($user))
        //{
        //    throw new DailyTokenExceededException;
        //}

        $vouchers = [];
        foreach($selection as $item)
        {
            if(!empty($item['id']))
            {
                //$used = $this->isUsed($item, $user);
                $used = false;

                $voucher = $this->getVoucher($user, $item['name'], $scores);

                if($voucher)
                {

                    $message    = $this->getMessage($item, $scores);
                    $level      = $this->calculator->getLevel(array_get($scores, $item['name']));

                    $vouchers[] = [
                        'pivot'     => $voucher,
                        'name'      => $item['name'],
                        'avatar'    => $item['avatar'],
                        'tag_id'    => $item['id'],
                        'used'      => $used,
                        'message'   => $message,
                        'level'     => $level
                    ];
                }
            }
        }

        return $vouchers;
    }

    public function redeem($user, $voucher, $params)
    {
        $now = new Carbon();

        DB::table('redeems')->insert([
            'user_id'                => $user->id,
            'facebook_friend_name'   => $params['name'],
            'facebook_friend_tag_id' => $params['tag_id'],
            'facebook_friend_avatar' => $this->encodeAvatar($params['avatar']),
            'voucher_id'             => $voucher['id'],
            'image_path'             => $params['image_path'],
            'code'                   => $voucher['code'],
            'discount'               => $voucher['discount'],
            'created_at'             => $now,
            'updated_at'             => $now
        ]);

        $voucher = $this->voucherModel->find($voucher['id']);

        if ($voucher)
        {
            $voucher->delete();
            $this->activity->log($user->id, ActivityModel::VOUCHER_REDEEM, $voucher->id);
        }

        $this->clearReserved($user);
    }

    public function set_collected($voucher_code)
    {
        $this->redeemModel->where('code', '=', $voucher_code)->update(array('is_collected' => 1,'collected_at' => Carbon::now()));
    }

    public function countRedeemed($user = null)
    {
        $model = $this->redeemModel;
        if($user)
        {
            $model = $model->where('user_id', '=', $user->id);
        }

//        return $model->count();
        return $model->where('is_shared', 1)->count();
    }

    public function countCollected()
    {
        $model = $this->redeemModel;
        $model = $model->where('is_collected', '=', 1);
        return $model->count();
    }

    public function countGenerated()
    {
        return $this->redeemModel->count();
    }

    protected function clearReserved($user)
    {
        // Error while sending STMT_PREPARE packet
        // $this->voucherModel->whereRaw("reserved_by = {$user->id}")->update(['reserved_by'=>null]);

        try {
            DB::statement('update vouchers set reserved_by = NULL where reserved_by = ' . $user->id . ' and deleted_at is null');
        } catch(QueryException $e) {
            // error query
        }
    }

    protected function getVoucher($user, $name, $scores)
    {
        $score = 0;
        if(isset($scores[$name]))
        {
            $score = $scores[$name];
        }

        $discount = $this->calculator->getDiscount($score);

        $voucher = $this->voucherModel->whereNull('reserved_by')->whereDiscount($discount)->first();
        if($voucher){
            $voucher->reserved_by = $user->id;
            $voucher->save();
        }

        // log
        $this->activity->log($user->id, ActivityModel::FRIEND_SELECTION_SCORE, $name, $score);

        return $voucher;
    }

    public function countUnused()
    {
        return $this->voucherModel->whereNull('reserved_by')->count();
    }

    public function countUsed()
    {
        return $this->voucherModel->whereNotNull('reserved_by')->count();
    }

    public function paginateUnused()
    {
        return $this->voucherModel->whereNull('reserved_by')->orderBy('code')->paginate();
    }

    public function paginateUsed($sortType)
    {
        return $this->voucherModel->with('user', 'unlock', 'redeem')->whereNotNull('reserved_by')->orderBy('updated_at', $sortType)->paginate();
    }

    public function allUsed($sortType)
    {
        return $this->voucherModel->with('user', 'unlock', 'redeem')->whereNotNull('reserved_by')->orderBy('updated_at', $sortType)->get();
    }


    public function paginateRedeemed($keyword = null){
        if($keyword)
        {
            // created_at dibikin alias karena reedems.created_at selalu ngambil users.created_at
            return $this->redeemModel->select('redeems.*', 'redeems.created_at as redeems_created_at', 'users.name', 'users.email', 'users.facebook_id')->join('users', 'users.id', '=', 'user_id')->where('code', '=', "$keyword")->orWhere('users.name','like',"%$keyword%")->orderBy('redeems.created_at', 'desc')->paginate();

        }else{
            return $this->redeemModel->select('redeems.*','redeems.created_at as redeems_created_at', 'users.name', 'users.email', 'users.facebook_id')->join('users', 'users.id', '=', 'user_id')->orderBy('redeems.created_at', 'desc')->paginate();
        }
    }

    public function paginateCollected($keyword = null){
        if($keyword)
        {
            return $this->redeemModel->select('redeems.*','redeems.created_at as redeems_created_at', 'users.name', 'users.email', 'users.facebook_id')->join('users', 'users.id', '=', 'user_id')->where('redeems.is_collected', '=', 1)->where(function ($query) use ($keyword) {$query->where('code', '=', "$keyword")->orWhere('users.name','like',"%$keyword%");})->orderBy('redeems.created_at', 'desc')->paginate();
        }else{
            return $this->redeemModel->select('redeems.*','redeems.created_at as redeems_created_at', 'users.name', 'users.email', 'users.facebook_id')->join('users', 'users.id', '=', 'user_id')->where('redeems.is_collected', '=', 1)->orderBy('redeems.created_at', 'desc')->paginate();
        }


        // return $query->get()->toArray();
    }

    public function bulk($rawData)
    {
        $data = array();
        $now = Carbon::now();
        $inserted = 0;

        foreach($rawData as $item)
        {
            $data = array(
                'code'       => $item['code'],
                'discount'   => $item['discount'],
                'created_at' => $now,
                'updated_at' => $now
            );

            $exist = $this->voucherModel->whereCode($data['code'])->exists();
            if(!$exist)
            {
                $this->voucherModel->insert($data);
                $inserted++;
            }
        }

        return $inserted;
    }

    public function getFriendsScore($feedback)
    {
        $friends = [];

        if(!is_array($feedback))
        {
            return $friends;
        }

        foreach($feedback as $item)
        {
            foreach($item['comments'] as $name => $count)
            {
                if(!isset($friends[$name]))
                {
                    $friends[$name] = 0;
                }
                $friends[$name] = $friends[$name] + ($this->setting->get('score.comment') * $count);
            }

            foreach($item['likes'] as $name => $count)
            {
                if(!isset($friends[$name]))
                {
                    $friends[$name] = 0;
                }
                $friends[$name] = $friends[$name] + ($this->setting->get('score.like') * $count);
            }

            foreach($item['sharedposts'] as $name => $count)
            {
                if(!isset($friends[$name]))
                {
                    $friends[$name] = 0;
                }
                $friends[$name] = $friends[$name] + ($this->setting->get('score.share') * $count);
            }
        }

        return $friends;

    }

    public function getFacebookPostMessage()
    {
        $message = 'Makasih Bro...'; //default message

        $messages = \Setting::get('voucher.messages.facebook');
        if($messages)
        {
            $message = $messages[array_rand($messages, 1)];
        }
        return $message;
    }


    protected function getMessage($item, $scores)
    {
        $score = 0;
        if (isset($scores[$item['name']])) {
            $score = $scores[$item['name']];
        }

        $level = $this->calculator->getLevel($score);

        $message = $this->getMessageByDiscount(\Setting::get("level.$level.discount"));

        foreach($item as $key=>$value)
        {
            $message = str_replace('{' . $key . '}', '<span class="'. $key  .'">' . $value . '</span>', $message);
        }

        return $message;
    }

    public function getMessageByDiscount($discount)
    {
        $message = 'Teman Sejati';

        if (!$this->messages)
        {
            $this->messages = \Setting::get('voucher.messages');
        }

        $messages = array_get($this->messages, $discount);
        if($messages)
        {
            shuffle($messages);
            $message = array_pop($messages);
        }


        return $message;
    }

    protected function encodeAvatar($avatar)
    {
        try {
            return Image::make($avatar)->encode('data-url')->getEncoded();
        } catch (\Intervention\Image\Exception\NotReadableException $e) {

        }
    }

    protected function isAllowRedeem($user)
    {
        //$alreadyRedeem = $this->redeemModel->where('user_id', '=', $user->id)->where(DB::raw('DATE(created_at)'), '=', date('Y-m-d'))->exists();
        //
        //return !$alreadyRedeem;
        return true;
    }

    protected function isUsed($item, $user)
    {
        // method 1: only redeemed user
//        return $this->redeemModel
//            ->where('facebook_friend_tag_id', '=', $item['id'])
//            ->orWhere('facebook_friend_name', '=', $item['name'])
//            ->exists();

        // method 2: all selected friends
        return $this->activity->isAlreadySelected($item, $user);
    }
}
