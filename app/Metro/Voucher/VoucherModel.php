<?php


namespace Metro\Voucher;

use Eloquent, Str;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class VoucherModel extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'vouchers';

    protected $fillable = array('');

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('\Metro\User\UserModel', 'reserved_by');
    }

    public function unlock()
    {
        return $this->belongsTo('\Metro\Invitation\InvitationModel', 'unlock_code_id')->withTrashed();
    }

    public function redeem()
    {
        return $this->hasOne('\Metro\Voucher\RedeemModel', 'voucher_id');
    }

    public static function getCode($length = 14)
    {
        do{
            $code = Str::quickRandom($length);
            $status = self::where('code', $code)->count() > 0 ? true : false;
        }while($status);

        return $code;
    }

    public static function getUniqueCode($length = 10)
    {
        do{
            $pool = '123456789abcdefghijklmnopqrstuvwxyz';

            $code = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);

            $status = self::where('unique_code', $code)->count() > 0 ? true : false;
        }while($status);

        return $code;
    }

    public function getUnlockCodeAttribute()
    {
        if($this->unlock) {
            return $this->unlock['code'];
        }

        return null;
    }

    protected function getSenderNameAttribute()
    {
        if($this->user){
            return $this->user->name;
        }

        return false;
    }

    protected function getSenderEmailAttribute()
    {
        if($this->user){
            return $this->user->email;
        }

        return false;
    }

    public function getGeneratedAtAttribute()
    {
        if($this->redeem) {
            return $this->redeem->created_at->format('d F Y H:i:s');
            if($this->redeem->is_shared == 1) {
                return $this->redeem->updated_at->format('d F Y H:i:s');
            }
        }

        return $this->updated_at->format('d F Y H:i:s');
    }

    protected function getStatusAttribute()
    {
        $status = 'unique link sent';

        if($this->redeem) {
            $status = 'link opened';
            if($this->redeem->is_shared == 1) {
                $status = 'shared';
            }
        }

        return $status;
    }

    protected function getUnlockDateAttribute()
    {
        if($this->unlock) {
            return $this->unlock->updated_at->format('d F Y H:i:s');
        }

        return false;
    }
}
