<?php
namespace Metro\Voucher\ImageGenerator;

use Image;
use DNS1D;
use BarCode39;

class NeoVoucher implements ImageGeneratorInterface
{
    /**
     * @var array
     */
    private $config;

    function __construct(array $config)
    {
        $this->config     = $config;
    }

    public function forPrint($param)
    {
        $userAvatar   = array_get($param, 'user_avatar', null);
        $friendAvatar = array_get($param, 'friend_avatar', null);
        $name         = array_get($param, 'user_name', '');
        $code         = array_get($param, 'code', '');
        $message      = array_get($param, 'message', '');
        $discount     = array_get($param, 'discount', '');

        $voucherPath  = $this->resolveVoucherTemplate($discount);
        $voucher      = Image::make($voucherPath);

        //set path image
        if ($friendAvatar) {
            $maskPath = base_path('assets/img/mask.png');
            $avatar   = Image::make($friendAvatar)->resize(48, 48);
            $avatar->mask($maskPath);
            $this->insert($voucher, $avatar, 'top-left', 275, 163);
        }

        //set path image
        if ($userAvatar) {
            $maskPath = base_path('assets/img/mask.png');
            $avatar   = Image::make($userAvatar)->resize(30, 30);
            $avatar->mask($maskPath);
            $this->insert($voucher, $avatar, 'top-left', 135, 315);
        }

        //write name on voucher
        $this->text($voucher, $name, 175, 322, 30, '#999999', 'left', 'top',  public_path("themes/neo/skins/voucher/yellowtail.otf"));

        //write valid date on voucher
        $validUntil = $this->config['valid_until'];
        $this->text(
            $voucher,
            $validUntil,
            232,
            400,
            11,
            '#666666',
            'left',
            'top',
            public_path("themes/neo/skins/voucher/courier.ttf")
        );

        //write valid place on voucher
        $validIn = $this->config['valid_in'];
        $this->text(
            $voucher,
            $validIn,
            34,
            400,
            11,
            '#666666',
            'left',
            'top',
            public_path("themes/neo/skins/voucher/courier.ttf")
        );

        //create barcode
        $bc = new BarCode39($code);
        $bc->draw(storage_path()."/barcode/".$code.".png");

        //write barcode to voucher
        $this->insert($voucher, storage_path()."/barcode/".$code.".png", 'top-right', 10, 390);

        $this->insertMessagePrint($voucher, $message, $param);

        //return the result
        return $voucher;
    }

    public function forFacebook($param = array())
    {
        $friendAvatar = array_get($param, 'friend_avatar', null);
        $discount     = array_get($param, 'discount', '');
        $message      = array_get($param, 'message', '');
        $name         = array_get($param, 'friend_name', '');

        //base image
        $voucherPath = $this->resolveVoucherTemplate($discount, 'facebook');
        $voucher     = Image::make($voucherPath);

        //set path image
        if ($friendAvatar) {
            $maskPath = base_path('assets/img/mask.png');
            $avatar   = Image::make($friendAvatar)->resize(65, 65);
            $avatar->mask($maskPath);
            $this->insert($voucher, $avatar, 'top-left', 232, 260);
        }

        $this->insertMessageFacebook($voucher, $message, $param);

        $this->text($voucher, $this->config['url'], 250, 440, 9, '#333', 'center', 'top',
            public_path("themes/neo/skins/voucher/Oswald-Regular.ttf"));

        //return the result
        return $voucher;
    }

    private function insert(
        $primaryImage,
        $secondaryImage,
        $position,
        $coorX,
        $coorY
    ) {
        $primaryImage->insert($secondaryImage, $position, $coorX, $coorY);
    }

    private function text(
        $image,
        $text,
        $posX,
        $posY,
        $fontSize,
        $fontColor = '#000000',
        $align = 'center',
        $valign = 'top',
        $fontFamily = 1
    ) {

        $image->text(
            $text,
            $posX,
            $posY,
            function ($font) use (
                $fontSize,
                $fontColor,
                $align,
                $valign,
                $fontFamily
            ) {
                $font->file($fontFamily);
                $font->size($fontSize);
                $font->color($fontColor);
                $font->align($align);
                $font->valign($valign);
            }
        );
    }

    protected function resolveVoucherTemplate($discount, $media = 'print')
    {
        $file = public_path("themes/neo/skins/voucher/$media-$discount.png");

        if (is_file($file)) {
            return $file;
        }

        throw new \InvalidArgumentException('Cannot find voucher template for discount: ' . $discount . ' on ' . $media);
    }

    protected function insertMessageFacebook($voucher, $message, $param)
    {
        // substitute dynamic variable
        foreach($param as $key=>$value)
        {
            $message = str_replace('{' . $key . '}', $value , $message);
        }

        // put name on separate line
        $message = str_replace($param['name'], "\n" . $param['name'] . "\n", $message);
        $temp = explode("\n", $message);

        $lines = [];
        foreach ($temp as $line) {
            $wrap = wordwrap($line, 45, "\n");

            foreach (explode("\n", $wrap) as $text) {
                $lines[] = $text;
            }

        }

        $y = 130;
        foreach ($lines as $line) {

            if (trim($line) == trim($param['name'])) {
                $color = '#cc3333';
            } else {
                $color = '#000000';
            }

            $this->text($voucher, $line, 250, $y, 24, $color, 'center', 'top',
                public_path("themes/neo/skins/voucher/bebas-neue.ttf"));
            $y += 20;

        }
    }

    protected function insertMessagePrint($voucher, $message, $param)
    {
        // substitute dynamic variable
        foreach($param as $key=>$value)
        {
            $message = str_replace('{' . $key . '}', $value , $message);
        }

        // put name on separate line
//        $message = str_replace($param['name'], "\n" . $param['name'] . "\n", $message);

        $temp = explode("\n", $message);
        $lines = [];
        foreach ($temp as $line) {
            $wrap = wordwrap($line, 75, "\n");

            foreach (explode("\n", $wrap) as $text) {
                $lines[] = $text;
            }

        }

        $x = 280;
        $y = 100;
        foreach ($lines as $line) {

            if (trim($line) == trim($param['name'])) {
                $color = '#cc3333';
            } else {
                $color = '#000000';
            }

            $this->text($voucher, $line, $x, $y, 20, $color, 'center', 'top',
                public_path("themes/neo/skins/voucher/bebas-neue.ttf"));
            $y += 20;

        }
    }
}
