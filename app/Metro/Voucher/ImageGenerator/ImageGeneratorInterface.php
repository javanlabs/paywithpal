<?php
namespace Metro\Voucher\ImageGenerator;


interface ImageGeneratorInterface
{

    public function forPrint($params);

    public function forFacebook($params);
}