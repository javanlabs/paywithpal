<?php
namespace Metro\Voucher\ImageGenerator;

use Image;
use DNS1D;

class MetroVoucher implements ImageGeneratorInterface
{
    /**
     * @var array
     */
    private $config;

    function __construct(array $config)
    {
        $this->config = $config;
    }

    public function forFacebook($param)
    {
        $avatarPath = array_get($param, 'avatar', null);
        $name       = array_get($param, 'name', '');
        $discount   = array_get($param, 'discount', '');
        $voucherPath = base_path(
            'assets/img/voucher-facebook-template.png'
        );
        $voucher = Image::make($voucherPath);

        //set path image
        if($avatarPath) {
            $maskPath    = base_path('assets/img/mask.png');
            $avatar      = Image::make($avatarPath)->resize(112, 112);
            $avatar->mask($maskPath);
            $this->insert($voucher, $avatar, 'top-left', 110, 61);
        }

        //write name on voucher
        if (strlen($name) > 15) {
            $text = substr($name, 0, 15) . '...';
        } else {
            $text = $name;
        }
        $this->text($voucher, $text, 245, 210, 30);

        $discountColor = $this->getDiscountColor($discount);
        $this->text(
            $voucher,
            $discount,
            340,
            70,
            90,
            $discountColor,
            'right',
            'top',
            'bold'
        );
        $this->text($voucher, '%', 370, 70, 50, $discountColor);

        //return the result
        return $voucher;
    }

    public function forPrint($param = array())
    {
        $avatarPath  = array_get($param, 'avatar', null);
        $name        = array_get($param, 'name', '');
        $voucherCode = array_get($param, 'code', '');
        $discount    = array_get($param, 'discount', '');

        //base image
        $voucherPath = base_path('assets/img/voucher-print-template.png');
        $voucher     = Image::make($voucherPath);

        if ($avatarPath) {
            //mask the avatar
            $maskPath = base_path('assets/img/mask.png');
            $avatar   = Image::make($avatarPath)->resize(120, 120);
            $avatar->mask($maskPath);

            //paste the avatar to voucher
            $this->insert($voucher, $avatar, 'top-left', 130, 221);
        }

        //create barcode
        $barcode = $this->createBarcode($voucherCode);
        $this->insert($voucher, $barcode, 'top-center', 0, 380);
        $this->text(
            $voucher,
            $voucherCode,
            290,
            415,
            13,
            '#000000',
            'center',
            'top'
        );

        //write name on voucher
        $text = $this->handleHeaderText($name);
        $this->text($voucher, $text, 310, 130, 13);

        // discount
        $discountColor = $this->getDiscountColor($discount);
        $this->text(
            $voucher,
            $discount,
            400,
            230,
            120,
            $discountColor,
            'right',
            'top',
            'bold'
        );
        $this->text($voucher, '%', 440, 230, 60, $discountColor);

        //write valid date on voucher
        $validUntil = $this->config['valid_until'];
        $this->text(
            $voucher,
            $validUntil,
            403,
            468,
            11,
            '#666666',
            'left',
            'top',
            'normal'
        );

        //write valid place on voucher
        $validIn = $this->config['valid_in'];
        $this->text(
            $voucher,
            $validIn,
            107,
            468,
            11,
            '#666666',
            'left',
            'top',
            'normal'
        );

        //return the result
        return $voucher;
    }

    protected function getFontFile($weight)
    {
        $fonts = array(
            'light'  => base_path('assets/fonts/Gotham-Light.ttf'),
            'normal' => base_path('assets/fonts/Gotham-Medium.otf'),
            'bold'   => base_path('assets/fonts/Gotham-Bold.ttf')
        );

        if (isset($fonts[$weight])) {
            return $fonts[$weight];
        } else {
            return 1;
        }
    }

    protected function getDiscountColor($discount)
    {
        if ($discount < 20) {
            $color = '#0071ba';
        } elseif ($discount < 30) {
            $color = '#b11b24';
        } else {
            $color = '#fcbc49';
        }

        return $color;
    }

    private function createBarcode($code, $type = 'C39')
    {
        return DNS1D::getBarcodePNGPath($code, $type, 1.5);
    }

    private function insert(
        $primaryImage,
        $secondaryImage,
        $position,
        $coorX,
        $coorY
    ) {
        $primaryImage->insert($secondaryImage, $position, $coorX, $coorY);
    }

    private function text(
        $image,
        $text,
        $posX,
        $posY,
        $fontSize,
        $fontColor = '#000000',
        $align = 'center',
        $valign = 'top',
        $fontWeight = 'normal'
    ) {
        $fontFamily = $this->getFontFile($fontWeight);

        $image->text(
            $text,
            $posX,
            $posY,
            function ($font) use (
                $fontSize,
                $fontColor,
                $align,
                $valign,
                $fontFamily
            ) {
                $font->file($fontFamily);
                $font->size($fontSize);
                $font->color($fontColor);
                $font->align($align);
                $font->valign($valign);
            }
        );
    }

    private function handleHeaderText($string)
    {
        $length = strlen($string);
        $text = '';
        if ($length > 15) {
            $explode = explode(' ', $string);
            if (sizeof($explode) == 1) {
                $text = substr($string, 0, 15) . '... ';
            } else {
                while ($length > 15) {
                    array_pop($explode);
                    $string = implode(' ', $explode);
                    $length = strlen($string);
                }
                $text = $string;
            }
        }
        $text = $text . ' ADALAH SOHIB YANG PALING BERHARGA';
        $text = strtoupper($text);

        return $text;
    }

}