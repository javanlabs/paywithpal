<?php
namespace Metro\Voucher\ImageGenerator;

use Carbon\Carbon;
use Image;

class McdVoucher
{
    public function uniqueLink($link, $senderName, $receiverName)
    {
        $template = public_path('themes/mcd2/img/email-link.png');
        $image = Image::make($template);

        // link text
        $image->text(
            $link,
            410, // x position
            590, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-Cond.otf"));
                $font->size(18);
                $font->color('#764416');
                $font->align('center');
                $font->valign('top');
            }
        );


        // receiver
        $image->text(
            str_limit(strtoupper('Hi ' . $receiverName), 25, '...'),
            180, // x position
            330, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(32);
                $font->color('#764416');
                $font->align('left');
                $font->valign('top');
            }
        );

        $image->text(
            str_limit($senderName, 16, '...'),
            365, // x position
            466, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(30);
                $font->color('#6E9E43');
                $font->align('left');
                $font->valign('top');
            }
        );

        return $image;
    }

    public function voucherCode($code, $label, $type = 1, $startHour = '18:00', $endHour = '21:00')
    {
        $expiredDate = (new Carbon())->addDays(7);

        $maxExpiredDate = Carbon::createFromDate(2015, 7, 17);

        if($expiredDate > $maxExpiredDate)
        {
            $expiredDate = $maxExpiredDate;
        }

        $template = public_path('themes/mcd2/img/vouchers/' . $type . '.png');
        $image = Image::make($template);

        // Voucher Code
        $image->text(
            strtoupper($code),
            400, // x position
            660, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(30);
                $font->color('#764416');
                $font->align('center');
                $font->valign('top');
            }
        );

        // Berlalu s/d tanggal:
        $image->text(
            'Berlaku s/d tanggal:',
            70, // x position
            460, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-MediumCond.otf"));
                $font->size(22);
                $font->color('#764416');
                $font->align('left');
                $font->valign('top');
            }
        );

        // DAY
        $image->text(
            $expiredDate->format('d'),
            160, // x position
            500, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(120);
                $font->color('#764416');
                $font->align('right');
                $font->valign('top');
            }
        );

        // MONTH
        $image->text(
            strtoupper($expiredDate->format('F')),
            210, // x position
            500, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(50);
                $font->color('#764416');
                $font->align('left');
                $font->valign('top');
            }
        );

        // YEAR
        $image->text(
            $expiredDate->format('Y'),
            210, // x position
            540, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-Cond.otf"));
                $font->size(50);
                $font->color('#764416');
                $font->align('left');
                $font->valign('top');
            }
        );

        // Start Hour
        $image->text(
            $startHour,
            480, // x position
            475, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-Cond.otf"));
                $font->size(50);
                $font->color('#764416');
                $font->align('center');
                $font->valign('top');
            }
        );

        // End Hour
        $image->text(
            $endHour,
            480, // x position
            550, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-Cond.otf"));
                $font->size(50);
                $font->color('#764416');
                $font->align('center');
                $font->valign('top');
            }
        );

        // item
        $image->text(
            str_limit($label, 25, '...'),
            270, // x position
            375, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(32);
                $font->color('#764416');
                $font->align('left');
                $font->valign('top');
            }
        );

        return $image;
    }

    public function shareToEmail($senderName, $receiverName)
    {
        $template = public_path('themes/mcd2/img/email-share.png');
        $image = Image::make($template);

        // sender
        $image->text(
            str_limit(strtoupper($senderName), 20, '...'),
            385, // x position
            320, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(32);
                $font->color('#764416');
                $font->align('center');
                $font->valign('top');
            }
        );

        // receiver
        $image->text(
            str_limit(strtoupper($receiverName), 17, '...'),
            385, // x position
            490, // y position
            function ($font) {
                $font->file(public_path("themes/mcd2/css/fonts/AkzidenzGrotesk-BoldCond.otf"));
                $font->size(32);
                $font->color('#764416');
                $font->align('center');
                $font->valign('top');
            }
        );

        return $image;
    }

    protected function strposOffset($search, $string, $offset)
    {
        /*** explode the string ***/
        $arr = explode($search, $string);
        /*** check the search is not out of bounds ***/
        switch( $offset )
        {
            case $offset == 0:
                return false;
                break;

            case $offset > max(array_keys($arr)):
                return false;
                break;

            default:
                return strlen(implode($search, array_slice($arr, 0, $offset)));
        }
    }
}
