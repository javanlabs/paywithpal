<?php


namespace Metro\Voucher\Calculator;


class RandomCalculator implements CalculatorInterface {

    public function getDiscount($score)
    {
        $discounts = [0, 100, 200, 300];
        return $discounts[array_rand($discounts)];
    }

    public function getLevel($score)
    {
        $levels = [1,2,3,4,5];
        return $levels[array_rand($levels)];
    }

}
