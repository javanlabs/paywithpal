<?php


namespace Metro\Voucher\Calculator;

use Metro\Exception\DiscountNotFoundException;
use Setting;

class DbCalculator implements CalculatorInterface
{
    public function getDiscount($score)
    {
        $level = $this->getLevel($score);

        if ( ! Setting::has("level.$level.discount"))
        {
            throw new DiscountNotFoundException('Unable to calculate discount for a score of ' . $score);
        }

        return Setting::get("level.$level.discount");
    }

    public function getLevel($score)
    {
        if($score > 700)
            $level = 5;
        elseif($score >= 500)
            $level = 4;
        elseif($score >= 300)
            $level = 3;
        elseif($score >= 100)
            $level = 2;
        else
            $level = 1;

        return $level;
    }
}