<?php


namespace Metro\Voucher\Calculator;


class RangeCalculator implements CalculatorInterface {

    public function getDiscount($score)
    {
        if($score > 700)
            $discount = 50;
        elseif($score >= 500)
            $discount = 40;
        elseif($score >= 300)
            $discount = 30;
        elseif($score >= 100)
            $discount = 20;
        else
            $discount = 10;

        return $discount;
    }

    public function getLevel($score)
    {
        if ($score > 700) {
            $level = 5;
        } elseif ($score >= 500) {
            $level = 4;
        } elseif ($score >= 300) {
            $level = 3;
        } elseif ($score >= 100) {
            $level = 2;
        } else {
            $level = 1;
        }

        return $level;
    }

}