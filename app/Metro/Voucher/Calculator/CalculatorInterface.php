<?php


namespace Metro\Voucher\Calculator;


interface CalculatorInterface
{
    public function getDiscount($score);

    public function getLevel($score);
}