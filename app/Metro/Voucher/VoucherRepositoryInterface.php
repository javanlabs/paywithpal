<?php


namespace Metro\Voucher;


interface VoucherRepositoryInterface {

    public function find($hash);

    public function all($page = 1, $keyword = null, $perPage = 9);

    public function my($user, $page = 1, $keyword = null, $perPage = 9);

    public function pickOne();

    public function loadFromSelection($user, $selection, $scores);

    public function redeem($user, $voucher, $params);

    public function countRedeemed($user = null);

    public function countUnused();

    public function paginateUnused();

    public function paginateRedeemed();
}