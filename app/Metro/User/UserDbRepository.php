<?php


namespace Metro\User;

use Carbon\Carbon;
use Illuminate\Support\MessageBag;
use Metro\Exception\ValidationException;
use Metro\FacebookHelper;
use Metro\Invitation\InvitationRepositoryInterface;
use Session;
use Cache;
use Auth;
use Metro\User\UserModel as User;
use Hash;
use DB;

class UserDbRepository implements UserRepositoryInterface
{

    protected $model;

    protected  $facebookHelper;

    protected $validator;

    function __construct(UserModel $model, FacebookHelper $facebookHelper, UserValidator $validator)
    {
        $this->model          = $model;
        $this->facebookHelper = $facebookHelper;
        $this->validator      = $validator;
    }

    public function loginOrCreate($authResponse)
    {

        $profile = $this->facebookHelper->getProfile(
            $authResponse['accessToken']
        );

        if (!$profile) {
            return false;
        }

        $data = [
            'name'                  => $profile->getProperty('name'),
            'email'                 => $profile->getProperty('email'),
            'facebook_id'           => $profile->getProperty('id'),
            'facebook_link'         => $profile->getProperty('link'),
            'facebook_access_token' => $authResponse['accessToken'],
            'last_login'            => Carbon::now(),
        ];

        $user = $this->model->where('facebook_id', '=', $data['facebook_id'])->orWhere('email', '=', $data['email'])->first();
        if($user)
        {
            $user->update($data);
        }
        else
        {
            $user = $this->model->create($data);
        }

        return $user->toArray();
    }

    public function findOrCreateViaEmail($credentials)
    {
        $user = $this->model->where('email', $credentials['email'])->first();

        $data = [
            'name' => $credentials['name'],
            'email' => $credentials['email'],
            'last_login' => Carbon::now()
        ];

        if($user){
            $user->update($data);
        }else{
            $user = $this->model->create($data);
        }

        return $user;
    }

    public function logout($user)
    {
        return Auth::logout();
    }

    public function friends($user)
    {
        $friends = $this->facebookHelper->getFriends($user->facebook_access_token);

        $friends->sort(function($a, $b){
            $name1 = $a['name'];
            $name2 = $b['name'];
            return $this->facebookHelper->trimName($name1) > $this->facebookHelper->trimName($name2);
        });

        return $friends;
    }

    public function friendsCached($user)
    {
        // cache for 1 hour using key
        $key = 'friends-' . $user->id . date('YmdH');

        return Cache::rememberForever($key, function() use ($user)
        {
            return $this->friends($user);
        });
    }

    public function score($user, $facebookId)
    {
        Auth::logout();
    }

    public function saveSelection($data)
    {
        return Session::put('friends_selection', $data);
    }

    public function loadSelection()
    {
        return Session::get('friends_selection');
    }

    public function clearSelection()
    {
        return Session::forget('friends_selection');
    }

    public function login($key, $password)
    {
        $remember = false;
        $credentials = array('email' => $key, 'password' => $password);
        return Auth::attempt($credentials);
        return Auth::attempt(array('email' => $key, 'password' => $password), $remember);
    }

    public function paginate()
    {
        return $this->model->orderBy(DB::raw('FIELD(role, "admin", "shopper")'))->paginate();
    }

    public function countShopper()
    {
        return $this->model->whereRole(User::ROLE_SHOPPER)->count();
    }

    public function countAdmin()
    {
        return $this->model->whereRole(User::ROLE_ADMIN)->count();
    }

    public function updatePassword($user, $input)
    {
        $v = UserValidator::make($input)->addContext('update_password')->bindReplacement('current_password', ['password' => $user->password]);

        if ($v->passes()) {
            $user->password = Hash::make($input['password']);
            $user->save();
            return $user;
        }

        throw new ValidationException($v->errors());
    }

    public function addAdmin($input)
    {
        $v = UserValidator::make($input)->addContext('add_admin');

        if ($v->passes()) {
            $user = $this->model->whereEmail($input['email'])->first();
            if ($user) {
                $user->password = Hash::make($input['password']);
                $user->role = User::ROLE_ADMIN;
                $user->save();
            }
            return $user;
        }

        throw new ValidationException($v->errors());
    }

    public function removeAdmin($authUser, $id)
    {

        $user = $this->model->find($id);

        if (!$user) {
            throw new ValidationException(
                new MessageBag(['Invalid User'])
            );
        }

        if ($user->id == $authUser->id) {
            throw new ValidationException(
                new MessageBag(['Cannot remove yourself from admin list. Please ask other admin.'])
            );
        }

        $user->role     = User::ROLE_SHOPPER;
        $user->save();
        return $user;
    }
}