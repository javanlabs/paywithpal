<?php

namespace Metro\User;

use Crhayes\Validation\ContextualValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserValidator extends ContextualValidator
{
    protected $rules = [
        'default' => [
            'password'              => 'required|min:6',
        ],
        'update_password' => [
            'current_password'      => 'required|same_hash:@password',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required'
        ],
        'add_admin' => [
            'email'                 => 'required|email|exists:users,email',
        ],
    ];

    protected $messages = [
        'current_password.same_hash' => 'Wrong current password'
    ];

    function __construct($attributes = null, $context = null)
    {
        parent::__construct($attributes, $context);

        Validator::extend('same_hash', function($attribute, $value, $parameters)
        {
            if( ! Hash::check( $value , $parameters[0] ) )
            {
                return false;
            }
            return true;
        });
    }

}