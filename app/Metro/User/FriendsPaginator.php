<?php


namespace Metro\User;


class FriendsPaginator {

    const PER_PAGE = 10;

    public function paginate($data, $page = 1, $name = null)
    {
        if($name)
        {
            $data = $data->filter(function($item) use ($name){
                return strpos(strtolower($item['name']), strtolower($name)) !== false;
            });
        }

        if(!$page)
        {
            $page = 1;
        }
        $page = abs($page);

        $start = ($page - 1) * static::PER_PAGE;
        $data = $data->slice($start, static::PER_PAGE);

        return $data;
    }
}