<?php


namespace Metro\User;


use Metro\Invitation\InvitationRepositoryInterface;

interface UserRepositoryInterface {

    public function loginOrCreate($authResponse);

    public function login($key, $password);

    public function findOrCreateViaEmail($credentials);

    public function logout($user);

    public function friends($user);

    public function score($user, $facebookId);

    public function saveSelection($data);

    public function loadSelection();

    public function clearSelection();

    public function paginate();

    public function countAdmin();

    public function countShopper();

    public function updatePassword($user, $input);
}