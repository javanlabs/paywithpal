<?php


namespace Metro\User;

use Eloquent;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserModel extends Eloquent implements UserInterface, RemindableInterface
{
    use UserTrait, RemindableTrait;

    const ROLE_SHOPPER  = 'shopper';
    const ROLE_ADMIN    = 'admin';

    protected $table = 'users';

    protected $fillable = array('name', 'email', 'facebook_id', 'facebook_access_token', 'facebook_link', 'last_login');

    protected $appends = array('is_admin', 'avatar');

    public function getIsAdminAttribute()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    public function getAvatarAttribute()
    {
        $id = $this->facebook_id;
        return "https://graph.facebook.com/$id/picture";
    }
}