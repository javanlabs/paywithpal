<?php namespace Metro\Core;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Setting;
use Facebook\FacebookSession;

class ServiceProvider extends BaseServiceProvider {

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        FacebookSession::setDefaultApplication($this->app->make('setting')->get('facebook.app_id'), $this->app->make('setting')->get('facebook.app_secret'));
    }

    /**
    * Register
    */
    public function register()
    {

        $this->app->bind(
            'Metro\User\UserRepositoryInterface',
            'Metro\User\UserDbRepository'
        );

        $this->app->bind(
            'Metro\Voucher\VoucherRepositoryInterface',
            'Metro\Voucher\VoucherDbRepository'
        );

        $this->app->bind(
            'Metro\Activity\ActivityRepositoryInterface',
            'Metro\Activity\ActivityDbRepository'
        );

        $this->app->bind(
            'Metro\Voucher\Calculator\CalculatorInterface',
            'Metro\Voucher\Calculator\RandomCalculator'
        );

        $this->app->bind('voucherGenerator', function($app){
            return new \Metro\Voucher\ImageGenerator\NeoVoucher(Setting::get('voucher'));
        });

        $this->app->bind(
            'Metro\Invitation\InvitationRepositoryInterface',
            'Metro\Invitation\InvitationRepository'
        );
    }


}
