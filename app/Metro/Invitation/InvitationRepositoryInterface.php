<?php namespace Metro\Invitation;

use Metro\User\UserModel;

interface InvitationRepositoryInterface {

    public function isValidCode($code);

    public function findByCode($code);

    public function reservedBy(UserModel $user, $code);

    public function paginateUnused();

    public function paginateUsed();

    public function countUnused();

    public function countUsed();

    public function bulkInsert($data);

    public function release($id);

    public function delete($code);
}
