<?php namespace Metro\Invitation;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Metro\User\UserModel;

class InvitationRepository implements InvitationRepositoryInterface{

    private $model;

    public function __construct(InvitationModel $model)
    {
        $this->model = $model;
    }

    public function isValidCode($code)
    {
        $invitation = $this->model->whereCode($code)->first();

        if($invitation){
            return true;
        }

        return false;
    }

    public function findByCode($code)
    {
        return $this->model->with('user')->whereCode($code)->first();
    }

    public function reservedBy(UserModel $user, $code)
    {
        $this->model->whereCode($code)->update(['reserved_by' => $user->id]);
    }

    public function paginateUnused()
    {
        return $this->model->unused()->paginate(100);
    }

    public function paginateUsed()
    {
        return $this->model->used()->with('voucher')->withTrashed()->orderBy('updated_at', 'ASC')->paginate();
    }

    public function countUnused()
    {
        return $this->model->unused()->count();
    }

    public function countUsed()
    {
        return $this->model->used()->count();
    }

    public function bulkInsert($data)
    {
        $now = Carbon::now()->toDateTimeString();
        $inserted = $invalid = 0;
        $duplicates = [];

        foreach($data as $code)
        {
            $code = array_pop($code);
            $insertData = array(
                'code'       => $code,
                'created_at' => $now,
            );

            $exist = $this->model->whereCode($code)->exists();

            if(!$exist)
            {
                DB::table($this->model->getTable())->insert($insertData);
                $inserted++;
            }
            else
            {
                $duplicates[] = $code;
                $invalid++;
            }

        }

        return [$inserted, $invalid, $duplicates];
    }

    public function release($id)
    {
        $model = $this->model->withTrashed()->where('id', $id);

        return $model->restore() && $model->update(['reserved_by' => null]);
    }

    public function delete($code)
    {
        $this->model->where('code', $code)->delete();
    }
}
