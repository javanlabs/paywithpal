<?php namespace Metro\Invitation;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class InvitationModel extends Eloquent{

    use SoftDeletingTrait;

    protected $table = 'invitations';

    protected $dates = ['deleted_at'];

    public function scopeUsed($query)
    {
        return $query->withTrashed()->whereNotNull("reserved_by");
    }

    public function scopeUnused($query)
    {
        return $query->whereNull("reserved_by");
    }

    public function user()
    {
        return $this->belongsTo('Metro\User\UserModel', 'reserved_by')->addSelect('id', 'name', 'email');
    }

    public function voucher()
    {
        return $this->hasOne('Metro\Voucher\VoucherModel', 'unlock_code_id')->orderBy('updated_at', 'DESC');
    }

    public function getReservedByNameAttribute()
    {
        if($this->user)
        {
            return $this->user->name;
        }

        return false;
    }

    public function getReservedByEmailAttribute()
    {
        if($this->user)
        {
            return $this->user->email;
        }

        return false;
    }

    public function getReservedTimeAttribute()
    {
        if($this->updated_at)
        {
            return $this->updated_at->format('d F Y H:i:s');
        }

        return false;
    }

    public function getFriendNameAttribute()
    {
        if($this->voucher) {
            return $this->voucher['friend_name'];
        }

        return false;
    }

    public function getFriendEmailAttribute()
    {
        if($this->voucher) {
            return $this->voucher['friend_email'];
        }

        return false;
    }
}
