<?php namespace Metro\Mail;

use Mail;

abstract class Mailer {

    public function sendTo($email, $subject, $view, $data = [])
    {
        Mail::send($view, $data, function($message) use ($email, $subject)
        {
            $message->to($email)->subject($subject);
        });
    }

}
