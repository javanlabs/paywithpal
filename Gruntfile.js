module.exports = function(grunt) {

    grunt.registerTask('watch', [ 'watch' ]);

    grunt.initConfig({
        less: {
            metro: {
                compress:true,
                files: {
                    "public/themes/metro/css/bootstrap.css": "assets/less/bootstrap/bootstrap.less",
                    "public/themes/metro/css/metro.css": "assets/less/metro/metro.less",
                    "public/themes/metro/css/print.css": "assets/less/metro/print.less"
                }
            },
            neo: {
                compress: true,
                files: {
                    "public/themes/neo/css/bootstrap.css": "public/themes/neo/less/bootstrap/bootstrap.less",
                    "public/themes/neo/css/neo.css": "public/themes/neo/less/neo/neo.less",
                    "public/themes/neo/css/print.css": "public/themes/neo/less/neo/print.less"
                }
            }
        },
        copy: {
            metro: {
                files: [
                    {expand: true, flatten:true, src: ['public/vendor/font-awesome-4.1.0/fonts/*', 'public/vendor/bootstrap-3.2.0/fonts/*'], dest: 'public/themes/metro/fonts/', filter: 'isFile'},
                ]
            },
            neo: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['public/vendor/font-awesome-4.1.0/fonts/*', 'public/vendor/bootstrap-3.2.0/fonts/*'],
                        dest: 'public/themes/neo/fonts/',
                        filter: 'isFile'
                    },
                ]
            },
        },
        concat: {
            jsVendor: {
                src: [
                    'public/vendor/jquery.min.js',
                    'public/vendor/bootstrap-3.2.0/js/bootstrap.min.js',
                ],
                dest: 'public/compiled/jquery-bootstrap.min.js'
            },
            jsFrontend: {
                src: [
                    'assets/js/frontend.js',
                    'public/vendor/jquery.slimscroll.min.js',
                    'public/vendor/jquery.countdown.min.js',
                    'public/vendor/bootstrap-progressbar.min.js'
                ],
                dest: 'public/compiled/frontend.js'
            },
            cssNeo: {
                src: [
                    'public/themes/neo/css/bootstrap.css',
                    'public/vendor/font-awesome-4.1.0/css/font-awesome.css',
                    'public/themes/neo/css/neo.css',
                ],
                dest: 'public/themes/neo/css/neo.css'
            },
            cssMetro: {
                src: [
                    'public/themes/metro/css/bootstrap.css',
                    'public/vendor/font-awesome-4.1.0/css/font-awesome.min.css',
                    'public/themes/metro/css/metro.css',
                ],
                dest: 'public/themes/metro/css/frontend.css'
            },
        },
        uglify: {
            options: {
                mangle: true
            },
            js: {
                files: {
                    'public/compiled/frontend.min.js': ['public/compiled/frontend.js'],
                }
            }
        },
        cssmin: {
            metro: {
                src: 'public/themes/metro/css/frontend.css',
                dest: 'public/themes/metro/css/frontend.min.css'
            },
            neo: {
                src: 'public/themes/neo/css/neo.css',
                dest: 'public/themes/neo/css/neo.min.css'
            }
        },
        watch: {
            js: {
                files: ['assets/js/*.js'],
                tasks: ['concat:jsVendor', 'concat:jsFrontend', 'uglify:js'],
            },
            lessMetro: {
                files: [
                    'assets/less/metro/*',
                    'assets/less/bootstrap/*'
                ],
                tasks: [ 'less:metro', 'concat:cssMetro', 'cssmin:metro']
            },
            lessNeo: {
                files: [
                    'public/themes/neo/less/neo/*',
                    'public/themes/neo/less/bootstrap/*',
                    'public/themes/neo/less/bootstrap/mixins/*'
                ],
                tasks: ['less:neo', 'concat:cssNeo', 'cssmin:neo']
            },
            cssNeo: {
                files: [
                    'public/themes/neo/css/neo.min.css'
                ],
                options: {
                    livereload: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');


    // Task definition
    grunt.registerTask('default', ['watch']);

};
