<?php

return array(
    'PROMO_END_DATE' => '2015-07-12',

    'APP_URL'       => 'http://paywithpal.dev',
    'APP_DEBUG'      => true,
    'APP_THEME'      => 'metro',

    'DB_HOST'        => 'localhost',
    'DB_DATABASE'    => 'paywithpal',
    'DB_USERNAME'    => 'root',
    'DB_PASSWORD'    => 'secret',

    'APP_FORCE_SSL' => false,

    'FB_APP_ID'      => '642881582478819',
    'FB_APP_SECRET'  => '02e827828d7e1f1c6d3d785c25b2807b',

    'SMTP_HOST'      => 'mailtrap.io',
    'SMTP_PORT'      => 25,
    'SMTP_USERNAME'  => '32530a520ef9b0589',
    'SMTP_PASSWORD'  => '8ab519dc067dae',

    'ROLLBAR_TOKEN' => '',
    'ROLLBAR_LEVEL' => 'error',

    'MAIL_FROM_ADDRESS' => 'no-reply@paywithpal.dev',
    'MAIL_FROM_NAME' => 'Paywithpal'
);
