@extends('layouts.main')

@section('content')
<div id="page-gate" class="text-center">
    <div class="headline">
        <img src="{{ theme_asset('skins/pay-with-pal.png') }}" class="img-responsive center-block">
    </div>
    <div class="description">
        <img src="{{ theme_asset('skins/gate.png') }}" alt="" class="img-responsive center-block"/>
    </div>
    <div class="center-block">
        <div class="box-like">
            <div class="button">
                <fb:like href="http://www.facebook.com/{{ Setting::get('facebook.page_id') }}" send="false" layout="button" width="250" show_faces="false"></fb:like>
            </div>
        </div>
    </div>
</div>
<style>
    .fb_iframe_widget{
        overflow: hidden;
        position: inherit;
    }
</style>
@stop

@section('script-fb-init')
    FB.Event.subscribe('edge.create', function(response) {
        window.location.replace("{{ route('voucher.index') }}");
    });
    @if(!$isAuth)

    @endif
@stop