@extends('layouts.main')

@section('content')
<div id="page-voucher">
    <div class="headline">
        <img src="{{ theme_asset('skins/pay-with-pal-small.png') }}" alt="" class="img-responsive center-block">
    </div>
    <div class="welcome">
        <img src="{{ theme_asset('skins/welcome.png') }}" alt="" class="img-responsive center-block"/>
    </div>

    <div class="mb text-center">
        <button id="btn-show-friends" class="btn btn-metro btn-primary btn-dust">
            Cek Harga Temen Loe <i class="fa fa-long-arrow-right"></i>
        </button>
    </div>

    <div class="section-voucher section-tile">
        <div class="mb clearfix">
            <div class="col-md-3 text-center">
                <div class="btn-group btn-group-sort">
                  <button type="button" class="btn btn-text btn-default">Latest Voucher</button>
                  <button type="button" class="btn btn-caret btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ $route }}">Latest Voucher</a></li>
                  </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="title text-center">
                    @if($vouchers)
                    <span class="count">{{ $voucherCount }}</span>
                    <img src="{{ theme_asset('skins/teman-udah-dipake-belanja.png') }}" class="" alt=""/>
                    @else
                    <span class="empty">Voucher Tidak Ditemukan</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <form class="form-search-voucher" id="form-search-voucher">
                    <div class="right-inner-addon inner">
                        <i class="icon-search glyphicon glyphicon-search"></i>
                        <input id="input-search-friends" type="text" name="keyword" value="{{ Input::get('keyword') }}" class="form-control input keyword" placeholder="Search" />
                    </div>
                </form>
            </div>
        </div>


        @if($vouchers)
            <div class="voucher-list container-fluid">
                <div class="row items">
                    @foreach($vouchers as $item)
                    @include('voucher.item')
                    @endforeach
                </div>
            </div>

            <div class="text-center">
                <a href="{{ $route }}" class="btn btn-metro btn-more btn-arrow">Load More</a>
            </div>
        @endif

    </div>

</div>

@include('modals.friends')

@stop
