<div class="col-md-4 col-sm-4">
    <div class="item">
        <table>
            <tr>
                <td class="discount text-center">
                    <img src="{{ $item['friend_avatar'] }}" alt="" class="img-circle img-responsive img-avatar discount-{{ $item['discount'] }}">
                    <div class="nominal discount-{{ $item['discount'] }}">
                        {{ substr($item['discount'], 0, -1) }}<span class="dummy">0</span>
                    </div>
                    <div class="unit">Ribu</div>
                </td>
                <td>
                    <div class="friend el">
                        {{ $item['friend_name'] }}
                    </div>
                    <div class="user">
                        <strong class="el">Dibuat Belanja Oleh</strong>
                        <div class="el name"><img src="{{ $item['user_avatar'] }}" alt="" class="img-circle img-responsive img-avatar"> <span>{{ $item['user_name'] }}</span></div>
                    </div>
                    <div class="date">
                        {{ $item['date'] }}
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>