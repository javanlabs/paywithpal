<div class="col-sm-4 item-big">
    <div class="voucher discount-{{ $discount }} clearfix">
        <div class="head">
            <img src="{{ theme_asset('skins/arrow-discount.png') }}" alt="" class="arrow"/>
            Voucher
        </div>
        <div class="text">
            {{ $item['message'] }}
        </div>
        <div class="discount">
            <div class="rp"><img src="{{ theme_asset('skins/rp.png') }}" alt=""/></div>
            <div class="nominal">
                <img src="{{ $item['avatar'] }}" alt="" class="img-circle img-responsive img-avatar">
                {{ substr($discount, 0, -1) }}<span class="dummy">10</span>
            </div>
            <div class="unit">ribu</div>
        </div>
        <div class="share hidden">
            <span class="up">Share: </span> &nbsp; <i class="fa fa-2x fa-facebook-square theme-facebook"></i> <i class="fa fa-2x fa-twitter-square theme-twitter"></i>
        </div>
    </div>
    <div class="redeem text-center">
        @if($discount > 0)
            @if($item['used'])
                <button disabled="disabled" class="btn disabled btn-sm btn-metro btn-secondary">Telah Digunakan</button>
            @else
            <form method="post" action="{{ route('voucher.redeem') }}" class="form-redeem">
                <input type="hidden" name="id" value="{{ $item['pivot']['id'] }}">
                <input type="hidden" name="avatar" value="{{ $item['avatar'] }}">
                <input type="hidden" name="tag_id" value="{{ $item['tag_id'] }}">
                <input type="hidden" name="name" value="{{ $item['name'] }}">
                <input type="hidden" name="level" value="{{ $item['level'] }}">
                <input type="hidden" name="message" value="{{{ $item['message'] }}}">
                <button type="submit" class="btn btn-sm btn-metro btn-primary btn-redeem">Gunakan Voucher</button>
            </form>
            @endif
        @endif
    </div>
</div>