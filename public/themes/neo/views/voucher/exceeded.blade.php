@extends('layouts.main')

@section('content')
    <div id="page-voucher-exceeded" class="section-tile">
        <div class="container-fluid">
            <h3 class="text-center up">Maaf, kesempatan mendapatkan voucher untuk hari ini sudah habis. <br>Silakan coba lagi esok hari.</h3>
        </div>
    </div>
@stop