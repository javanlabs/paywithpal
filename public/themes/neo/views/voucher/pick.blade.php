@extends('layouts.main')

@section('content')
<div id="page-voucher-pick">
    <div class="headline">
        <img src="{{ theme_asset('skins/pay-with-pal-small.png') }}" alt="" class="img-responsive center-block">
    </div>
    <div class="description pad">
        <img src="{{ theme_asset('skins/pilih-voucher.png') }}" alt="" class="img-responsive center-block">
    </div>

    <div class="section-voucher section-tile">
        <div class="voucher-list container-fluid" style="min-height: 400px">
        </div>

        <div class="after-pick hidden">
            <h3>Terima kasih, voucher juga sudah dikirim ke email kamu.</h3>
        </div>

    </div>
</div>

@include('modals.voucher')
@include('modals.loading')
@stop

@section('script-end')
<script type="text/javascript">
    $(function(){
        var fetchComplete = false;
        var loadingComplete = false;

        $('#modal-loading-selection .progress .progress-bar').progressbar({
            transition_delay: 1000,
            display_text: 'fill',
            done: function($this){
                loadingComplete = true;
                $this.addClass('progress-bar-striped active');
            }
        });

        $('#modal-loading-selection').modal('show');
        $('.voucher-list').css('opacity', 0);

        loadSelection(function(response){
            var height = 0;
            $('#page-voucher-pick .item-big').each(function(idx, elm){
                var target = $(this).find('.text');
                height = Math.max(height, target.height());
            });
            $('#page-voucher-pick .item-big .text').height(height);

            fetchComplete = true;

        });

        setInterval(function(){
            if(loadingComplete) {
                $('#modal-loading-selection .progress .progress-bar').html('Preparing your voucher...');
            }
            if(fetchComplete && loadingComplete) {
                $('.voucher-list').css('opacity', 1);
                $('#modal-loading-selection').modal('hide');
            }
        }, 500);

        $('#modal-voucher').on('hidden.bs.modal', function(e){
            window.location.href = '{{ route('voucher.index') }}';
        });

        var loadingIndex = 0;
        // loop loading text
        var loadingTextTimer = setInterval(function(){

            if (loadingComplete) {
                clearInterval(loadingTextTimer);
                return false;
            }

            loadingIndex ++;
            if(loadingIndex > 2) {
                loadingIndex = 1;
            }
            var text = '';
            var img = '';
            switch(loadingIndex) {
                case 1:
                text = getRandom(window.loadingText.likes);
                img = '{{ theme_asset('skins/loading-like.png') }}';
                break;
                case 2:
                text = getRandom(window.loadingText.comments);
                img = '{{ theme_asset('skins/loading-comment.png') }}';
                break;
            }
            $('#modal-loading-selection .text-loading').html(text);
            $('#modal-loading-selection .img-loading').attr('src', img);

        }, 2000);
    });

    var loadingText = {
        comments: ['Mengevaluasi frekuensi cela2an', 'Merekapitulasi frekuensi komen', 'Mengevaluasi intensitas komen'],
        likes: ['Mengkalkulasi jumlah like', 'Merekap total jempol di status', 'Mengecek frekuensi like pada foto yang sama', 'Merekap jumlah like pada status yang sama', 'Mengevaluasi jempol di status']
    };

    function getRandom(array) {
      return array[Math.floor(Math.random() * array.length)];
    }
</script>
@stop