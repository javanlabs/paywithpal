<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="id"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="id"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="id"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="id"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="{{ theme_asset_hashed('css/neo.min.css') }}">
        <link rel="stylesheet" href="{{ theme_asset_hashed('css/print.css') }}" type="text/css" media="print" />

        <link href='//fonts.googleapis.com/css?family=Lato:300,400,700|Oswald:300,400|Bad+Script|Montserrat' rel='stylesheet' type='text/css'>

        <script src="{{ asset('vendor/modernizr-2.6.2.min.js') }}"></script>
    </head>

    <body class="{{ $bodyClass or '' }}">
        @include('partials.alert')
        @yield('body')
    </body>

</html>
