@extends('layouts.main')

@section('content')
    <div id="page-gate" class="text-center">
        <div class="headline">
            <img src="{{ theme_asset('skins/pay-with-pal.png') }}" alt="" class="pay-with-pal">
        </div>
        <div class="description">
            <img src="{{ theme_asset('skins/gate.png') }}" alt="" class="img-responsive center-block"/>
        </div>
        <div>
            <a href="{{ route('voucher.index') }}" class="btn btn-metro btn-primary btn-dust">Cari Harga Teman Loe Di Sini <i class="fa fa-long-arrow-right"></i></a>
        </div>
    </div>

@stop