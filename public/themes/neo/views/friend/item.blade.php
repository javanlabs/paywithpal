<div class="col-md-6 col-sm-6 col-xs-12 item" id="friend-item-{{ $item['id'] }}">
    <table>
        <tr>
            <td class="action">
                <button type="button" class="btn btn-metro btn-selection btn-select-friend" data-avatar="{{ $item['avatar'] }}" data-name="{{ $item['name'] }}" data-id="{{ $item['id'] }}" data-toggle="tooltip" data-placement="top" title="PILIH">
                    Piih
                </button>
            </td>
            <td class="avatar">
                <img src="{{ $item['avatar'] }}" class="img-circle img-avatar" alt="">
            </td>
            <td class="name">
                <h5 class="el">{{ $item['name'] }}</h5>
            </td>
        </tr>
    </table>
</div>
