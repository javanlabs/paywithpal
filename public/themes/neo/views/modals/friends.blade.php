<!-- Modal -->
<div class="modal-friends fade modal" id="modal-friends" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-selected=0 data-url='{{ route('friends') }}'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <a class="btn btn-link btn-close btn-sm" data-dismiss="modal">Close [x]</a>
                <h3 class="title">Pilih 3 Teman Untuk Ditukarkan Dengan Voucher</h3>
            </div>

            <div class="modal-body">
                <form method="post" action="{{ route('friends.select') }}" class="clearfix form-selected-friends">

                    <div class="selected-friends text-center">
                        <div class="items">
                            @foreach(range(0, 2) as $id)
                            <div class="item" id="selected-friend-{{ $id }}">
                                <img src="data:image/gif;base64,R0lGODdhZABkAIAAAMzMzJaWliwAAAAAZABkAAACc4SPqcvtD6OctNqLs968+w+G4kiW5omm6sq27gvH8kzX9o3n+s73/g8MCofEovGITCqXzKbzCY1Kp9Sq9YrNarfcrvcLDovH5LL5jE6r1+y2+w2Py+f0uv2Oz+v3/L7/DxgoOEhYaHiImKi4yNjo+AhpWAAAOw==" class="img-circle avatar" alt="">
                                <h5 class="name el" data-empty="Belum Dipilih">Belum Dipilih</h5>
                                <a class="btn-remove-friend" href="#" data-id="{{ $id }}" data-friend-id="">[x]</a>
                                <input type="hidden" class="input-id" name="selection[{{ $id }}][id]" >
                                <input type="hidden" class="input-name" name="selection[{{ $id }}][name]" >
                                <input type="hidden" class="input-avatar" name="selection[{{ $id }}][avatar]" >
                            </div>
                            @endforeach
                        </div>
                    </div>
                </form>

                <div class="clearfix">
                    <form class="col-md-12 form-search-friends" id="form-search-friends">
                        <div class="right-inner-addon ">
                            <i class="icon-loading fa fa-refresh fa-spin"></i>
                            <i class="icon-search glyphicon glyphicon-search"></i>
                            <input id="input-search-friends" type="text" name="keyword" class="form-control input keyword" placeholder="Search" />
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <div id="box-friend-empty" class="hidden alert alert-danger">Teman tidak ditemukan atau kamu tidak mengizinkan aplikasi ini untuk melihat daftar temanmu.</div>
                </div>

                <div class="scrollable section-friends"  data-scroll-height="300px" id="section-friends">
                    <div class="text-center up empty">
                        Memuat Daftar Teman...
                    </div>
                </div>
                <div class="text-center">
                    <button type="button" class="btn btn-metro btn-arrow btn-arrow-dark btn-more">Load More</button>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-metro btn-secondary btn-cancel btn-sm" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-metro btn-primary btn-sm btn-next">Lanjut</button>
            </div>
        </div>
    </div>
</div>

<div class="modal-token-exceeded modal fade" id="modal-token-exceeded" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{ theme_asset('skins/coba-lagi.png') }}" alt="" class="img-responsive center-block"/>
                <div id="countdown" class="counter">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-metro btn-sm btn-primary" data-dismiss='modal'>Close</button>
            </div>
        </div>
    </div>
</div>

@section('script-fb-init')

    var fbConnected = false;
    var fbResponse;

    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            fbConnected     = true;
            fbResponse      = response;
        }
    });

    $('#btn-show-friends').on('click', function(e){

        e.preventDefault();

        if (fbConnected){
            showModal(fbResponse, '{{ route('site.login') }}');
            ga('send', 'pageview', '/vp/FBC-LogIn');
        } else {
            ga('send', 'pageview', '/vp/FBC-Registrant');
            FB.login(function(fbResponse) {
                if(fbResponse.status === 'connected'){
                    showModal(fbResponse, '{{ route('site.login') }}');
                }
            }, {
               scope: 'public_profile, email, user_friends, publish_actions, user_status',
               return_scopes: true
            });
        }
    });

    $('#modal-friends').on('click', '.item', function(e){
        $(e.currentTarget).find('.btn-select-friend').trigger('click');
    });

    $('#modal-friends').on('hidden.bs.modal', function(){
        if(window.isLogin && !window.isTokenExceeded) {
            window.location.reload();
        }
    });

@stop

@section('script-end')
<script type="text/javascript">

    @if(isset($usedFriends))
    var usedFriends = {{ json_encode($usedFriends) }};
    @endif

    $(function(){
        $('#modal-friends').on('click', '.btn-next', function(){
            $('#modal-friends .form-selected-friends').submit();
        });
    });
</script>
@stop