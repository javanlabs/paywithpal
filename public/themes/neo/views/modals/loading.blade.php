<div class="modal-loading-selection fade modal" id="modal-loading-selection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-selected=0 data-url='{{ route('voucher.selection') }}' data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content text-center">
            <div class="modal-header section-tile">
                <img src="{{ theme_asset('skins/sedang-proses.png') }}" alt=""/>
            </div>
            <div class="modal-body">
                <div class="friends">
                    @foreach($selections as $friend)
                        @if($friend['avatar'])
                            <img src="{{ $friend['avatar'] }}" alt="" class="img-circle img-avatar"/>
                        @endif
                    @endforeach
                </div>
                <img src="{{ theme_asset('skins/loading-photo.png') }}" alt="" class="img-loading"/>
                <img src="{{ theme_asset('skins/loading-like.png') }}" alt="" class="hidden"/>
                <img src="{{ theme_asset('skins/loading-comment.png') }}" alt="" class="hidden"/>
                <img src="{{ theme_asset('skins/loading-mutual.png') }}" alt="" class="hidden"/>
                <h3 class="title text-loading">Menghitung jumlah foto bareng...</h3>
            </div>
            <div class="modal-footer section-tile">
                <div class="progress">
                    <div class="progress-bar progress-timer progress-bar-danger" data-transitiongoal="100"></div>
                </div>
            </div>
        </div>
    </div>
</div>