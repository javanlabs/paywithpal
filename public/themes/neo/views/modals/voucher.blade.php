<!-- Modal -->
<div class="modal fade modal-voucher" id="modal-voucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-selected=0>
    <div class="modal-dialog">
        <div class="modal-content section-tile">
            <div class="modal-body">
                <div class="mb"><img src="{{ theme_asset('skins/print-voucher.png') }}" alt="" class="title img-responsive"/></div>
                <div class="voucher-container">
                    <img src="" class="img-voucher img-responsive" alt="">
                </div>
            </div>
            <div class="modal-footer text-center">
                <a class="btn btn-metro btn-sm btn-secondary btn-close-voucher" href="#" data-dismiss="modal">Close</a>
                <a class="btn btn-metro btn-sm btn-primary btn-print-voucher" href="javascript:window.print()">Print</a>
            </div>
        </div>
    </div>
</div>
