<footer>
    <div class="links">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <a href="#modal-term" data-toggle="modal">Syarat dan Ketentuan</a>
                    |
                    <a href="#modal-privacy" data-toggle="modal">Kebijakan Privasi</a>
                </div>
                <div class="col-sm-6 copyright">
                    <a href="#modal-copyright" data-toggle="modal">&copy; 2014 PT Metropolitan Retailmart</a>
                </div>
            </div>
        </div>
    </div>
</footer>

@include('modals.static.disclaimer')
@include('modals.static.privacy')
@include('modals.static.term')
@include('modals.static.copyright')