<header>
    <div class="navbar" role="navigation">
      <div class="container-fluid">
        <div class="row">
        <div class="navbar-header">

            <div class="pull-left" style="margin-top:3px">
                <a href="http://www.metroindonesia.com/" target="_blank" class="btn-logo-metro"><img class="visible-xs-inline" src="{{ theme_asset('skins/logo.png') }}" alt=""></a>
            </div>

          @if($isAuth)
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          @endif
        </div>
        <div class="collapse navbar-collapse">

            @if($isAuth)
            <ul class="nav navbar-nav menu up">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                <li><a href="{{ route('voucher.index') }}">All Vouchers</a></li>
                <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                <li><a href="{{ route('voucher.my') }}">My Vouchers</a></li>
                <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                <li class="logout"><a href="{{ route('site.logout') }}">Logout</a></li>
            </ul>
            @endif
            <a href="http://www.metroindonesia.com/" target="_blank"  class="btn-logo-metro">
                <img class="pull-right visible-sm visible-md visible-lg" src="{{ theme_asset('skins/logo.png') }}" alt="">
            </a>
        </div><!--/.nav-collapse -->
        </div>

      </div>
    </div>
</header>