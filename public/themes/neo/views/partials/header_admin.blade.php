<header>
    <div class="navbar navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="row">
                <div class="navbar-header">

                    <div class="pull-left" style="margin-top:3px">
                        <a data-toggle="modal" href="#modal-copyright"><img class="visible-xs-inline" src="{{ theme_asset('skins/logo.png') }}" alt=""></a>
                    </div>

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav menu up">
                        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i></a></li>
                        <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                        <li><a href="{{ route('admin.voucher') }}">Vouchers</a></li>
                        <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                        <li><a href="{{ route('admin.user') }}">Users</a></li>
                        <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                        <li><a href="{{ route('admin.setting') }}">Settings</a></li>
                    </ul>

                    <ul class="nav navbar-nav menu menu-user pull-right">
                        <li><a href="{{ route('admin.password') }}"><i class="fa fa-circle-o" style="color:#090"></i>&nbsp; <strong>Login as {{ $authUser['name'] }}</strong></a></li>
                        <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                        <li class="logout up"><a href="{{ route('admin.logout') }}">Logout</a></li>
                    </ul>

                </div><!--/.nav-collapse -->
            </div>

        </div>
    </div>
</header>