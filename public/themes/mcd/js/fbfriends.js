var friends = new Array;
var friendsFiltered = new Array;
var friendsPerPage = 6;
var friendsCurrentPage = 1;
var friendsAjax = null;
var friendsSearchTimer;
var friendsLoaded = false;
var isLogin = false;
var redeemAjax = null;
var friendsSelected = new Array;
var isTokenExceeded = false;
var currentKeyword = '';

$(function(){

    //showFriends(1, '', function(){
    ////                        $('#modal-friends .btn-more').button('reset').removeClass('loading');
    //    $('.btn-more').removeAttr('disabled').text('Load More');
    //});

    $('.btn-more').on('click', function(e){

        //ga('send', 'event', 'LoadParticipant', 'Click', 'Click');
        //
        //if(friendsSearching) {
        //    return false;
        //}

        var btn = $(e.currentTarget);
        //var modal = $(e.delegateTarget);

        friendsCurrentPage++;
        //btn.button('loading').addClass('loading');
        btn.attr('disabled', true).text('Loading..');
        showFriends(friendsCurrentPage, $('#keyword').val(), function(){
            //btn.button('reset').removeClass('loading');
            btn.removeAttr('disabled').text('Load More');
        });
    });

    $(".form-search-friends").on('submit', function (e) {
        e.preventDefault();
    });

    $('#friend-lists').on('click', '.well', function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        var avatar = $(this).data('avatar');
        var modal = $('#modal-selected-friend');
        modal.modal('show');
        modal.find('input[name="friend_id"]').val(id);
        modal.find('input[name="friend_name"]').val(name);
        modal.find('input[name="friend_avatar"]').val(avatar);
        modal.find('img').attr('src', avatar);
        modal.find('#selected-friend-name').text(name.toUpperCase());
    });

});

function paginate(data, page) {
    var start = (page - 1) * friendsPerPage;
    var result = data.slice(start, start + friendsPerPage);
    return result;
}

function appendFriends(data, page, keyword) {

    if (page == 1) {
        $('#friend-lists').html('');
    }
    //$('#box-friend-empty').addClass('hidden');

    if(data.length == 0) {
        if (page == 1) {
            //$('#box-friend-empty').removeClass('hidden');
            //alert('Teman tidak ditemukan atau kamu tidak mengizinkan aplikasi ini untuk melihat daftar temanmu.');
        } else {
            alert('Tidak ada lagi teman');
            $('.btn-more').remove();
        }
        return false;
    }

    var items = $('<div class="row items">');

    for(var i=0;i<data.length;i++) {
        var item = $('<div class="col-md-6 col-sm-6 col-xs-12 item"></div>').attr('id', 'friend-item-' + data[i].id);
        var inner = $('<div class="inner well"></div>');
        var table = $('<table>');
        var tr = $('<tr>');
        //var button = $('<button data-toggle="tooltip" data-placement="top" title="PILIH">')
        //    .addClass('btn btn-default btn-selection btn-select-friend')
        //    .data('id', data[i].id)
        //    .data('name', data[i].name)
        //    .data('avatar', data[i].picture.data.url);
        inner.attr('data-id', data[i].id).attr('data-name', data[i].name).attr('data-avatar', data[i].picture.data.url);

        if(friendsSelected[data[i].id] === true) {
            button.addClass('selected');
        }

        //if(usedFriends.indexOf(data[i].name) != -1) {
        //    button.addClass('used');
        //    item.addClass('used');
        //    button.attr('title', 'SUDAH PERNAH DIPAKE');
        //}

        var avatar = $('<img>').addClass('img-avatar').attr('src', data[i].picture.data.url);
        var name = $('<h4>').addClass('el').html(data[i].name);

        //var left = $('<td>').addClass('action').append(button);
        var center = $('<td>').addClass('avatar').append(avatar);
        var wrapper = $('<div style="width:200px">').append(name);
        var right = $('<td>').addClass('name').append(wrapper);
        tr.append(center).append(right);
        table.append(tr);
        inner.append(table);
        item.append(inner);
        item.css('opacity', 0);
        items.append(item);
        item.css('opacity', 1);
    }

    $('#friend-lists').append(items);
    //$('#modal-friends .btn-select-friend').tooltip({container:'body'});
    //
    //var scrollHeight = $('#section-friends').prop('scrollHeight') + 'px';
    //$('#section-friends').slimScroll({scrollTo : scrollHeight });
}

function showFriends(page, keyword, onDone){

    if(friends.length == 0) {

        function handleFriends(response){

            if (response && response.data) {

                for(var i=0; i<response.data.length; i++){
                    friends.push(response.data[i]);
                }

                if (response.paging.next){
                    FB.api(response.paging.next, handleFriends);
                } else {

                    friendsFiltered = paginate(filterByKeyword(friends, keyword), page);
                    appendFriends(friendsFiltered, page, keyword);
                    if($.isFunction(onDone)){
                        onDone();
                    }
                    friendsLoaded = true;
                }

            }
        }

        FB.api("me/taggable_friends", {
            fields: "id,name,picture.width(210).height(210)"
        }, handleFriends);
    } else {

        friendsFiltered = paginate(filterByKeyword(friends, keyword), page);
        appendFriends(friendsFiltered, page, keyword);
        if($.isFunction(onDone)){
            onDone();
        }
    }

}

function filterByKeyword(data, keyword) {

    var result = new Array;
    var keyword = keyword.toLowerCase();

    if(keyword != "") {
        for(var i=0;i<data.length;i++) {
            var name = data[i].name.toLowerCase();
            if(name.indexOf(keyword) != -1) {
                result.push(data[i]);
            }
        }
    } else {
        result = data;
    }

    result.sort(function(a, b){
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    });

    return result;
}

friendsSearchTimer = setInterval(function(){

    if(!friendsLoaded) {
        return false;
    }

    var form = $('#form-search-friends');
    var keyword = form.find('#keyword').val();
    if(keyword != currentKeyword) {
        currentKeyword = keyword;
        friendsCurrentPage = 1;
        //form.addClass('loading');
        //form.find('.btn-more').button('loading');
        //$('.btn-more').attr('disabled', true).text('Loading...');
        friendsFiltered = paginate(filterByKeyword(friends, keyword), 1);
        appendFriends(friendsFiltered, 1, keyword);
    }
},800);