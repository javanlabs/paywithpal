@extends('layouts.base')

@section('body')
    <div class="col-md-12 col-distance">
        {{ Form::open(array('route' => 'invite.check')) }}
        <div class="form-group">
            {{ Form::text('code', Input::old('code'), ['class' => 'form-control input-lg input-invite-code', 'autocomplete' => 'off', 'placeholder' => 'masukkan kode disini']) }}
        </div>
        <div class="form-group text-center">
            <div class="btn-group btn-group-code" role="group">
                @if(Setting::get('route') == 'facebook')
                <button type="submit" class="btn btn-primary btn-lg" name="register-via" value="facebook">SUBMIT</button>
                @else
                <button type="submit" class="btn btn-primary btn-lg" name="register-via" value="email">SUBMIT</button>
                @endif
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <div class="col-md-12">
        Mekanisme:
        <ol>
            <li>Dapatkan Kupon GRATIS Ramadhan untuk setiap pembelian Paket McD Ramadhan.</li>
            <li>Dapatkan voucher GRATIS Ramadhan untuk setiap pembelian Paket McD Ramadhan.</li>
            <li>Voucher Ramadhan hanya dapat diperoleh dari pembelian McD Ramadhan, mulai tanggal 10-30 April 2015.</li>
            <li>Masukkan email kamu yang masih valid karena voucher akan dikirimkan ke email kamu.</li>
            <li>Tukarkan voucher tersebut ke store McDonald's terdekat.</li>
            <li>
                Voucher tidak berlaku di McDonald's:
                <ol type="a" style="margin-left: -23px;">
                    <li>Lippo Karawaci</li>
                    <li>Plaza Senayan</li>
                    <li>Dufan</li>
                    <li>Depok Mall</li>
                    <li>Cempaka Mas</li>
                    <li>Mall Taman Anggrek</li>
                    <li>Voucher hanya berlaku dengan menunjukkan bukti email dan tidak berlaku dalam bentuk print out.</li>
                </ol>
            </li>
        </ol>
    </div>
@stop
