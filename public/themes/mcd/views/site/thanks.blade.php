@extends('layouts.base')

@section('body')
    <div class="col-md-12">
        <div id="thanks" class="text-center">
            <div>{{ $redeem->user->name }}</div>
            <div>mengucapkan terima kasih kepada</div>
            <div>{{ $redeem->voucher->user->name }}</div>
            <div class="col-distance"></div>
            <a href="{{ route('route2.home') }}" class="btn btn-primary btn-lg btn-lg2">SAYA MAU IKUTAN</a>
        </div>
    </div>
@stop