@extends('layouts.main')

@section('content')
    <div id="finish" class="col-md-8 col-md-offset-2 text-center">
        <div>SELAMAT</div>
        <div>VOUCHER SUDAH DIKIRIMKAN</div>
        @if(Setting::get('route') == 'facebook')
        <div class="well text-left">
            <div class="row">
                <div class="col-sm-4">
                    <img class="img-responsive" src="{{ Session::pull('friend_avatar', ''); }}">
                </div>
                <div class="col-sm-8">{{ Session::pull('friend_name', ''); }}</div>
            </div>
        </div>
        @else
        <div class="well">
            <div>{{ Session::pull('friend_name', ''); }}</div>
            <div>({{ Session::pull('friend_email', ''); }})</div>
        </div>
        @endif
    </div>
@stop