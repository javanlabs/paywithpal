@extends('layouts.main')

@section('content')
    <div id="fb-root"></div>
    <div class="col-md-10 col-md-offset-1 text-center">
        <button type="button" class="btn btn-primary btn-lg btn-facebook" style=""><i class="fa fa-facebook fa-fw"></i> MASUK DENGAN FACEBOOK</button>
    </div>
@stop

@section('script')
    @parent
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            }
        });

        var login = function(authResponse){
            $.post('{{ route('register.fb') }}', {authResponse: authResponse}).done(function(data){
                if(data.status == 1){
                    window.location.assign(data.redirect_url);
                }else{
                    alert("Gagal login");
                }
            }).fail(function(){
                alert("Some error");
            });
        };

        // Facebook
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '{{ Setting::get('facebook.app_id') }}',
                xfbml      : true,
                version    : 'v2.1'
            });

            FB.Canvas.setAutoGrow();

            var fbConnected = false;
            var fbResponse;

            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    fbConnected     = true;
                    fbResponse      = response;
                }
            });

            $('.btn-facebook').on('click', function(){
                if (fbConnected){
                    login(fbResponse.authResponse);
                } else {
                    FB.login(function(fbResponse) {
                        if(fbResponse.status === 'connected'){
                            login(fbResponse.authResponse);
                        }
                    }, {
                        scope: 'public_profile, email, user_friends, publish_actions',
                        return_scopes: true
                    });
                }

            });

        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/id_ID/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@stop