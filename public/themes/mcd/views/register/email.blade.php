@extends('layouts.main')

@section('content')
    <div id="fb-root"></div>
    <div class="col-md-6 col-md-offset-3">
        <div class="text-center" id="fb-connect">
            <button type="button" class="btn btn-primary btn-lg btn-facebook" style=""><i class="fa fa-facebook fa-fw"></i> SAMBUNGKAN DENGAN FACEBOOK</button>
        </div>

        {{ Form::open(array('route' => 'route2.register', 'id' => 'register-form', 'class' => 'hidden')) }}
        <div class="form-group">
            {{ Form::text('name', Input::old('name'), ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA KAMU']) }}
        </div>
        <div class="form-group">
            {{ Form::email('email', Input::old('email'), ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN EMAIL KAMU']) }}
        </div>
        <div class="form-group text-center">
            {{ Form::submit('SUBMIT', ['class' => 'btn btn-success btn-lg btn-lg2']) }}
        </div>
        {{ Form::close() }}
    </div>
@stop

@section('script')
    @parent
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            }
        });

        var fbConnect = function(authResponse){
            $.post('{{ route('route2.fb.connect') }}', {authResponse: authResponse}).done(function(data){
                if(data.status == 1){
                    $('#fb-connect').addClass('hidden');
                    var form = $('#register-form');
                    form.find('input[name="name"]').val(data.profile.name);
                    form.find('input[name="email"]').val(data.profile.email);
                    form.removeClass('hidden');
                }else{
                    alert("Gagal menyambungkan dengan facebook");
                }
            }).fail(function(){
                alert("Some error");
            });
        };

        // Facebook
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '{{ Setting::get('facebook.app_id') }}',
                xfbml      : true,
                version    : 'v2.1'
            });

            FB.Canvas.setAutoGrow();

            var fbConnected = false;
            var fbResponse;

            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    fbConnected     = true;
                    fbResponse      = response;
                }
            });

            $('.btn-facebook').on('click', function(){
                if (fbConnected){
                    fbConnect(fbResponse.authResponse);
                } else {
                    FB.login(function(fbResponse) {
                        if(fbResponse.status === 'connected'){
                            fbConnect(fbResponse.authResponse);
                        }
                    }, {
                        scope: 'public_profile',
                        return_scopes: true
                    });
                }
            });

        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/id_ID/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@stop