<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <style>
            body{font-family: 'Open Sans', Arial;}
            #footer{margin: 40px 0 20px 0;}
            #footer ul{list-style: none;padding: 0;margin: 0;}
            #footer ul > li {display: inline-block;margin-left: 8px;}
            #tagline{font-size: 36px; font-weight: 700; margin-top: 40px; text-transform: uppercase;}
            .mar-bottom-30{margin-bottom: 30px;}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img src="{{ asset("/images/mcd-logo.jpg") }}" width="140" alt="McD Logo">
                </div>
                <div class="col-xs-9">
                    <div id="tagline">SELAMAT HARI LEBARAN!</div>
                </div>
                <div class="clearfix mar-bottom-30"></div>
                <div class="col-xs-12 text-center">
                    <img src="{{ asset('ecards/email/'.$ecard->image_path) }}">
                </div>
                <div class="clearfix mar-bottom-30"></div>
                <div class="col-xs-12">
                    <p>Halo {{ $ecard->friend_name }},</p>
                    <p>{{ $ecard->message }}</p>
                    <p>Salam,<br> {{ $ecard->me }}</p>
                    <p>Copyright &copy; 2015 McDonald's Indonesia. All rights reserved <br>www.mcdonald.co.id/kartuucapan</p>
                </div>
                <div class="col-xs-12 text-center">
                    <div id="footer">
                        <ul>
                            <li><a href="#">Terms & Condition</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>