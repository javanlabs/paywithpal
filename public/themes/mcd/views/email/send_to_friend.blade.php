@extends('layouts.mail')

@section('content')
    <div class="well-mail">
        <div>{{ $sender }}</div>
        <div>Baru saja mengirimkan voucher kepada</div>
        <div class="mar-bottom-20">{{ $receiver }}</div>
        <div>Klik link dibawah untuk me-redeem voucher</div>
        <div><a href="{{ $link }}">Link Voucher</a></div>
    </div>
@stop