@extends('layouts.mail')

@section('content')
    <div class="well-mail">
        <span id="voucher">{{ $voucher }}</span>
    </div>
    <div id="thanks-text">
        <div>{{ $from }}</div>
        <div>mengucapkan terima kasih kepada</div>
        <div>{{ $to }}</div>
    </div>
@stop