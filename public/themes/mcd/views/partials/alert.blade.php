@if(Session::has('success') || Session::has('error') || Session::has('validcode'))
    <div class="col-md-12 col-distance text-center">
    @if(Session::has('validcode'))
        <div class="alert-code text-success"><i class="fa fa-check"></i> Kode Valid</div>
    @endif

    @if(Session::has('success'))
        <div class="alert-code text-success"><i class="fa fa-check"></i> {{ Session::get('success') }}</div>
    @elseif(Session::has('error'))
        <div class="alert-code text-danger"><i class="fa fa-times"></i> {{ Session::get('error') }}</div>
    @endif
    </div>
@endif