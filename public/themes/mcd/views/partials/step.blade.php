<div class="col-md-12 col-distance">
    <div class="stepwizard">
        <div class="stepwizard-row">
            <div class="stepwizard-step">
                <a href="#" class="btn btn-{{{ $btnClass[1] or 'default' }}} btn-circle" disabled="disabled">1</a>
                <p>Daftar</p>
            </div>
            <div class="stepwizard-step">
                <a href="#" class="btn btn-{{{ $btnClass[2] or 'default' }}} btn-circle" disabled="disabled">2</a>
                <p>Pilih Teman</p>
            </div>
            <div class="stepwizard-step">
                <a href="#" class="btn btn-{{{ $btnClass[3] or 'default' }}} btn-circle" disabled="disabled">3</a>
                <p>Kirim E-Card</p>
            </div>
            <div class="stepwizard-step">
                <a href="#" class="btn btn-{{{ $btnClass[4] or 'default' }}} btn-circle" disabled="disabled">4</a>
                <p>Selesai</p>
            </div>
        </div>
    </div>
</div>