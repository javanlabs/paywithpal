@if(!$friends->isEmpty())
<div class="row items">
    @foreach($friends as $item)
        @include('friend.item')
    @endforeach
</div>
@endif