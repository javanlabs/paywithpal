@extends('layouts.main')

@section('content')
    <div class="col-md-12">
        {{ Form::open(array('route' => 'friends.ecard', 'id' => 'ecard-form')) }}
        <div class="row">
            <div class="col-sm-6">
                <h2>Dari</h2>
                <div class="form-group">
                    {{ Form::text('name', Auth::user()->name, ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA KAMU']) }}
                </div>
                <div class="form-group">
                    {{ Form::email('email', Auth::user()->email, ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN EMAIL KAMU']) }}
                </div>
                <div class="form-group">
                    {{ Form::text('location', Input::old('location'), ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'LOKASI MCD TERDEKAT']) }}
                </div>
            </div>
            <div class="col-sm-6">
                <h2>Kepada</h2>
                <div class="form-group">
                    {{ Form::text('friend_name', Session::get('friend_name'), ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA TEMAN KAMU']) }}
                </div>
                <div class="form-group">
                    {{ Form::email('friend_email', Session::get('friend_email'), ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN EMAIL TEMAN KAMU']) }}
                </div>
                <div class="form-group">
                    {{ Form::textarea('message', Input::old('message'), ['class' => 'form-control input-lg', 'placeholder' => 'MASUKKAN PESAN', 'rows' => 5]) }}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="carousel slide media-carousel" id="ecard-image">
                    {{ Form::hidden('ecard_tpl') }}
                    <div class="carousel-inner">
                        @foreach($eCardImages as $image)
                        <div class="item {{ $image['active'] or null }}" data-slide-number="{{ $image['slide_number'] }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="thumbnail" href="#"><img alt="" src="{{ $image['img_src'] }}"></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <a data-slide="prev" href="#ecard-image" class="left carousel-control">‹</a>
                    <a data-slide="next" href="#ecard-image" class="right carousel-control">›</a>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="carousel slide media-carousel" id="ecard-thumb-image">
                    <div class="carousel-inner">
                        @foreach($eCardThumbImages as $thumbs)
                        <div class="item {{ $thumbs['active'] or null }}" data-slide-number="{{ $thumbs['slide_number'] }}">
                            <div class="row">
                                @foreach($thumbs['images'] as $image)
                                <div class="col-md-3">
                                    <a class="thumbnail" href="#" data-thumb-id="{{ $image['thumb_number'] }}"><img alt="" src="{{ $image['img_src'] }}"></a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <a data-slide="prev" href="#ecard-thumb-image" class="left carousel-control">‹</a>
                    <a data-slide="next" href="#ecard-thumb-image" class="right carousel-control">›</a>
                </div>
            </div>
            <div class="col-sm-12 text-center">
                {{ Form::submit('LANJUT', ['class' => 'btn btn-success btn-lg btn-lg2']) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
@stop

@section('script')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            var setSrcEcardSelected = function(){
                var eCardSelected = $('#ecard-image').find('.item.active').find('a > img').attr('src');
                $('input[name="ecard_tpl"]').val(eCardSelected);
            };

            $('#ecard-image, #ecard-thumb-image').carousel({
                pause: true,
                interval: false
            });
            setSrcEcardSelected();

            $('#ecard-image').on('slid.bs.carousel', function () {
                var slideNumber = $(this).find('.item.active').data('slide-number');
                var thumbItemCurrent = $('#ecard-thumb-image .item a[data-thumb-id='+(parseInt(slideNumber))+']').closest('.item').data('slide-number');
                $('#ecard-thumb-image').carousel(parseInt(thumbItemCurrent));

                setSrcEcardSelected();
            });

            $('#ecard-thumb-image .item a').on('click', function(e){
                e.preventDefault();
                var id = $(this).data('thumb-id');
                $('#ecard-image').carousel(parseInt(id));

                setSrcEcardSelected();
            });
        });
    </script>
@stop