@extends('layouts.main')

@section('content')
    <div id="fb-root"></div>
    <div class="col-md-10 col-md-offset-1">
        <form class="form-search-friends" id="form-search-friends">
        {{ Form::text('keyword', null, ['id' => 'keyword', 'class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA TEMAN KAMU']) }}
        </form>
    </div>
    <div class="clearfix col-distance"></div>
    <div class="col-md-10 col-md-offset-1" id="friend-lists">
        <h3 class="text-center">Memuat daftar teman...</h3>
    </div>
    <div class="col-md-10 col-md-offset-1 text-center">
        <button type="button" class="btn btn-default btn-lg btn-metro btn-secondary btn-more" disabled="disabled">Loading..</button>
    </div>

    @include('modals.selected_friend')
@stop

@section('script')
    @parent
    <script src="{{ theme_asset('js/fbfriends.js') }}"></script>
    <script>
    $(function(){

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '{{ Setting::get('facebook.app_id') }}',
                xfbml      : true,
                version    : 'v2.1'
            });

            FB.Canvas.setAutoGrow();

            var fbConnected = false;
            var fbResponse;

            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    fbConnected     = true;
                    fbResponse      = response;
                }

                if(fbResponse.status === 'connected'){

                    showFriends(1, '', function(){
//                        $('#modal-friends .btn-more').button('reset').removeClass('loading');
                        $('.btn-more').removeAttr('disabled').text('Load More');
                    });

                }
            });

        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/id_ID/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    });
    </script>
@stop