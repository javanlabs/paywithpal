@extends('layouts.main')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        {{ Form::open(array('route' => 'friends.select')) }}
        <div class="form-group">
            {{ Form::text('name', Input::old('name'), ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA TEMAN KAMU']) }}
        </div>
        <div class="form-group">
            {{ Form::email('email', Input::old('email'), ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN EMAIL TEMAN KAMU']) }}
        </div>
        <div class="form-group text-center">
            {{ Form::submit('SUBMIT', ['class' => 'btn btn-success btn-lg btn-lg2']) }}
        </div>
        {{ Form::close() }}
    </div>
@stop