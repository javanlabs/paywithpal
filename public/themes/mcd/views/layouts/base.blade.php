<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="id"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="id"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="id"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="id"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>McD Paywithpal</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="{{ theme_asset_hashed('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ theme_asset_hashed('css/frontend.css') }}">

        <link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="{{ asset('vendor/font-awesome-4.1.0/css/font-awesome.min.css') }}">
        <script src="{{ asset('vendor/modernizr-2.6.2.min.js') }}"></script>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="row">
                        <div class="col-md-2 col-sm-4">
                            <img src="{{ asset("/images/mcd-logo.jpg") }}" class="img-responsive" alt="McD Logo">
                        </div>

                        <div class="col-md-10 col-sm-8 text-center">
                            <div id="tagline">Bukaan dari teman</div>
                        </div>

                        <div class="clearfix col-distance"></div>

                        <div class="col-md-4 col-sm-6">
                            <img src="{{ asset("/images/burger-vector.jpg") }}" id="burger-img" class="img-responsive" alt="Burger">
                        </div>

                        <div class="col-md-8 col-sm-6 text-center" id="voucher">
                            <div id="voucher-count">{{ $countRedeem }}</div>
                            <div id="voucher-count-text">Voucher Sudah Dikirimkan</div>
                        </div>

                        <div class="clearfix col-distance"></div>
                        @include('partials.alert')
                        @yield('body')

                        <div id="footer" class="col-md-12">
                            <div class="pull-right">
                                <ul>
                                    <li><a href="#modal-term" data-toggle="modal">Terms & Condition</a></li>
                                    <li><a href="#modal-privacy" data-toggle="modal">Privacy Policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('modals.static.privacy')
        @include('modals.static.term')

        @section('script')
            <script src="{{ asset_hashed('compiled/jquery-bootstrap.min.js') }}"></script>
        @show
    </body>
</html>
