@extends('layouts.base')

@section('body')
    @include('partials.step')
    @yield('content')
@stop