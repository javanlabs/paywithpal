<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <style>
            body{font-family: 'Open Sans', Arial;}
            .well-mail{margin: 30px 0;background-color: #f2f2f2;padding: 30px 25px;font-size: 24px;}
            #thanks-text{font-size: 24px;}
            #footer{margin: 40px 0 20px 0;}
            #footer ul{list-style: none;padding: 0;margin: 0;}
            #footer ul > li {display: inline-block;margin-left: 8px;}
            #tagline{font-size: 36px; font-weight: 700; margin-top: 40px; text-transform: uppercase;}
            #voucher{font-size: 52px; font-weight: 700}
            .mar-bottom-20{margin-bottom: 20px;}
        </style>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <img src="{{ asset("/images/mcd-logo.jpg") }}" width="140" alt="McD Logo">
            </div>
            <div class="col-xs-8">
                <div id="tagline">Bukaan dari teman</div>
            </div>
            <div class="clearfix mar-bottom-20"></div>
            <div class="col-xs-12 text-center">
                <img src="https://t1.ftcdn.net/jpg/00/46/39/00/400_F_46390039_t8q1nBvAwqwdz7lc3VtQUqhgGj1o8HLr.jpg" width="200"/>
                @yield('content')
                <div id="footer">
                    <ul>
                        <li><a href="#">Terms & Condition</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>