<div class="modal fade" id="modal-selected-friend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        {{ Form::open(['route' => 'route1.friends.select']) }}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body text-center">
                {{ Form::hidden('friend_id', null) }}
                {{ Form::hidden('friend_name', null) }}
                {{ Form::hidden('friend_avatar', null) }}
                <img/>
                <h4><span id="selected-friend-name"></span> AKAN MENERIMA VOUCHER RAMADHAN?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary">Benar</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>