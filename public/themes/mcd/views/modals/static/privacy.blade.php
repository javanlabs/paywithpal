<!-- Modal -->
<div class="modal fade modal-privacy modal-static-content" id="modal-privacy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-selected=0>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="up">Kebijakan Privasi</h4>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                {{ Setting::get('pages.privacy') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-metro btn-primary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
