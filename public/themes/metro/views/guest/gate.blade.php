@extends('layouts.main')

@section('content')
<div id="page-voucher" class="text-center">
    <div class="big-screen">
        <div class="headline">
            <img src="{{ theme_asset('skins/pay-with-pal-white-small.png') }}" alt="" class="pay-with-pal hidden-xs">
            <h2 class="tagline">Kalo Beneran <i>Sohib</i> <br> Pasti Saling Menghargai</h2>
            <h3 class="caption">
                Sekarang Teman Bisa Dipake Belanja, <br>Karena Teman Facebook Loe Bisa Dijadiin Diskon Di Metro.
                <br>
                <span>
                    Semakin Deket Loe Ama Temen Loe, <br>Semakin Gede Diskonnya.
                </span>
            </h3>

            <div class="center-block">
                <fb:like href="http://www.facebook.com/{{ Setting::get('facebook.page_id') }}" send="false" layout="standard" width="250" show_faces="false"></fb:like>
            </div>

        </div>
    </div>

</div>
@stop

@section('script-fb-init')
    FB.Event.subscribe('edge.create', function(response) {
        window.location.replace("{{ route('voucher.index') }}");
    });
@stop