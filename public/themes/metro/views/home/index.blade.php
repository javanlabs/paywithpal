@extends('layouts.main')

@section('content')
    <div id="page-home" class="text-center">
        <div class="big-screen">
            <img src="{{ theme_asset('skins/pay-with-pal.png') }}" alt="" class="pay-with-pal">
        </div>
        <div class="headline">
            <h2 class="tagline">Kalo Beneran Sohib <br> Pasti Saling Menghargai</h2>

            <p class="caption">
                Sekarang Teman Bisa Dipake Belanja.
                <br>
                <span>
                    Semakin Deket Loe Ama Temen Loe, <br>Semakin Gede Diskonnya.
                </span>
            </p>

            <div>
                <a href="{{ route('voucher.index') }}" class="btn btn-metro btn-primary">
                    Klik Disini Untuk Memulai
                    <i class="fa fa-caret-right"></i>
                </a>
            </div>

        </div>
    </div>

    @include('modals.friends')

@stop
