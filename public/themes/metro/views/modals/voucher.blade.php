<!-- Modal -->
<div class="modal fade modal-voucher" id="modal-voucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-selected=0>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-9">
                        <strong class="up">Selamat! Kamu Mendapatkan Voucher Diskon 30% Untuk Berbelanja di Metro Dept. Store</strong>
                    </div>
                    <div class="col-md-3 text-center">
                        <a class="btn btn-primary" href="javascript:window.print()">Print</a>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="voucher-container">
                    <img src="" class="img-voucher img-responsive" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
