<!-- Modal -->
<div class="modal-friends modal fade" id="modal-friends" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-selected=0 data-url='{{ route('friends') }}'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-xs-6">
                        <img src="{{ theme_asset('skins/pay-with-pal-small.png') }}" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-6 text-right">
                        <strong class="title">Pilih 3 Teman <br>Yang Paling Berharga</strong>
                    </div>
                </div>

                <div class="row">
                    <form method="post" action="{{ route('friends.select') }}" class="clearfix form-selected-friends">

                        <div class="selected-friends text-center col-md-8">
                            <div class="items">
                                @foreach(range(0, 2) as $id)
                                <div class="item" id="selected-friend-{{ $id }}">
                                    <img src="data:image/gif;base64,R0lGODdhZABkAIAAAMzMzJaWliwAAAAAZABkAAACc4SPqcvtD6OctNqLs968+w+G4kiW5omm6sq27gvH8kzX9o3n+s73/g8MCofEovGITCqXzKbzCY1Kp9Sq9YrNarfcrvcLDovH5LL5jE6r1+y2+w2Py+f0uv2Oz+v3/L7/DxgoOEhYaHiImKi4yNjo+AhpWAAAOw==" class="img-circle avatar" alt="">
                                    <h5 class="name el">Belum Dipilih</h5>
                                    <a class="btn-remove-friend" href="#" data-id="{{ $id }}" data-friend-id="">[x]</a>
                                    <input type="hidden" class="input-id" name="selection[{{ $id }}][id]" >
                                    <input type="hidden" class="input-name" name="selection[{{ $id }}][name]" >
                                    <input type="hidden" class="input-avatar" name="selection[{{ $id }}][avatar]" >
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class=" col-md-4 next">
                            <button type="submit" class="btn btn-metro btn-primary">Lanjut <i class="fa fa-caret-right"></i></button>
                            <button type="button" class="btn btn-link btn-cancel btn-block" data-dismiss="modal">Batal</button>
                        </div>
                    </form>

                    <form class="col-md-12 form-search-friends" id="form-search-friends">
                        <div class="right-inner-addon ">
                            <i class="icon-loading fa fa-refresh fa-spin"></i>
                            <i class="icon-search glyphicon glyphicon-search"></i>
                            <input id="input-search-friends" type="text" name="keyword" class="form-control input keyword" placeholder="Search" />
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-body scrollable section-friends"  data-scroll-height="300px" id="section-friends">
                <div class="row items">
                    Memuat daftar teman...
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-metro btn-secondary btn-more">Load More</button>
            </div>
        </div>
    </div>
</div>

<div class="modal-token-exceeded modal fade" id="modal-token-exceeded" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <h3 class="text-center up">Maaf, kamu sudah menggunakan 3x kesempatan memilih teman. <br>Silakan coba lagi esok hari.</h3>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss='modal'>Tutup</button>
            </div>
        </div>
    </div>
</div>

@section('script-fb-init')

    var fbConnected = false;
    var fbResponse;

    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            fbConnected     = true;
            fbResponse      = response;
        }
    });

    $('#btn-show-friends').on('click', function(e){

        e.preventDefault();

        if (fbConnected){
            showModal(fbResponse, '{{ route('site.login') }}');
        } else {
            FB.login(function(fbResponse) {
                if(fbResponse.status === 'connected'){
                    showModal(fbResponse, '{{ route('site.login') }}');
                }
            }, {
               scope: 'public_profile, email, user_friends, publish_actions',
               return_scopes: true
            });
        }

    });

@stop
