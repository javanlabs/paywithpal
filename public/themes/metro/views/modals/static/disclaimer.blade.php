<!-- Modal -->
<div class="modal fade modal-disclaimer modal-static-content" id="modal-disclaimer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-selected=0>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="up">Disclaimer</h4>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                Lorem ipsum
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-metro btn-primary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
