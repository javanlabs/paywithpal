@extends('layouts.main')

@section('content')
<div id="page-voucher" class="text-center">
    <div class="big-screen">
        <div class="headline">
            <img src="{{ theme_asset('skins/pay-with-pal-white-small.png') }}" alt="" class="pay-with-pal">
            <h2 class="tagline">Kalo Beneran Sohib <br> Pasti Saling Menghargai</h2>

            <p class="caption">
                Login Using Your Facebook Account
                <br>
                {{--<span>--}}
                    {{--Semakin Deket Loe Ama Temen Loe, <br>Semakin Gede Diskonnya.--}}
                {{--</span>--}}
            </p>

            <div>
                @if(Auth::check())
                    <button id="btn-show-friends" class="btn btn-metro btn-primary">
                        Cari Harga Teman
                        <i class="fa fa-caret-right"></i>
                    </button>
                @else
                    <button id="btn-show-friends" class="btn btn-social btn-facebook">
                        Connect With Facebook
                        <i class="fa fa-facebook"></i>
                    </button>
                @endif
            </div>
        </div>
    </div>

    <div class="section-voucher">
        <h3 class="title">
            @if($vouchers)
            <span>{{ $voucherCount }}</span>
            Teman Sudah Dipake Belanja
            @else
            Belum Ada Voucher
            @endif
        </h3>

        @if($vouchers)
            <div class="voucher-list container-fluid">
                <div class="row items">
                    @foreach($vouchers as $item)
                    @include('voucher.item')
                    @endforeach
                </div>
            </div>

            <div class="text-center">
                <a href="{{ route('voucher.index') }}" class="btn btn-metro btn-secondary btn-more">Load More</a>
            </div>
        @endif

    </div>

</div>

@include('modals.friends')

@stop
