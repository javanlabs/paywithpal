@extends('layouts.main')

@section('content')
    <div id="page-voucher-exceeded">
        <div class="container-fluid">
            <h3 class="text-center up">Maaf, kamu sudah menggunakan 3x kesempatan memilih teman. <br>Silakan coba lagi esok hari.</h3>
        </div>
    </div>
@stop