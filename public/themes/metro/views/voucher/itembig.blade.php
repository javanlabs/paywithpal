<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="item item-big">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-4 avatar"><img src="{{ $item['avatar'] }}" alt="" class="img-circle"></div>
            <div class="col-md-6 col-sm-6 col-xs-8 discount">
                <div class="primary"><span class="discount-{{ $item['pivot']['discount'] }}">{{ $item['pivot']['discount'] }}<small>ribu</small></span> <span class="off">off</span></div>
                <div class="secondary">discount voucher</div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-8 col-xs-offset-4 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 action">
                <div class="share hidden">
                    <span class="up">Share: </span> &nbsp; <i class="fa fa-2x fa-facebook-square theme-facebook"></i> <i class="fa fa-2x fa-twitter-square theme-twitter"></i>
                </div>
                <div class="redeem">
                    <form method="post" action="{{ route('voucher.redeem') }}" class="form-redeem">
                        <input type="hidden" name="id" value="{{ $item['pivot']['id'] }}">
                        <input type="hidden" name="avatar" value="{{ $item['avatar'] }}">
                        <input type="hidden" name="tag_id" value="{{ $item['tag_id'] }}">
                        <input type="hidden" name="name" value="{{ $item['name'] }}">
                        <input type="hidden" name="level" value="{{ $item['level'] }}">
                        <input type="hidden" name="message" value="{{{ $item['message'] }}}">
                        <button type="submit" class="btn btn-metro btn-primary btn-redeem">Redeem <i class="fa fa-caret-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
