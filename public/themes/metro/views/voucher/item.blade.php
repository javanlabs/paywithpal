<div class="col-md-4 col-sm-6">
    <div class="item">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3 avatar"><img src="{{ $item['friend_avatar'] }}" alt="" class="img-circle img-responsive"></div>
            <div class="col-md-3 col-sm-3 col-xs-3 discount"><span class="discount-{{ $item['discount'] }}">{{ $item['discount'] }}<small>%</small></span> off</div>
            <div class="col-md-3 col-sm-3 col-xs-3 desc">dibuat belanja oleh</div>
            <div class="col-md-3 col-sm-3 col-xs-3 avatar"><img src="{{ $item['user_avatar'] }}" alt="" class="img-circle img-responsive"></div>
        </div>
    </div>
</div>