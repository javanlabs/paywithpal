<?php $bodyClass = 'admin' ?>

@extends('layouts.base')

@section('body')
    <div class="viewport" style="max-width: 90%">
        @include('partials.header_admin')
        @yield('content')
    </div>

    <script src="{{ asset_hashed('compiled/jquery-bootstrap.min.js') }}"></script>
    <script src="{{ asset_hashed('compiled/frontend.min.js') }}"></script>

    @yield('script-end')

@stop
