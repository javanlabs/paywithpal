@extends('layouts.base')

@section('body')
    {{ Analytics::render() }}
    <div id="fb-root"></div>

    <div class="viewport">

        @include('partials.header')

        @yield('content')

        @include('partials.footer')

    </div>

    <script src="{{ asset_hashed('compiled/jquery-bootstrap.min.js') }}"></script>
    <script src="{{ asset_hashed('compiled/frontend.min.js') }}"></script>

    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '{{ Setting::get('facebook.app_id') }}',
          xfbml      : true,
          version    : 'v2.1'
        });

        FB.Canvas.setAutoGrow()


        @yield('script-fb-init')

      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/id_ID/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>


    @yield('script-end')

@stop
