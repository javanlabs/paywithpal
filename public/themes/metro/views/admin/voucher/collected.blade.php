@extends('layouts.admin')
@section('content')

    <h2 class="page-title">Vouchers</a></h2>

    <ul class="nav nav-tabs" role="tablist">
        <li><a href="{{ route('admin.voucher') }}">Available <span class="badge">{{ $availableCount }}</span></a></li>
        <li><a href="{{ route('admin.voucher.redeemed') }}">Redeemed <span class="badge">{{ $redeemedCount }}</span></a></li>
        <li class="active"><a href="{{ route('admin.voucher.collected') }}">Collected <span class="badge">{{ $collectedCount }}</span></a></li>
        <li><a href="{{ route('admin.voucher.generated') }}">Generated <span class="badge">{{ $generatedCount }}</span></a></li>
        @if($authUser['email']=="acep.kosim@id.leoburnett.com")
            <li class="pull-right"><a href="{{ route('admin.voucher.import') }}"><i class="fa fa-upload"></i> Import Vouchers</a></li>
        @endif
    </ul>
    
    @if($vouchers!="")
        <table class="table">
            <thead>
                <tr>
                    <th colspan="5">
                        <form class="form-search-voucher" id="form-search-voucher">
                            <div class="right-inner-addon inner">
                                <i class="icon-search glyphicon glyphicon-search"></i>
                                <input id="input-search-voucher" type="text" name="keyword" value="{{ Input::get('keyword') }}" class="form-control input keyword" placeholder="Search" />
                            </div>
                        </form> 
                    </th>
                </tr>
                <tr>
                    <th>Code</th>
                    <th class="text-center">Discount</th>
                    <th class="">Used By</th>
                    <th class="text-right">Redeemed Date</th>
                    <th class="text-right">Colleted Date</th>
                </tr>
            </thead>
            <tbody>
            @foreach($vouchers as $voucher)
            <tr>
                <td>{{ $voucher['code'] }}</td>
                <td class="text-center discount-{{ $voucher['discount'] }}">{{ $voucher['discount'] }}</td>
                <td class=""><img src="{{ $voucher['user_avatar'] }}" alt="" class="img-circle" style="width:20px"> {{ $voucher['user']['name'] }}</td>
                <td class="text-right">{{ $voucher['redeems_created_at'] }}</td>
                <td class="text-right">{{ $voucher['collected_at'] }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <span class="empty">Voucher Tidak Ditemukan</span>
    @endif
    

    <div class="text-center">
        {{ $vouchers->links() }}
    </div>

@stop
