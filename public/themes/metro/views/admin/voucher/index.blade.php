@extends('layouts.admin')
@section('content')

    <h2 class="page-title">Vouchers</h2>

    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="{{ route('admin.voucher') }}">Available <span class="badge">{{ $availableCount }}</span></a></li>
        <li><a href="{{ route('admin.voucher.generated') }}">Used <span class="badge">{{ $usedCount }}</span></a></li>
    </ul>
    <table class="table">
        <thead>
            <tr>
                <th>Code</th>
                <th class="text-center">Product</th>
            </tr>
        </thead>
        <tbody>
        @foreach($vouchers as $voucher)
        <tr>
            <td>{{ $voucher['code'] }}</td>
            <td>{{ $voucher['label'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <div class="text-center">
        {{ $vouchers->links() }}
    </div>

@stop
