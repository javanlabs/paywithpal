@extends('layouts.admin')
@section('content')

    <div id="page-voucher-import">
        <div class="mb">
            <a href="{{ route('admin.voucher') }}">&laquo; Back to voucher list</a>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Import Vouchers</div>
            <div class="panel-body">
                <p>
                    Tambah kode voucher dengan mengunggah file excel atau csv:
                    {{ Form::open(['route' => 'admin.voucher.import.do', 'files' => true]) }}
                        <div class="form-group">
                            {{ Form::file('file') }}
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>

                    {{ Form::close() }}
                </p>
            </div>
        </div>

        <div class="bg-warning" style="padding:20px">
            Pastikan format file yang Anda unggah sesuai dengan template yang sudah disedikan berikut ini:
            <br>
            <br>
            <div class="btn-group">
                <a class="btn btn-default" href="{{ asset('files/voucher-template.xls') }}"><i class="fa fa-download"></i> Download Template Excel</a>
                <a class="btn btn-default" href="{{ asset('files/voucher-template.csv') }}"><i class="fa fa-download"></i> Download Template CSV</a>
            </div>
        </div>

    </div>
@stop
