@extends('layouts.admin')
@section('content')
    <style>
        .table tbody tr td{
            vertical-align: middle;
        }
    </style>
    <h2 class="page-title">Vouchers</h2>

    <ul class="nav nav-tabs" role="tablist">
        <li><a href="{{ route('admin.voucher') }}">Available <span class="badge">{{ $availableCount }}</span></a></li>
        <li class="active"><a href="{{ route('admin.voucher.generated') }}">Used <span class="badge">{{ $usedCount }}</span></a></li>
        <li class="pull-right"><a href="{{ route('admin.voucher.generated.export', ['sortType' => Input::get('sortType', 'asc')]) }}"><i class="fa fa-download"></i> Download CSV</a></li>
    </ul>
    <table class="table">
        <thead>
        <tr>
            <th class="text-center">Unlock Code</th>
            <th class="text-center">Pengirim</th>
            <th class="text-center">Penerima</th>
            <th class="text-center">Voucher</th>
            <th class="text-center">Status</th>
            <th class="text-center">Tanggal Unlock Code</th>
            <th class="text-center">
                    Tanggal
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($vouchers as $voucher)
            <tr>
                <td class="text-center">{{ $voucher['unlock_code'] }}</td>
                <td>{{ $voucher->sender_name }} <br/>({{ $voucher->sender_email }})</td>
                <td>{{ $voucher->friend_name }} <br/>({{ $voucher->friend_email }})</td>
                <td>{{ $voucher->label }}</td>
                <td>{{ $voucher->status }}</td>
                <td>{{ $voucher->unlock_date }}</td>
                <td>{{ $voucher->generated_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="text-center">
        {{ $vouchers->appends(['sortType' => Input::get('sortType')])->links() }}
    </div>

@stop
