@extends('layouts.admin')
@section('content')

    <h2 class="page-title">Vouchers</a></h2>

    <ul class="nav nav-tabs" role="tablist">
        <li><a href="{{ route('admin.voucher') }}">Available <span class="badge">{{ $availableCount }}</span></a></li>
        <li class="active"><a href="{{ route('admin.voucher.redeemed') }}">Redeemed <span class="badge">{{ $redeemedCount }}</span></a></li>
        <li><a href="{{ route('admin.voucher.collected') }}">Collected <span class="badge">{{ $collectedCount }}</span></a></li>
        @if($authUser['email']=="acep.kosim@id.leoburnett.com")
            <li class="pull-right"><a href="{{ route('admin.voucher.import') }}"><i class="fa fa-upload"></i> Import Vouchers</a></li>
        @endif
    </ul>

    @if($vouchers!="")
        <table class="table">
            <thead>
                <tr>
                    <th colspan="5">
                        <form class="form-search-voucher" id="form-search-voucher">
                            <div class="right-inner-addon inner">
                                <i class="icon-search glyphicon glyphicon-search"></i>
                                <input id="input-search-voucher" type="text" name="keyword" value="{{ Input::get('keyword') }}" class="form-control input keyword" placeholder="Search" />
                            </div>
                        </form>
                    </th>
                </tr>
                <tr>
                    <th>Code</th>
                    <th class="text-center">Discount</th>
                    <th class="">Used By</th>
                    <th class="">Time</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>

            @foreach($vouchers as $voucher)

            <tr>
                <td>
                    <a href="{{ asset($voucher['image_path']) }}" target="_blank">{{ $voucher['code'] }}</a>
                    <br/>
                    <a target="_blank" href="{{ route('voucher.regenerate', ['id' => $voucher['id']]) }}">Regenerate Printed Voucher</a>
                </td>
                <td class="text-center discount-{{ $voucher['discount'] }}">{{ $voucher['discount'] }}</td>
                <td class=""><img src="{{ $voucher['user_avatar'] }}" alt="" class="img-circle" style="width:20px"> {{ $voucher['user']['name'] }}</td>
                <td class="">{{ $voucher['redeems_created_at'] }}</td>
                <td class="text-right">
                    @if($voucher['is_collected']!="1")
                        @if($voucher['discount']!="0")
                            <form method="post" action="{{ route('admin.voucher.redeemed.collect') }}" class="form-redeem">
                                <input type="hidden" name="code" value="{{ $voucher['code'] }}">
                                <button type="submit" class="btn ">collect</button>
                            </form>
                        @else
                            -
                        @endif
                    @else
                        coleected
                    @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <span class="empty">Voucher Tidak Ditemukan</span>
    @endif

    <div class="text-center">
        {{ $vouchers->links() }}
    </div>

@stop
