@extends('......layouts.base')
@section('body')
    <div id="page-login" class="viewport clearfix">
        <div class="box">
            <div class="text-center">
                <img src="{{ theme_asset('skins/logo.png') }}" alt="" class="logo">
            </div>
            {{ Form::open(['route' => 'admin.login.do']) }}
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group">
                    <input type="text" name="email" class="form-control input-md" placeholder="Email..." value="{{ Input::old('email') }}">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control input-md" placeholder="Password...">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-md btn-block mb">Sign In</button>
                    <div class="text-center">
                        <a href="#">Forgot Password</a>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

    <script src="{{ asset_hashed('compiled/jquery-bootstrap.min.js') }}"></script>
@stop