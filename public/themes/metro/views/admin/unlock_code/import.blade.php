@extends('layouts.admin')
@section('content')

    <div id="page-voucher-import">
        <div class="mb">
            <a href="{{ route('admin.code') }}">&laquo; Back to code list</a>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Import Unlock Codes</div>
            <div class="panel-body">
                <p>
                    Tambah unlock code dengan mengunggah file csv:
                    {{ Form::open(['route' => 'admin.code.import.do', 'files' => true]) }}
                        <div class="form-group">
                            {{ Form::file('file') }}
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>

                    {{ Form::close() }}
                </p>
            </div>
        </div>

    </div>
@stop
