@extends('layouts.admin')
@section('content')

    <h2 class="page-title">Unlock Code</h2>

    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="{{ route('admin.code') }}">Unused <span class="badge">{{ $unusedCount }}</span></a></li>
        <li><a href="{{ route('admin.code.used') }}">Used <span class="badge">{{ $usedCount }}</span></a></li>
        <li class="pull-right"><a href="{{ route('admin.code.import') }}"><i class="fa fa-upload"></i> Import Unlock Code</a></li>
    </ul>

    <div class="row section-unlock-code">
        @foreach($codes as $code)
            <div class="col-md-3"><div class="item">{{ $code['code'] }}</div></div>
        @endforeach
    </div>

    <div class="text-center">
        {{ $codes->links() }}
    </div>

    <style>
        .section-unlock-code {margin-top: 10px}
        .section-unlock-code .item {
            border: 1px solid #ddd;
            text-align: center;
            padding: 10px;
            margin-bottom: 5px;
            font-size: 1.2em;
            text-transform: uppercase;
        }
    </style>
@stop
