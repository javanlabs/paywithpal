@extends('layouts.admin')
@section('content')

    <h2 class="page-title">Unlock Code</h2>

    <ul class="nav nav-tabs" role="tablist">
        <li><a href="{{ route('admin.code') }}">Unused <span class="badge">{{ $unusedCount }}</span></a></li>
        <li class="active"><a href="{{ route('admin.code.used') }}">Used <span class="badge">{{ $usedCount }}</span></a></li>
        <li class="pull-right"><a href="{{ route('admin.code.import') }}"><i class="fa fa-upload"></i> Import Unlock Code</a></li>
    </ul>

    <table class="table">
        <thead>
        <tr>
            <th>Code</th>
            <th>Digunakan Oleh</th>
            <th>Dikirim Ke</th>
            <th class="text-right">Pada Tanggal</th>
            <th>Voucher</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        @foreach($codes as $code)
            <tr>
                <td>{{ $code['code'] }}</td>
                <td>{{ $code['reserved_by_name'] }} ({{ $code['reserved_by_email'] }})</td>
                <td>{{ $code['friend_name'] }} ({{ $code['friend_email'] }})</td>
                <td class="text-right">{{ $code['reserved_time'] }}</td>
                <td>{{ $code['voucher']['label'] }}</td>
                <td class="text-center">
                    <a href="{{ route('admin.code.release', $code['id']) }}" class="btn btn-primary btn-xs btn-release"><i class="fa fa-refresh"></i> release</a>
                </td>
            </tr>
        @endforeach
    </table>

    <div class="text-center">
        {{ $codes->links() }}
    </div>

    <style>
        .section-unlock-code {margin-top: 10px}
        .section-unlock-code .item {
            border: 1px solid #ddd;
            text-align: center;
            padding: 10px;
            margin-bottom: 5px;
            font-size: 1.2em;
            text-transform: uppercase;
        }
        .modal-footer {
            text-align: center;
            background: transparent url("{{ theme_asset("skins/pattern-modal-footer.png") }}") repeat-x left bottom;
            padding-bottom: 20px;
        }
    </style>

    <!-- Modal -->
    <div class="modal fade" id="modal-release" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(['method' => 'put']) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    Apakah yakin akan me-<em>release</em> unlock code tersebut?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Ok</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('script-end')
    <script>
        $(document).ready(function(){
            $('.btn-release').on('click', function(e){
                e.preventDefault();
                $('#modal-release').modal('show');
                $('#modal-release').find('form').attr('action', $(this).attr('href'));
            });
        });
    </script>
@stop
