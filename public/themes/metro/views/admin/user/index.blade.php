@extends('layouts.admin')
@section('content')
    <div id="page-user">
        <h2 class="page-title">
            {{ $userCount }} Connected Users
            <small>+ {{ $adminCount }} Admin</small>
            <a class="btn btn-primary btn-xs" href="{{ route('admin.user.add') }}">Add Admin</a>
        </h2>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th style="width:100px">&nbsp;</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Facebook ID</th>
                    <th>Connected</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <td class="text-center">
                    @if($user['is_admin'])
                    {{ Form::open(['route' => 'admin.user.remove', 'method' => 'post']) }}
                        {{ Form::hidden('id', $user['id']) }}
                        <button class="badge btn btn-link btn-remove">
                            <i class="fa fa-check"></i>
                            {{ $user['role'] }}
                        </button>
                    {{ Form::close() }}
                    @endif
                </td>
                <td>{{ $user['name'] }}</td>
                <td>{{ $user['email'] }}</td>
                <td>{{ $user['facebook_id'] }}</td>
                <td>{{ $user['created_at'] }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $users->links() }}
        </div>
    </div>
@stop

@section('script-end')
<script type="text/javascript">
    $(function(){
        $('#page-user').on('mouseenter mouseleave', '.btn-remove', function(e){
            var btn = $(e.currentTarget);
            if(e.type == 'mouseenter') {
                btn.find('.fa').removeClass('fa-check').addClass('fa-times');
            } else {
                btn.find('.fa').addClass('fa-check').removeClass('fa-times');
            }
        });
    });
</script>
@stop