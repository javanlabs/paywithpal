@extends('layouts.admin')
@section('content')
    <h2 class="page-title">Add Admin</h2>

    {{ Form::open(['route' => 'admin.user.store', 'role' => 'form']) }}
        <h3 class="subtitle">Add New Admin From Existing User</h3>
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" value="{{ Input::old('email') }}">
            <div class="help-block">Email from existing user that already <strong>facebook connect</strong> to this application</div>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="text" class="form-control" name="password" value="{{ Input::old('password') }}">
        </div>

        <div class="mb">&nbsp;</div>
        <button type="submit" class="btn btn-primary">Add New Admin</button>

    {{ Form::close() }}
@stop
