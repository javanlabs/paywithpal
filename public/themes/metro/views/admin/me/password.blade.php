@extends('layouts.admin')
@section('content')
    <h2 class="page-title">My Account</h2>

    <h3 class="subtitle">Email</h3>
    <div class="form-group">
      {{ $authUser['email'] }}
    </div>

    {{ Form::open(['route' => 'admin.password.save', 'role' => 'form']) }}
        <h3 class="subtitle">Change Password</h3>
        <div class="form-group">
            <label>Current Password</label>
            <input type="password" class="form-control" name="current_password" value="">
        </div>
        <div class="form-group">
            <label>New Password</label>
            <input type="password" class="form-control" name="password">
        </div>
        <div class="form-group">
            <label>Confirm New Password</label>
            <input type="password" class="form-control" name="password_confirmation">
        </div>

        <div class="mb">&nbsp;</div>
        <button type="submit" class="btn btn-primary">Change My Password</button>

    {{ Form::close() }}
@stop
