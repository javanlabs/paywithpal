@extends('layouts.admin')
@section('content')

    <div class="text-center text-muted pad">
        <blockquote style="font-size:5em">
            Thank God It's {{ date('l') }}
        </blockquote>
        <small>Today is a gift. Use menu on top to find something useful.</small>
    </div>
@stop
