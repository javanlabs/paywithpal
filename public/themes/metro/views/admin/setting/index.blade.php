@extends('layouts.admin')
@section('content')
<style type="text/css" media="screen">
    .ace {
        height: 300px;
        width: 100%;
        border: 1px solid #ddd;
    }
</style>

    <h2 class="page-title">Settings</h2>

    {{ Form::open(['route' => 'admin.setting.save', 'role' => 'form', 'id'=>'form-setting']) }}

        <h3 class="subtitle">Game Play</h3>
        <div class="form-group">
            <p>Pilih route yang digunakan dalam aplikasi:</p>
            <table class="table-bordered table">
                <tr>
                    <td>
                        <div class="radio">
                            <label>
                                {{ Form::radio('route', 'facebook', $route == 'facebook') }}
                                Facebook
                            </label>
                        </div>
                        <p class="text-muted">Notifikasi ke teman akan dikirimkan melalui postingan facebook disertai dengan tagging</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="radio">
                            <label>
                                {{ Form::radio('route', 'email', $route == 'email') }}
                                Email
                            </label>
                        </div>
                        <p class="text-muted">Notifikasi ke teman akan dikirimkan melalui email</p>
                    </td>
                </tr>
            </table>
        </div>

        <h3 class="subtitle">Voucher</h3>
        <div class="form-group">
            <label>Valid Until</label>
            <div class="form-group">
                <div class='input-group date' id='valid-until'>
                    <input type='text' class="form-control" name="voucher[valid_until]" value="{{ $voucher['valid_until'] }}"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            {{--<input type="text" class="form-control" name="voucher[valid_until]" value="{{ $voucher['valid_until'] }}">--}}
        </div>
        <div class="form-inline">
            <label>Valid Hour</label>
            <br/>
            <div class="form-group">
                <input type="text" class="form-control" name="voucher[valid_hour_start]" value="{{ $voucher['valid_hour_start'] or '' }}"/>
                -
                <input type="text" class="form-control" name="voucher[valid_hour_finish]" value="{{ $voucher['valid_hour_finish'] or '' }}"/>
            </div>
        </div>
<br/>
        <div class="form-group">
            <label>Percentage</label>
            {{ Form::select('voucher[type1]', $percentages, $voucher['type1'], ['class' => 'form-control']) }}
        </div>

        <h3 class="subtitle">Email</h3>
        <div class="form-group">
            <label>Subject</label>
            <input type="text" class="form-control" name="email[subject]" value="{{ $email['subject'] }}">
        </div>

        <h3 class="subtitle">Static Page</h3>
        <div class="form-group">
            <label for="">Term & Conditions</label>
            {{ Form::textarea("pages[term]", $pages['term'], ['class' => 'input-ace form-control input-sm hidden', 'rows' => 5, 'cols' => 15]) }}
            <div class="ace" id="editorTerm">{{{ $pages['term'] }}}</div>
        </div>
        <div class="form-group">
            <label for="">Privacy Policy</label>
            {{ Form::textarea("pages[privacy]", $pages['privacy'], ['class' => 'input-ace form-control input-sm hidden', 'rows' => 5, 'cols' => 15]) }}
            <div class="ace" id="editorPrivacy">{{{ $pages['privacy'] }}}</div>
        </div>
        <div class="mb">&nbsp;</div>
        <button type="submit" class="btn btn-primary">Save All</button>

    {{ Form::close() }}
@stop

@section('script-end')
<script src="//cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('vendor/moment/moment.min.js') }}"></script>
<script src="{{ asset('vendor/moment/locale/id.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>

    $(document).ready(function(){

        var editors = new Array();

        $('.ace').each(function(idx, elm){
            var id = $(elm).attr('id');
            editors[id] = ace.edit(id);
            editors[id].setTheme("ace/theme/xcode");
            editors[id].getSession().setMode("ace/mode/html");

        });


        $('#form-setting').on('submit', function(e){
            $('.input-ace').each(function(id, elm){
                $(elm).val(editors[$(elm).next().attr('id')].getValue());
            });
        });

        $('#valid-until').datetimepicker({
            locale: 'id',
            format: 'D MMMM YYYY'
        });
    });
</script>
@stop
