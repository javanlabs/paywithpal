@extends('layouts.admin')
@section('content')

    <h2 class="page-title">Restart Application</h2>
    {{ Form::open(['route' => 'admin.restart.do', 'class' => 'well']) }}
    <p>
        Aksi dibawah ini akan menghapus semua data user, voucher, dan activity log.
    </p>
    {{ Form::submit('Restart Now', ['class' => 'btn btn-danger']) }}
    {{ Form::close() }}
@stop