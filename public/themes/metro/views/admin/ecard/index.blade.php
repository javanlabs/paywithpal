@extends('layouts.admin')
@section('content')
    <h2 class="page-title">Ecards</h2>
    <ul class="nav nav-tabs" role="tablist">
        <li class="pull-right"><a href="{{ route('admin.ecard.export', ['sortType' => Input::get('sortType', 'asc')]) }}"><i class="fa fa-download"></i> Download CSV</a></li>
    </ul>
    <table class="table">
        <thead>
        <tr>
            <th class="text-center">Pengirim</th>
            <th class="text-center">Penerima</th>
            <th class="text-center">Pesan</th>
            <th class="text-center">Kota</th>
            <th class="text-center">
                <a href="{{ route('admin.ecard', ['sortType' => Input::get('sortType', 'asc') == 'asc' ? 'desc' : 'asc']) }}">
                    Tanggal Input
                    @if(Input::get('sortType') == 'asc')
                        <i class="fa fa-sort-asc"></i>
                    @elseif(Input::get('sortType') == 'desc')
                        <i class="fa fa-sort-desc"></i>
                    @else
                        <i class="fa fa-sort"></i>
                    @endif
                </a>
            </th>
            <th>Status</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ecards as $ecard)
            <tr>
                <td>{{ $ecard->sender_name }}<br/>({{ $ecard->sender_email }})</td>
                <td>{{ $ecard->friend_name }}<br/>({{ $ecard->friend_email }})</td>
                <td style="width: 35%;">{{ $ecard->message }}</td>
                <td>
                    @if($ecard->location != '-- Pilih Kota --')
                    {{ $ecard->location }}
                    @endif
                </td>
                <td>{{ $ecard->generated_at }}</td>
                <td>{{ $ecard->status_html }}</td>
                <td>
                    @if($ecard->is_sent)
                    <a href="{{ $ecard->url }}" target="_blank"><i class="fa fa-file-image-o"></i></a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="text-center">
        {{ $ecards->appends(['sortType' => Input::get('sortType')])->links() }}
    </div>

@stop
