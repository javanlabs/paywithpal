<footer>
    <div class="pattern"></div>
    <div class="links">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <a href="#modal-privacy" data-toggle="modal">Privacy Policy</a>
                    |
                    <a href="#modal-disclaimer" data-toggle="modal">Disclaimer</a>
                    |
                    <a href="#modal-term" data-toggle="modal">Terms & Conditions</a>
                </div>
                <div class="col-sm-6 copyright">
                    <a href="#modal-copyright" data-toggle="modal">&copy; {{ date('Y') }} Javanlabs</a>
                </div>
            </div>
        </div>
    </div>
</footer>

@include('modals.static.disclaimer')
@include('modals.static.privacy')
@include('modals.static.term')
@include('modals.static.copyright')
