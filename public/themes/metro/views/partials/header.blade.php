<header>
    <div class="navbar" role="navigation">
        <div class="container-fluid">
            <div class="row">
                @if($isAuth)
                    <div class="navbar-header">

                        <div class="pull-left" style="margin-top:3px">
                            <a data-toggle="modal" href="#modal-copyright">
                                <img class="visible-xs-inline" src="{{ theme_asset('skins/logo.png') }}" alt=""></a>
                            <img class="visible-xs-inline" src="{{ theme_asset('skins/pay-with-pal-small.png') }}"
                                 alt="">
                        </div>

                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse">

                        <ul class="nav navbar-nav menu up">
                            <li><a href="{{ route('home') }}">About</a></li>
                            <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                            <li><a href="{{ route('voucher.index') }}">All Vouchers</a></li>
                            <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                            <li><a href="{{ route('voucher.my') }}">My Vouchers</a></li>
                            <li class="visible-sm visible-md visible-lg"><a>|</a></li>
                            <li class="logout"><a href="{{ route('site.logout') }}">Logout</a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                @endif
            </div>

        </div>
    </div>
</header>
