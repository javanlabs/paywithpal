<div class="col-md-6 col-sm-6 col-xs-12 item">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-4 left">
            <div class="box">
                <img src="{{ $item['avatar'] }}" class="img-circle avatar" alt="">
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-8 right">
            <h5 class="name el">{{ $item['name'] }}</h5>
            <button type="button" class="btn btn-metro btn-selection btn-select-friend" data-avatar="{{ $item['avatar'] }}" data-name="{{ $item['name'] }}" data-id="{{ $item['id'] }}">Pilih</button>
        </div>
    </div>
</div>
