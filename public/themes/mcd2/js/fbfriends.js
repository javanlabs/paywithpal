var friends = new Array;
var friendsFiltered = new Array;
var friendsPerPage = 12;
var friendsCurrentPage = 1;
var friendsAjax = null;
var friendsSearchTimer;
var friendsLoaded = false;
var isLogin = false;
var redeemAjax = null;
var friendsSelected = new Array;
var isTokenExceeded = false;
var currentKeyword = '';

$(function(){
    $('#btn-more').on('click', function(e){
        var btn = $(e.currentTarget);

        friendsCurrentPage++;

        btn.attr('disabled', true);
        showFriends(friendsCurrentPage, $('#keyword').val(), function(){
            //btn.button('reset').removeClass('loading');
            btn.removeAttr('disabled');
        });
    });

    $("#form-search-friends").on('submit', function (e) {
        e.preventDefault();
        $('#btn-more').removeClass('hidden');
    });

    $('#friend-lists').on('click', '.friend-item', function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        var avatar = $(this).data('avatar');
        var modal = $('#modal-selected-friend');
        modal.find('input[name="friend_id"]').val(id);
        modal.find('input[name="friend_name"]').val(name);
        modal.find('input[name="friend_avatar"]').val(avatar);
        modal.find('img#friend-avatar').attr('src', avatar);
        modal.find('#friend-name').text(name.toUpperCase());
        modal.modal('show');
    });
});

function paginate(data, page) {
    var start = (page - 1) * friendsPerPage;
    var result = data.slice(start, start + friendsPerPage);
    return result;
}

function appendFriends(data, page, keyword) {
    if (page == 1) {
        $('#friend-lists').html('');
    }
    if(data.length == 0) {
        if (page == 1) {
            //$('#box-friend-empty').removeClass('hidden');
            alert('Teman tidak ditemukan atau kamu tidak mengizinkan aplikasi ini untuk melihat daftar temanmu.');
        } else {
            alert('Tidak ada lagi teman');
            $('#btn-more').addClass('hidden');
        }
        return false;
    }

    var items = $('#friend-lists');

    function friendColumn(data){
        return $('<div class="col-sm-4">' +
        '<div class="row friend-item" data-id="'+data.id+'" data-name="'+data.name+'" data-avatar="'+data.picture.data.url+'">' +
            '<div class="col-sm-4 col-xs-3">' +
                '<img src="'+data.picture.data.url+'" alt="..." class="img-circle">' +
            '</div>' +
            '<div class="col-sm-8 col-xs-9">' +
                '<div class="friend-name">'+data.name+'</div>' +
            '</div>' +
        '</div>' +
        '</div>');
    }
    for(var i=0;i<data.length;i++) {
        var item = friendColumn(data[i]);
        items.append(item);
    }
    $('#btn-more').removeClass('hidden');
}

function showFriends(page, keyword, onDone){

    if(friends.length == 0) {
        function handleFriends(response){
            if (response && response.data) {
                for(var i=0; i<response.data.length; i++){
                    friends.push(response.data[i]);
                }

                if (response.paging.next){
                    FB.api(response.paging.next, handleFriends);
                } else {
                    friendsFiltered = paginate(filterByKeyword(friends, keyword), page);
                    appendFriends(friendsFiltered, page, keyword);
                    if($.isFunction(onDone)){
                        onDone();
                    }
                    friendsLoaded = true;
                }
            }
        }

        FB.api("me/taggable_friends", {
            fields: "id,name,picture.width(210).height(210)"
        }, handleFriends);
    } else {

        friendsFiltered = paginate(filterByKeyword(friends, keyword), page);
        appendFriends(friendsFiltered, page, keyword);
        if($.isFunction(onDone)){
            onDone();
        }
    }
}

function filterByKeyword(data, keyword) {
    var result = new Array;
    var keyword = keyword.toLowerCase();

    if(keyword != "") {
        for(var i=0;i<data.length;i++) {
            var name = data[i].name.toLowerCase();
            if(name.indexOf(keyword) != -1) {
                result.push(data[i]);
            }
        }
    } else {
        result = data;
    }

    result.sort(function(a, b){
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    });

    return result;
}

friendsSearchTimer = setInterval(function(){
    if(!friendsLoaded) {
        return false;
    }

    var form = $('#form-search-friends');
    var keyword = form.find('#keyword').val();
    if(keyword != currentKeyword) {
        currentKeyword = keyword;
        friendsCurrentPage = 1;
        friendsFiltered = paginate(filterByKeyword(friends, keyword), 1);
        appendFriends(friendsFiltered, 1, keyword);
    }
},800);