<footer>
    <div class="text-center">
        <a href="#" data-target="#modal-terms-condition-" data-toggle="modal">Terms & Condition </a> | <a href="#" data-target="#modal-privacy-policy" data-toggle="modal">Privacy Policy</a>
    </div>
</footer>

@include('site.terms-condition', ['submission' => false])
@include('site.privacy-policy')
