<div class="row">
    <div class="col-sm-3 col-xs-6 col-xs-3-custome text-center">
        <div class="step pull-left">
            <img src="{{ theme_asset("img/steps01-".$imgStep[1].".png") }}" alt="" class="img-responsive"/>
            <div class="step-circle {{ $classStep[1] or null }}">1</div>
            <div class="step-text">DAFTAR</div>
        </div>
        <div class="step-arrow pull-right hidden-xs">
            <img src="{{ theme_asset("img/arrow-red-1.png") }}" alt=""/>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6 col-xs-3-custome text-center">
        <div class="step pull-left">
            <img src="{{ theme_asset("img/steps02-".$imgStep[2].".png") }}" alt="" class="img-responsive"/>
            <div class="step-circle {{ $classStep[2] or null }}">2</div>
            <div class="step-text">PILIH TEMAN</div>
        </div>
        <div class="step-arrow pull-right hidden-xs">
            <img src="{{ theme_asset("img/arrow-blue-2.png") }}" alt=""/>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6 col-xs-3-custome text-center">
        <div class="step pull-left">
            <img src="{{ theme_asset("img/steps03-".$imgStep[3].".png") }}" alt="" class="img-responsive"/>
            <div class="step-circle {{ $classStep[3] or null }}">3</div>
            <div class="step-text">KIRIM <em>E-CARD</em></div>
        </div>
        <div class="step-arrow pull-right hidden-xs">
            <img src="{{ theme_asset("img/arrow-yellow-3.png") }}" alt=""/>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6 col-xs-3-custome2 text-center">
        <div class="step pull-left">
            <img src="{{ theme_asset("img/steps04-".$imgStep[4].".png") }}" alt="" class="img-responsive"/>
            <div class="step-circle {{ $classStep[4] or null }}">4</div>
            <div class="step-text">SELESAI</div>
        </div>
    </div>
</div>
