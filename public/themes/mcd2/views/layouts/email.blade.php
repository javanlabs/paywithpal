<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title') :: McD Bukaan Untuk Teman</title>

        @if(App::environment() != 'production')
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
            @include('emails.styles')
        @else
        <link rel="stylesheet" href="{{ theme_asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ theme_asset('css/email.css') }}">
        @endif
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div id="main-wrapper">
                        <div class="container-fluid">
                            <div class="row" id="wrapper-content">
                                <div class="col-md-12 text-center" id="banner">
                                    <img src="{{ theme_asset("img/bukaanteman-header.png") }}" alt="" class="img-responsive"/>
                                </div>
                                @yield('content')
                                <div class="col-md-12 text-center" id="footer">
                                    <a href="{{ route('site.term') }}">Terms & Condition</a> | <a href="{{ route('site.privacy') }}">Privacy Policy</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
