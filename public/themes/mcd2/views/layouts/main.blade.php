@extends('layouts.base')

@section('body')
    <div class="container-fluid" id="full-wrapper">
        @yield('content')
        <div id="footer-home"></div>
    </div>
@stop