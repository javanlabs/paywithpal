@extends('layouts.main')

@section('title', 'Verifikasi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center">
                    @if($isVerified)
                        <img src="{{ theme_asset("img/profil-verifikasi-image.png") }}" alt="" class="img-responsive" style="margin:0 auto"/>
                    @else
                        <div class="flash-error">PROFILE TIDAK TERVERIFIKASI</div>
                        <div style="font-size: 100px; margin-bottom: 50px;">:(</div>
                    @endif
                </div>
            </div>
            @if($isVerified)
                <div class="col-md-10 col-md-offset-1">
                    <div id="box-verify" class="bg-white rounded">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div id="voucher" class="rounded none-bottom">
                                    <div class="row">
                                        <div class="col-xs-4 text-center">
                                            <img id="img-verify" src="{{ theme_asset("img/ramadhan-logo.png") }}" alt="" class="img-responsive" style="margin:0 auto"/>
                                        </div>
                                        <div class="col-xs-8 text-center" id="voucher-text">
                                            <img src="{{ theme_asset('img/voucher-ramadhan-image.png') }}" alt="" class="img-responsive" style="margin:0 auto"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            @if($redeem['is_shared'] == 0)
                            <div class="col-sm-2 col-xs-4 text-center">
                                <img src="{{ theme_asset("img/hand-image.png") }}" alt="" class="img-responsive"/>
                            </div>
                            <div class="col-sm-6 col-xs-8">
                                <div id="voucher-get-text" class="bg-gradient">
                                    Dapatkan vouchernya dengan mengucapkan terima kasih
                                </div>
                                <div id="verify-arrow">
                                    <img src="{{ theme_asset("img/arrow-up.png") }}" alt="" class="img-responsive"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <button class="btn btn-fb md btn-lg"><i class="fa fa-facebook fa-fw"></i> KATAKAN DENGAN FACEBOOK</button>
                                </div>
                                <div class="form-group">
                                    <a href="https://twitter.com/intent/tweet?url={{ route('route2.home') }}&text={{ str_replace(' ', '+', 'Terima kasih, '.$redeem['sender'].'. Saya dapat voucher McDonald’s Ramadan nih. Mau dapat juga? Yuk klik untuk tahu caranya!') }}" class="btn btn-tw md btn-lg"><i class="fa fa-twitter fa-fw"></i> KATAKAN DENGAN TWITTER</a>
                                </div>
                            </div>
                            @else
                            <h1 class="text-center text-info">Kamu sudah mendapatkan voucher dari {{ $redeem['sender'] }}</h1>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop

@section('script')
    @parent
    @if($isVerified && $redeem['is_shared'] == 0)
        <script>
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}'
                    }
                });

                var afterShared = function(){
                    $.post('{{ route('shared') }}', {token: '{{ $thanksCode }}'}).done(function(data){
                        if(data.status == 1){
                            window.location.assign(data.redirect_url);
                        }
                    }).fail(function(){
                        alert("Some error");
                    });
                };

                // Facebook
                window.fbAsyncInit = function() {
                    FB.init({
                        appId      : '{{ Setting::get('facebook.app_id') }}',
                        xfbml      : true,
                        version    : 'v2.1'
                    });

                    FB.Canvas.setAutoGrow();

                    $('.btn-fb').on('click', function(e){
                        e.preventDefault();
                        FB.ui({
                            method: 'feed',
                            name: 'Terim kasih, {{ $redeem['sender'] }}.',
                            picture: '{{ theme_asset('img/share.jpg') }}',
                            caption: '{{ route('route2.home') }}',
                            link: '{{ route('route2.home') }}',
                            description: 'Saya dapat voucher McDonald’s Ramadan nih. Mau dapat juga? Yuk klik untuk tahu caranya!',
                            display: 'popup'
                        }, function(response){
                            if(response.post_id){
                                afterShared();
                            }
                        });
                    });

                };

                (function(d, s, id){
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/id_ID/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));


                // Twitter
                window.twttr = (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0],
                            t = window.twttr || {};
                    if (d.getElementById(id)) return t;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "https://platform.twitter.com/widgets.js";
                    fjs.parentNode.insertBefore(js, fjs);

                    t._e = [];
                    t.ready = function(f) {
                        t._e.push(f);
                    };

                    return t;
                }(document, "script", "twitter-wjs"));

                twttr.ready(function (twttr) {
                    twttr.events.bind('tweet', function(event){
                        if(event.type == 'tweet'){
                            afterShared();
                        }
                    });
                });
            });
        </script>
    @endif
@stop


@section('body-start')
    @if(App::environment('production'))
        <script type='text/javascript'>
            var ebRand = Math.random()+'';
            ebRand = ebRand * 1000000;
            //<![CDATA[
            document.write('<scr'+'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=647383&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
            //]]>
        </script>
        <noscript>
            <img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=647383&amp;ns=1"/>
        </noscript>
    @endif
@endsection
