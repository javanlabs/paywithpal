@extends('layouts.main')

@section('title', 'Verifikasi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center">
                    @if($isVerified)
                        <img src="{{ theme_asset("img/profil-verifikasi-image.png") }}" class="img-responsive" style="margin: 0 auto;"/>
                    @else
                        <div class="flash-error">PROFILE TIDAK TERVERIFIKASI</div>
                        <div style="font-size: 100px; margin-bottom: 50px;">:(</div>
                    @endif
                </div>
            </div>
            @if($isVerified)
            <div class="col-md-10 col-md-offset-1">
                <div id="box-verify" class="bg-white rounded">
                    <div class="row">
                        <div class="col-sm-7 text-center">
                            <img src="{{ theme_asset("img/verify-voucher-bukaan.png") }}" alt="" class="img-responsive" style="margin: 0 auto;"/>
                        </div>
                        <div class="col-sm-5 text-center">
                            <img id="hand-img" src="{{ theme_asset("img/verify-hand.png") }}" alt=""/>
                            <div id="text-say-to-thanks">Dapatkan vouchernya dengan mengucapkan terima kasih</div>
                            <div id="arrow">
                                <img src="{{ theme_asset("img/arrow-say-to-thanks.png") }}" alt=""/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="wave-line"></div>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-red md btn-lg"><i class="fa fa-envelope-o fa-fw"></i> <u>KATAKAN DENGAN EMAIL</u></button>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-fb md btn-lg"><i class="fa fa-facebook fa-fw"></i> <u>KATAKAN DENGAN FACEBOOK</u></button>
                        </div>
                        <div class="col-sm-4">
                            <a href="https://twitter.com/intent/tweet?url={{ route('route2.home') }}&text={{ str_replace(' ', '+', 'Terima kasih, '.$redeem['sender'].'. Saya dapat voucher McDonald’s Ramadan nih. Mau dapat juga? Yuk klik untuk tahu caranya!') }}" class="btn btn-tw md btn-lg"><i class="fa fa-twitter fa-fw"></i> <u>KATAKAN DENGAN TWITTER</u></a>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@stop

@if($isVerified)
    @section('script')
        @parent
        <script>
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}'
                    }
                });

                var afterShared = function(type){
                    $.post('{{ route('shared') }}', {token: '{{ $thanksCode }}', type: type}).done(function(data){
                        if(data.status == 1){
                            window.location.assign(data.redirect_url);
                        }
                    }).fail(function(){
                        alert("Some error");
                    });
                };

                $('.btn-red').on('click', function(){
                    $(this).attr('disabled', true);
                    afterShared('email');
                });

                // Facebook
                window.fbAsyncInit = function() {
                    FB.init({
                        appId      : '{{ Setting::get('facebook.app_id') }}',
                        xfbml      : true,
                        version    : 'v2.1'
                    });

                    FB.Canvas.setAutoGrow();

                    $('.btn-fb').on('click', function(e){
                        e.preventDefault();
                        FB.ui({
                            method: 'feed',
                            name: 'Terim kasih, {{ $redeem['sender'] }}.',
                            picture: '{{ theme_asset('img/share.jpg') }}',
                            caption: '{{ route('route2.home') }}',
                            link: '{{ route('route2.home') }}',
                            description: 'Saya dapat voucher McDonald’s Ramadan nih. Mau dapat juga? Yuk klik untuk tahu caranya!',
                            display: 'popup'
                        }, function(response){
                            if(response.post_id){
                                afterShared();
                            }
                        });
                    });

                };

                (function(d, s, id){
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/id_ID/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));


                // Twitter
                window.twttr = (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0],
                            t = window.twttr || {};
                    if (d.getElementById(id)) return t;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "https://platform.twitter.com/widgets.js";
                    fjs.parentNode.insertBefore(js, fjs);

                    t._e = [];
                    t.ready = function(f) {
                        t._e.push(f);
                    };

                    return t;
                }(document, "script", "twitter-wjs"));

                twttr.ready(function (twttr) {
                    twttr.events.bind('tweet', function(event){
                        if(event.type == 'tweet'){
                            afterShared();
                        }
                    });
                });
            });
        </script>
    @stop
@endif