@extends('layouts.main2')

@section('title', 'Connect with Facebook')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="wrapper-connect" class="rounded" style="margin-bottom: 30px;">
                    <div class="row">
                        <div class="col-sm-5" id="text">
                            <div id="you-get">Kamu mendapatkan</div>
                            <div class="voucher-bukaan">Voucher Bukaan</div>
                            <div class="voucher-bukaan">Untuk Teman</div>
                            <div id="arrow-side">
                                <img src="{{ theme_asset("img/arrow-verify.png") }}" alt="" class="img-responsive"/>
                            </div>
                            <div id="arrow-bottom">
                                <img src="{{ theme_asset("img/arrow-say-to-thanks.png") }}" alt="" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-sm-6 col-sm-offset-1 text-center">
                            <img src="{{ theme_asset("img/verify-voucher-bukaan.png") }}" alt="" class="img-responsive" style="margin: 0 auto;"/>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12 line"><div id="line"></div></div>
                            <div class="text-center">
                                <button class="btn btn-fb btn-lg"><i class="fa fa-facebook fa-fw"></i> | CONNECT WITH FACEBOOK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}'
                }
            });

            var login = function(authResponse){
                $.post('{{ route('route1.redeem.login') }}', {authResponse: authResponse,code: '{{ $code }}'}).done(function(data){
                    if(data.status == 1){
                        window.location.assign(data.redirect_url);
                    }else{
                        alert(data.message);
                        window.location.assign(data.redirect_url);
                    }
                }).fail(function(){
                    alert("Some error");
                });
            };

            // Facebook
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '{{ Setting::get('facebook.app_id') }}',
                    xfbml      : true,
                    version    : 'v2.1'
                });

                FB.Canvas.setAutoGrow();

                var fbConnected = false;
                var fbResponse;

                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        fbConnected     = true;
                        fbResponse      = response;
                    }
                });

                $('.btn-fb').on('click', function(){
                    $(this).attr('disabled', true);
                    if (fbConnected){
                        login(fbResponse.authResponse);
                    } else {
                        FB.login(function(fbResponse) {
                            if(fbResponse.status === 'connected'){
                                console.log(fbResponse);
                                login(fbResponse.authResponse);
                            }
                        }, {
                            scope: 'public_profile, email, user_friends, publish_actions',
                            return_scopes: true
                        });
                    }

                });

            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/id_ID/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        });
    </script>
@stop