@extends('layouts.main2')

@section('title', 'Thanks')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="box-thanks">
                    <div class="row">
                        <div class="col-sm-3 text-center" id="from">
                            <img src="{{ theme_asset("img/photo_blank.png") }}" alt="..." class="img-circle">
                            <div id="sender">{{ $redeem->user_name }}</div>
                        </div>
                        {{--<div class="clearfix"></div>--}}
                        <div class="col-sm-9" id="say-to-thanks">
                            <div class="row">
                                <div class="col-sm-3 text-center">
                                    <img id="img-hand" src="{{ theme_asset("img/hand-image-bigger.png") }}" alt=""/>
                                </div>
                                <div class="col-sm-4" id="text-thanks">
                                    <div class="light-text">Mengucapkan</div>
                                    <div class="bolder-text">Terima Kasih</div>
                                    <div class="light-text">Kepada</div>
                                    <div id="thanks-arrow">
                                        <img src="{{ theme_asset("img/arrow-thanks.png") }}" alt="" class="img-responsive"/>
                                    </div>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <img src="{{ theme_asset("img/photo_blank.png") }}" alt="..." class="img-circle" id="img-to">
                                    <div id="to" class="bg-gradient">{{ $redeem->voucher_sender_name }}</div>
                                </div>
                                <div class="col-sm-12 text-center" id="want-to-follow">
                                    <a href="{{ route('route2.home') }}" class="btn btn-red btn-lg" style="width: 226px;">SAYA MAU IKUTAN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop