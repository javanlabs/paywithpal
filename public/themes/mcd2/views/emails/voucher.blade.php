@extends('layouts.email')

@section('title', 'Voucher Bukaan Puasa')

@section('content')
    <div class="col-sm-12">
        <div id="box-top" class="bg-white rounded none-bottom">
            <div class="row">
                <div class="col-sm-3 text-center" id="from">
                    <img src="{{ theme_asset("img/photo_blank.png") }}" alt="..." class="img-circle">
                    <div id="sender">{{ $receiver }}</div>
                </div>
                <div class="col-sm-9 bg-white" id="words">
                    <div class="row" id="words-row">
                        <div class="col-xs-4 text-center">
                            <img src="{{ theme_asset("img/finish-send.png") }}" alt=""/>
                        </div>
                        <div class="col-xs-8">
                            <div id="send-text">Baru saja mendapatkan voucher bukaan puasa dari</div>
                        </div>
                    </div>
                    <div class="row" id="receiver">
                        <div class="col-xs-4 text-center" id="receiver-photo">
                            <img src="{{ theme_asset("img/photo_blank.png") }}" alt="..." class="img-circle">
                        </div>
                        <div class="col-xs-8">
                            <div id="receiver-name">{{ $sender }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="box-bottom" class="bg-white rounded none-top">
            <div class="row">
                <div class="col-sm-4 col-xs-5">
                    <div id="text-redeem">Voucher Bukaan Puasa:</div>
                    <div id="arrow">
                        <img src="{{ theme_asset("img/arrow-thanks.png") }}" alt="" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-sm-8 col-xs-7" style="padding-left: 2px;">
                    <div id="voucher" class="bg-gradient">
                        {{ $voucher }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop