@extends('layouts.email')

@section('title', 'Voucher Link')

@section('content')
    <div class="col-sm-12">
        <div id="box-top" class="bg-white rounded none-bottom">
            <div class="row">
                <div class="col-xs-3">
                    <img id="img-box" class="img-responsive" src="{{ theme_asset("img/box-white-send-to.png") }}" alt=""/>
                </div>
                <div class="col-xs-9">
                    <div id="receiver-link">Hai Valdano,</div>
                </div>
                <div class="col-xs-12 bg-white" id="wrapper-text-bukaan">
                    <div id="barusaja">Kamu baru saja mendapat</div>
                    <div><span id="text-bukaan">Bukaan Untuk Teman</span> dari <span id="sender-link">Herman Taniawan</span></div>
                </div>
            </div>
        </div>
        <div id="box-bottom" class="bg-white rounded none-top">
            <div class="row">
                <div class="col-sm-4 col-xs-5">
                    <div id="text-redeem">Klik link untuk mendapatkan voucher:</div>
                    <div id="arrow">
                        <img src="{{ theme_asset("img/arrow-thanks.png") }}" alt="" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-sm-8 col-xs-7" style="padding-left: 2px;">
                    <div id="link" class="bg-gradient">
                        <a href="#">http://paywithpal.dev/asdasdasd</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop