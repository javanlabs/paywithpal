@extends('layouts.main')

@section('title', 'Daftar')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center">
                    <img src="{{ theme_asset("img/valid-code.png") }}" alt="" class="img-responsive" style="margin: 0 auto"/>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 col-max-320">
                <div id="box-steps" class="manual">
                    @include('partials.step')
                    <div class="row field">
                        <div class="col-sm-12 line"><div id="line"></div></div>
                        {{--<div class="col-sm-12" id="fb-connect">--}}
                            {{--<div class="text-center">--}}
                                {{--<button class="btn btn-fb btn-lg"><i class="fa fa-facebook fa-fw"></i> | CONNECT WITH FACEBOOK</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{ Form::open(array('route' => 'route2.register', 'id' => 'register-form')) }}
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::text('name', Input::old('name'), ['class' => 'form-control light-blue input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA KAMU', 'required' => 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::email('email', Input::old('email'), ['class' => 'form-control light-blue input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN EMAIL KAMU', 'required' => 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::submit('SUBMIT', ['class' => 'btn btn-block btn-yellow md text-choco btn-lg']) }}
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('body-start')
    @if(App::environment('production'))
    <script type='text/javascript'>
        var ebRand = Math.random()+'';
        ebRand = ebRand * 1000000;
        //<![CDATA[
        document.write('<scr'+'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=647381&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
        //]]>
    </script>
    <noscript>
        <img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=647381&amp;ns=1"/>
    </noscript>
    @endif
@endsection
{{--@section('script')--}}
    {{--@parent--}}
    {{--<script>--}}
        {{--$.ajaxSetup({--}}
            {{--headers: {--}}
                {{--'X-CSRF-Token': '{{ csrf_token() }}'--}}
            {{--}--}}
        {{--});--}}

        {{--var fbConnect = function(authResponse){--}}
            {{--$.post('{{ route('route2.fb.connect') }}', {authResponse: authResponse}).done(function(data){--}}
                {{--if(data.status == 1){--}}
                    {{--$('#fb-connect').addClass('hidden');--}}
                    {{--var form = $('#register-form');--}}
                    {{--form.find('input[name="name"]').val(data.profile.name);--}}
                    {{--form.find('input[name="email"]').val(data.profile.email);--}}
                    {{--form.removeClass('hidden');--}}
                {{--}else{--}}
                    {{--alert("Gagal menyambungkan dengan facebook");--}}
                {{--}--}}
            {{--}).fail(function(){--}}
                {{--alert("Some error");--}}
            {{--});--}}
        {{--};--}}

        {{--// Facebook--}}
        {{--window.fbAsyncInit = function() {--}}
            {{--FB.init({--}}
                {{--appId      : '{{ Setting::get('facebook.app_id') }}',--}}
                {{--xfbml      : true,--}}
                {{--version    : 'v2.1'--}}
            {{--});--}}

            {{--FB.Canvas.setAutoGrow();--}}

            {{--var fbConnected = false;--}}
            {{--var fbResponse;--}}

            {{--FB.getLoginStatus(function(response) {--}}
                {{--if (response.status === 'connected') {--}}
                    {{--fbConnected     = true;--}}
                    {{--fbResponse      = response;--}}
                {{--}--}}
            {{--});--}}

            {{--$('.btn-fb').on('click', function(){--}}
                {{--$(this).attr('disabled', true);--}}
                {{--if (fbConnected){--}}
                    {{--fbConnect(fbResponse.authResponse);--}}
                {{--} else {--}}
                    {{--FB.login(function(fbResponse) {--}}
                        {{--if(fbResponse.status === 'connected'){--}}
                            {{--fbConnect(fbResponse.authResponse);--}}
                        {{--}--}}
                    {{--}, {--}}
                        {{--scope: 'public_profile',--}}
                        {{--return_scopes: true--}}
                    {{--});--}}
                {{--}--}}
            {{--});--}}

        {{--};--}}

        {{--(function(d, s, id){--}}
            {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
            {{--if (d.getElementById(id)) {return;}--}}
            {{--js = d.createElement(s); js.id = id;--}}
            {{--js.src = "//connect.facebook.net/id_ID/sdk.js";--}}
            {{--fjs.parentNode.insertBefore(js, fjs);--}}
        {{--}(document, 'script', 'facebook-jssdk'));--}}
    {{--</script>--}}
{{--@stop--}}
