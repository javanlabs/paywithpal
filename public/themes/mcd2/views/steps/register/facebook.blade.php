@extends('layouts.main2')

@section('title', 'Daftar')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 bg-white rounded">
                <div class="text-center">
                    <img src="{{ theme_asset("img/valid-code.png") }}" alt=""/>
                </div>

                <div id="box-steps" class="mar-step1">
                    @include('partials.step')
                </div>

                <div class="text-center" style="margin: 20px 0;">
                    <button class="btn btn-fb btn-lg"><i class="fa fa-facebook fa-fw"></i> | CONNECT WITH FACEBOOK</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            }
        });

        var login = function(authResponse){
            $('.btn-fb').attr('disabled', true);
            $.post('{{ route('route1.register') }}', {authResponse: authResponse}).done(function(data){
                if(data.status == 1){
                    window.location.assign(data.redirect_url);
                }else{
                    alert("Gagal login");
                    $('.btn-fb').attr('disabled', false);
                }
            }).fail(function(){
                alert("Some error");
                $('.btn-fb').attr('disabled', false);
            });
        };

        // Facebook
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '{{ Setting::get('facebook.app_id') }}',
                xfbml      : true,
                version    : 'v2.1'
            });

            FB.Canvas.setAutoGrow();

            var fbConnected = false;
            var fbResponse;

            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    fbConnected     = true;
                    fbResponse      = response;
                }
            });

            $('.btn-fb').on('click', function(){
                if (fbConnected){
                    login(fbResponse.authResponse);
                } else {
                    FB.login(function(fbResponse) {
                        if(fbResponse.status === 'connected'){
                            login(fbResponse.authResponse);
                        }
                    }, {
                        scope: 'public_profile, email, user_friends, publish_actions',
                        return_scopes: true
                    });
                }

            });

        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/id_ID/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@stop