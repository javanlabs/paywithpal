@extends('layouts.main2')

@section('title', 'Kirim E-card')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="box-steps">
                    @include('partials.step')
                </div>
            </div>
            {{ Form::open(array('route' => 'route2.ecard', 'id' => 'ecard-form')) }}
            <div class="col-md-10 col-md-offset-1">
                <div id="box-input-ecard" class="bg-white rounded none-bottom">
                    <div class="row">
                        {{ Form::open(array('route' => 'route2.ecard', 'id' => 'ecard-form')) }}
                        <div class="col-sm-5">
                            <div id="box-sender">
                                <span id="from">DARI:</span>
                                <div id="line"></div>
                                <div id="sender-fields">
                                    <div class="form-group">
                                        {{ Form::text('name', Auth::user()->name, ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA KAMU', 'required' => 'required']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::email('email', Auth::user()->email, ['class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN EMAIL KAMU', 'required' => 'required']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::select('location', $cities, Input::old('location'), ['class' => 'form-control input-lg']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div id="box-receiver">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div id="send-to-title">Dikirim Untuk:</div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div id="receiver-fields">
                                            <div class="form-group">
                                                {{ Form::text('friend_name', Session::get('friend_name'), ['class' => 'form-control light-blue input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA TEMAN', 'required' => 'required']) }}
                                            </div>
                                            <div class="form-group">
                                                {{ Form::email('friend_email', Session::get('friend_email'), ['class' => 'form-control light-blue input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN EMAIL TEMAN', 'required' => 'required']) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        {{ Form::textarea('message', Input::old('message'), ['class' => 'form-control', 'placeholder' => 'TULIS PESAN', 'rows' => 3, 'maxlength' => 140]) }}
                                        <p class="help-block text-right"><span id="message-limit">140</span> karakter tersisa</p>
                                        {{ Form::hidden('ecard_tpl', $eCardRows[0]['ecards'][0]['src']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 wrapper-wave-ecard-field" id="col-wave-yellow">
                            <div id="wave-yellow"></div>
                        </div>
                        <div class="col-sm-7 wrapper-wave-ecard-field" id="col-wave-white">
                            <div id="wave-white"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="wave-ecard-bottom"></div>
                    </div>
                </div>
                <div id="box-ecard-img" class="bg-white rounded none-top">
                    <div class="row">
                        <div class="col-sm-4" id="thumbs">
                            <div id="select-ecard-text">
                                Pilih E-CARD untuk dikirim pada temanmu di Hari Raya!
                            </div>
                            <div id="gelombang-ecard"></div>
                            <div class="carousel slide media-carousel" id="media">
                                <div class="carousel-inner">
                                    @foreach($eCardRows as $eCardRow)
                                    <div class="item {{ $eCardRow['active'] or null }}">
                                        <div class="row">
                                            @foreach($eCardRow['ecards'] as $eCard)
                                            <div class="col-xs-6">
                                                <a class="thumbnail ecard-thumb" href="#"><img alt="" src="{{ $eCard['thumb_src'] }}" data-ecard-src="{{ $eCard['src'] }}"></a>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            @if(count($eCardRows) > 1)
                            <div id="control-slider">
                                <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
                                <a data-slide="next" href="#media" class="right carousel-control">›</a>
                            </div>
                            @endif
                        </div>
                        <div class="col-sm-8" id="ecard-selected">
                            <div id="loading-ecard" class="hidden">
                                <i class="fa fa-spinner fa-spin fa-3x"></i>
                            </div>
                            <img src="{{ $eCardRows[0]['ecards'][0]['src'] }}" class="img-responsive" alt=""/>
                        </div>
                        <div class="col-sm-12" id="line-bottom">
                            <div class="row">
                                <div class="col-md-5 col-sm-8 text-center">
                                    <div id="note">*E-card akan dikirimkan pada tanggal 17 Juli</div>
                                </div>
                                <div class="col-md-7 col-sm-4">
                                    <div class="pull-right">
                                        {{--<button class="btn btn-yellow md btn-lg">KIRIM KEMBALI</button>--}}
                                        {{ Form::submit('LANJUT', ['class' => 'btn btn-yellow md text-choco btn-lg']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('script')
    @parent
    <script>
        $(document).ready(function() {

            $('#media').carousel({
                pause: true,
                interval: false
            });

            var maxLength = $('textarea').attr('maxlength');
            $('textarea').on('keyup', function() {
                var length = $(this).val().length;
                var length = maxLength-length;
                $('#message-limit').text(length);
            });

            $('#thumbs').on('click', '.ecard-thumb', function(e){
                e.preventDefault();
                var src = $(this).find('img').data('ecard-src');
                $('input[name="ecard_tpl"]').val(src);

                var width = $('#ecard-selected img').width();
                var height = $('#ecard-selected img').height();

                $('#loading-ecard').css({
                    width: width,
                    height: height
                });

                $('#loading-ecard').removeClass('hidden');

                $('#ecard-selected img').attr('src', src).on('load', function(){
                    $('#loading-ecard').addClass('hidden');
                });
            });
        });
    </script>
@stop
