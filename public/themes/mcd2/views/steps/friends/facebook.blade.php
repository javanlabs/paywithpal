@extends('layouts.main2')

@section('title', 'Pilih Teman')

@section('content')
    <div id="fb-root"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="box-steps">
                    @include('partials.step')
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <div id="box-friends" class="bg-white rounded">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <form id="form-search-friends">
                            <div class="input-group input-group-lg input-group-xlg">
                                <input id="keyword" type="text" class="form-control" placeholder="masukkan nama temanmu">
                                <span class="input-group-btn">
                                    <button class="btn btn-warning" type="button">CARI</button>
                                </span>
                            </div>
                            </form>
                        </div>

                        <div class="col-md-12">
                            <div class="row" id="friend-lists">
                                <div class="col-sm-12 text-center">
                                    <h4 class="text-success">Sedang mengambil daftar teman facebook kamu...</h4>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-12 text-center">
                                <button type="button" id="btn-more" class="hidden"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade mcd-modal" id="modal-selected-friend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                {{ Form::open(['route' => 'route1.friend.select']) }}
                <div class="modal-body text-center">
                    {{ Form::hidden('friend_id', null) }}
                    {{ Form::hidden('friend_name', null) }}
                    {{ Form::hidden('friend_avatar', null) }}

                    <img id="friend-avatar" alt="..." class="img-responsive" style="margin: 0 auto;">
                    <div id="friend-name">Ramadani</div>
                    <div>Akan menerima voucher Ramadhan?</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-yellow big btn-lg" data-dismiss="modal">KEMBALI</button>
                    <button type="submit" class="btn btn-green big btn-lg">BENAR</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ theme_asset('js/fbfriends.js') }}"></script>
    <script>
        $(function(){
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '{{ Setting::get('facebook.app_id') }}',
                    xfbml      : true,
                    version    : 'v2.1'
                });

                FB.Canvas.setAutoGrow();

                var fbConnected = false;
                var fbResponse;

                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        fbConnected     = true;
                        fbResponse      = response;
                    }

                    if(fbResponse.status === 'connected'){
                        showFriends(1, '', function(){
                            $('#btn-more').removeAttr('disabled');
                        });

                    }
                });

            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/id_ID/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        });
    </script>
@stop