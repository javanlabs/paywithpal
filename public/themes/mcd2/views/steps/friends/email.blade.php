@extends('layouts.main2')

@section('title', 'Kirim Voucher Ke Teman')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="box-steps">
                    @include('partials.step')
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <div id="box-email-to-friend" class="bg-white rounded none-bottom">
                    <div class="row">
                        <div class="col-sm-4" id="wrapper-send-to">
                            <div class="row">
                                <div class="col-sm-6 col-xs-3">
                                    <img src="{{ theme_asset("img/box-white-send-to.png") }}" alt="" class="img-responsive"/>
                                </div>
                                <div class="col-sm-6 col-xs-9">
                                    <div id="send-to-text">
                                        Dikirim Untuk:
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8" id="fields">
                            <div class="row">
                                {{ Form::open(array('route' => 'route2.friend.select')) }}
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {{ Form::text('name', Input::old('name'), ['class' => 'form-control light-blue input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN NAMA TEMAN', 'required' => 'required']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::email('email', Input::old('email'), ['class' => 'form-control light-blue input-lg', 'autocomplete' => 'off', 'placeholder' => 'MASUKKAN EMAIL TEMAN', 'required' => 'required']) }}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" style="margin-bottom: 7px;">
                                        <img src="{{ theme_asset("img/box-arrow-send-to.png") }}" alt="" style="margin-top: -10px; margin-left: -10px;"/>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::submit('SUBMIT', ['class' => 'btn btn-yellow md text-choco btn-lg']) }}
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="box-friend-wave">
                    <div class="col-sm-4 wrapper-wave-friend-field">
                        <div id="wave-yellow"></div>
                    </div>
                    <div class="col-sm-8 wrapper-wave-friend-field" id="col-wave-white">
                        <div id="wave-white"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
