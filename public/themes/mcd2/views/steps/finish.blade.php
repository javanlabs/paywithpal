@extends('layouts.main')

@section('title', 'Selesai')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center">
                    <img src="{{ theme_asset("img/valid-code.png") }}" alt="" class="img-responsive" style="margin: 0 auto"/>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <div id="box-steps" style="margin: 20px 0;">
                    @include('partials.step')
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <div id="box-finish" class="bg-white rounded none-bottom">
                    <div class="row">
                        <div class="col-sm-4 text-center" id="img-finish">
                            <img src="{{ theme_asset("img/items/" . Session::pull('voucher_type') . ".png") }}" alt=""/>
                        </div>
                        <div class="col-sm-8" id="finish-note">
                            <div class="row">
                                <div class="col-sm-3 text-center">
                                    <img src="{{ theme_asset("img/finish-send.png") }}" alt=""/>
                                </div>
                                <div class="col-sm-9">
                                    <div id="send-text">KAMU BARU SAJA MENGIRIMKAN</div>
                                    <div class="line-text"></div>
                                    <div id="product">{{ Session::pull('voucher_label', '') }}</div>
                                    <div class="line-text"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-3 text-right" id="arrow-receiver">
                                    <img src="{{ theme_asset("img/bullet-ke.png") }}" id="top" alt=""/>
                                    <img src="{{ theme_asset("img/bullet-bottom-ke.png") }}" id="bottom" alt=""/>
                                </div>
                                <div class="col-sm-9">
                                    <div id="receiver">{{ Session::pull('friend_name', '') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('body-start')
    @if(App::environment('production'))
        <script type='text/javascript'>
            var ebRand = Math.random() + '';
            ebRand = ebRand * 1000000;
            //<![CDATA[
            document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=647382&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
            //]]>
        </script>
        <noscript>
            <img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=647382&amp;ns=1"/>
        </noscript>
    @endif
@endsection
