@extends('layouts.base')

@section('content')
    <div id="fb-root"></div>
    <div class="container-fluid" id="full-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="text-center">
                        @if($isVerified)
                        <img src="{{ theme_asset("img/profil-verifikasi-image.png") }}" alt="" class="img-responsive"/>
                        @else
                        <img src="{{ theme_asset("img/profil-verifikasi-gagal-image.png") }}" alt="" class="img-responsive"/>
                        @endif
                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div id="box-verify" class="bg-white rounded">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div id="voucher" class="rounded none-bottom">
                                    <div class="row">
                                        <div class="col-xs-4 text-center">
                                            <img id="img-verify" src="{{ theme_asset("img/ramadhan-logo.png") }}" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="col-xs-8 text-center" id="voucher-text">
                                            <img src="{{ theme_asset('img/voucher-ramadhan-image.png') }}" alt=""  class="img-responsive"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-2 col-xs-4 text-center">
                                <img src="{{ theme_asset("img/hand-image.png") }}" alt="" class="img-responsive"/>
                            </div>
                            <div class="col-sm-6 col-xs-8">
                                <div id="voucher-get-text" class="bg-gradient">
                                    @if($redeem['is_shared'] == 0)
                                        <p>Dapatkan vouchernya dengan mengucapkan terima kasih</p>
                                    @else
                                        <h2 class="text-info">Kamu sudah mendapatkan voucher dari {{ $redeem['sender'] }}</h2>
                                    @endif
                                </div>
                                <div id="verify-arrow">
                                    <img src="{{ theme_asset("img/arrow-up.png") }}" alt="" class="img-responsive"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <button type="button" class="btn-facebook btn btn-fb md btn-lg"><i class="fa fa-facebook fa-fw"></i> KATAKAN DENGAN FACEBOOK</button>
                                </div>
                                <div class="form-group">
                                    <a href="https://twitter.com/intent/tweet?url={{ route('thanks', ['token' => $thanksCode]) }}&text={{ str_replace(' ', '+', 'Thanks '.$redeem['sender'].'. Saya sudah menerima voucher McDonals Ramadhan sebesar Rp. 100.000. Mau tahu caranya? Klik disini') }}" class="btn btn-tw md btn-lg"><i class="fa fa-twitter fa-fw"></i> KATAKAN DENGAN TWITTER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer-home"></div>
    </div>

@stop

@section('script')
    @parent
    @if($isVerified && $redeem['is_shared'] == 0)
        <script>
            $(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}'
                    }
                });

                var afterShared = function(){
                    $.post('{{ route('shared') }}', {token: '{{ $thanksCode }}'}).done(function(data){
                        window.location.assign(data.redirect_url);
                    }).fail(function(){
                        alert("Some error");
                    });
                };

                // Facebook
                window.fbAsyncInit = function() {
                    FB.init({
                        appId      : '{{ Setting::get('facebook.app_id') }}',
                        xfbml      : true,
                        version    : 'v2.1'
                    });

                    FB.Canvas.setAutoGrow();

                    $('.btn-facebook').on('click', function(e){
                        e.preventDefault();
                        FB.ui({
                            method: 'feed',
                            name: 'Thanks {{ $redeem['sender'] }}',
                            picture: 'http://1.bp.blogspot.com/-o1PUR3G-3lg/VOamP6mj2qI/AAAAAAAAAYg/MAFU4I81jzQ/s1600/promo%2Bmcdonalds%2Bbig%2Bmac%2B2015.jpg',
                            caption: 'www.ramadhan.mcdonals.co.id',
                            link: '{{ route('thanks', ['token' => $thanksCode]) }}',
                            description: 'Saya sudah menerima voucher McDonals Ramadhan sebesar Rp. 100.000. Mau tahu caranya? Klik disini',
                            display: 'popup'
                        }, function(response){
                            if(response.post_id){
                                afterShared();
                            }
                        });
                    });

                };

                (function(d, s, id){
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/id_ID/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));


                // Twitter
                window.twttr = (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0],
                            t = window.twttr || {};
                    if (d.getElementById(id)) return t;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "https://platform.twitter.com/widgets.js";
                    fjs.parentNode.insertBefore(js, fjs);

                    t._e = [];
                    t.ready = function(f) {
                        t._e.push(f);
                    };

                    return t;
                }(document, "script", "twitter-wjs"));

                twttr.ready(function (twttr) {
                    twttr.events.bind('tweet', function(event){
                        if(event.type == 'tweet'){
                            afterShared();
                        }
                    });
                });
            });
        </script>
    @endif
@stop
