@extends('layouts.main2')

@section('title', 'Home')

@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div id="wrapper-unlock" class="rounded none-bottom">
                        <div class="row">
                            <div class="col-sm-3 text-center">
                                <img id="mari-berbagi" src="{{ theme_asset("img/mari-berbagi-image.png") }}" alt="" />
                            </div>
                            <div class="col-sm-9" id="fields">
                                {{ Form::open(array('route' => 'invite.check')) }}
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="agree" value="1" required> <span id="agree-text">Saya menyetujui <a href="#" data-target="#modal-terms-condition-" data-toggle="modal">syarat & ketentuan berlaku</a></span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-lg input-group-xlg">
                                        {{ Form::text('code', Input::old('code'), ['id' => 'unlock-code', 'class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'masukkan kode disini..']) }}
                                        <span class="input-group-btn">
                                            @if(Setting::get('route') == 'facebook')
                                            <button class="btn btn-warning" type="submit" name="register-via" value="facebook" data-toggle="modal" data-target="#modal-terms-condition-submission">
                                                <span class="text-submit">SUBMIT</span>
                                                <span class="icon-submit"><i class="fa fa-check fa-fw"></i></span>
                                            </button>
                                            @else
                                            <button class="btn btn-warning" type="submit" name="register-via" value="email" data-toggle="modal" data-target="#modal-terms-condition-submission">
                                                <span class="text-submit">SUBMIT</span>
                                                <span class="icon-submit"><i class="fa fa-check fa-fw"></i></span>
                                            </button>
                                            @endif
                                            {{--<button type="submit" class="hidden">SUBMIT</button>--}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                            @if(Session::has('error'))
                                <div class="flash-error">{{ Session::pull('error') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="gelombang"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div id="mechanism">
                        <div class="pull-left" id="mechanism-img"></div>
                        <div class="pull-left" id="mechanism-txt">
                            <ol>
                                <li><span>Setiap pembelian Curry Beef Delight, kamu akan mendapatkan kode unik yang tertera di kemasannya (sticker warna merah).</span></li>
                                <li><span>Masukkan kode unik di <a href="http://www.mcdbukaanuntukteman.com">www.mcdbukaanuntukteman.com</a> untuk mengirimkan bukaan untuk temanmu.</span></li>
                                <li><span>Teman yang menerima kiriman bukaan dari kamu akan memperoleh voucher yang dapat ditukarkan dengan produk McDonald’s.</span></li>
                                <li><span>Voucher yang diterima oleh temanmu bisa berupa produk gratis tanpa pembelian, atau bisa juga berupa produk gratis dengan pembelian tertentu.</span></li>
                                <li><span>Periode pengiriman bukaan untuk teman: 17 Juni – 12 Juli 2015.</span></li>
                            </ol>
                        </div>
                        <div class="pull-right" id="mechanism-right-corner"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    @if(promo_ended())
        @include('site/promo-end')
    @endif

@stop

@section('script')
    @parent
    <script>
        $(function(){
            $('#modal-promo-end').modal({backdrop:'static'});
            $('#modal-promo-end').modal('show');
        });
    </script>
@endsection
