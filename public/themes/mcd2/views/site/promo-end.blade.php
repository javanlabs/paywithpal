<div class="modal fade mcd-modal" id="modal-promo-end" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center; font-family: 'AgroCondensed'; font-size: 30px">
                    <p>
                        Terima kasih atas partisipasi Anda pada program
                        <br/>
                        <strong>Bukaan Untuk Teman</strong>.
                        <br/>
                        <small>Promo telah berakhir pada {{ Carbon\Carbon::createFromFormat('Y-m-d', Config::get('promo.end_date'))->formatLocalized('%d %B %Y') }}.</small>
                    </p>
            </div>
        </div>
    </div>
</div>
