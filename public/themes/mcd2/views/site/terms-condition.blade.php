<div class="modal fade mcd-modal" id="modal-terms-condition-{{ $submission or '' }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Term &amp; Condition</h4>
            </div>
            <div class="modal-body">
                {{ Setting::get('pages.term') }}
            </div>
            <div class="modal-footer">
                @if($submission)
                <button class="btn btn-yellow big btn-lg" data-dismiss="modal">
                    <span class="text-terms">KEMBALI</span>
                    <span class="icon-terms"><i class="fa fa-arrow-left fa-fw"></i></span>
                </button>
                <button class="btn btn-green big btn-lg" id="agree">
                    <span class="text-terms">SETUJU</span>
                    <span class="icon-terms"><i class="fa fa-arrow-right fa-fw"></i></span>
                </button>
                @else
                    <button class="btn btn-yellow big btn-lg" data-dismiss="modal">TUTUP</button>
                @endif
            </div>
        </div>
    </div>
</div>
