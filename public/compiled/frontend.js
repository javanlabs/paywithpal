var friends = new Array;
var friendsFiltered = new Array;
var friendsPerPage = 6;
var friendsCurrentPage = 1;
var friendsAjax = null;
var friendsSearchTimer;
var friendsLoaded = false;
var isLogin = false;
var redeemAjax = null;
var friendsSelected = new Array;
var isTokenExceeded = false;
var currentKeyword = '';

var loadVoucherAjax = null;
var voucherCurrentPage = 1;

$(function(){

    // google analytics tracking
    $(document).on('click', '.btn-logo-metro', function() {
        ga('send', 'event', 'Logo', 'Click', 'Click');
    });
    $(document).on('click', '.btn-print-voucher', function() {
        ga('send', 'event', 'Print', 'Click', 'Voucher');
    });
    $(document).on('click', '.btn-close-voucher', function() {
        ga('send', 'event', 'Button', 'Close', 'Print');
    });

    $('.scrollable').each(function(idx, elm){
        $(elm).slimScroll({
            height: $(elm).data('scroll-height'),
            railVisible: true,
            alwaysVisible: true,
            allowPageScroll: true
        });
    });


    window.friendsSearchTimer = window.setInterval(function(){

        if(!window.friendsLoaded) {
            return false;
        }

        var form = $('#form-search-friends');
        var keyword = form.find('.keyword').val();
        if(keyword != window.currentKeyword) {
           window.currentKeyword = keyword;
           window.friendsCurrentPage = 1;
           form.addClass('loading');
           form.find('.btn-more').button('loading');
            window.friendsFiltered = paginate(filterByKeyword(window.friends, keyword), 1);
            appendFriends(window.friendsFiltered, 1, keyword);
       }
    },800);


    $("#modal-friends").on('submit', '.form-search-friends', function (e) {
        e.preventDefault();
    });

    $('#modal-friends').on('click', '.btn-select-friend', function(e){
        e.preventDefault();

        if(e.handled !== true)
        {
            // put your payload here.
            // this next line *must* be within this if statement
            e.handled = true;

            var btn = $(e.currentTarget);
            var modal = $(e.delegateTarget);
            var id = btn.data('id');

            var selectedItem = modal.data('selected');

            if(btn.hasClass('used')) {
                return false;
            }

            if(btn.hasClass('selected')) {
                $('#selected-friend-' + btn.data('selected-index') + ' .btn-remove-friend').trigger('click');
                return false;
            }

            if(selectedItem > 2) {
                return false;
            }

            var used = $('#selected-friend-' + selectedItem + ' .input-id').val() != '';
            while (used && selectedItem < 2) {
                selectedItem++;
                used = $('#selected-friend-' + selectedItem + ' .input-id').val() != '';
            }

            modal.data('selected', selectedItem + 1);
            btn.addClass('selected');
            btn.data('selected-index', selectedItem);

            var avatar = btn.data('avatar');
            var name = btn.data('name');

            $('#selected-friend-' + selectedItem).find('.avatar').attr('src', avatar);
            $('#selected-friend-' + selectedItem).find('.name').html(name);

            $('#selected-friend-' + selectedItem).find('.input-id').val(id);
            $('#selected-friend-' + selectedItem).find('.input-name').val(name);
            $('#selected-friend-' + selectedItem).find('.input-avatar').val(avatar);
            $('#selected-friend-' + selectedItem).find('.btn-remove-friend').data('friend-id', id);

            window.friendsSelected[id] = true;
        }

        return false;
    });

    $('#modal-friends').on('click', '.btn-remove-friend', function(e){
        e.preventDefault();

        var btn = $(e.currentTarget);
        var modal = $(e.delegateTarget);
        var id = btn.data('id');

        modal.data('selected', Math.min(modal.data('selected'), id));

        removeSelectedFriend(id);
    });

    function removeSelectedFriend(selectedItem) {
        var id = $('#selected-friend-' + selectedItem).find('.input-id').val();
        $('#friend-item-' + id).find('.btn-select-friend').removeClass('selected');

        $('#selected-friend-' + selectedItem).find('.avatar').attr('src', 'data:image/gif;base64,R0lGODdhZABkAIAAAMzMzJaWliwAAAAAZABkAAACc4SPqcvtD6OctNqLs968+w+G4kiW5omm6sq27gvH8kzX9o3n+s73/g8MCofEovGITCqXzKbzCY1Kp9Sq9YrNarfcrvcLDovH5LL5jE6r1+y2+w2Py+f0uv2Oz+v3/L7/DxgoOEhYaHiImKi4yNjo+AhpWAAAOw==');
        $('#selected-friend-' + selectedItem).find('.name').html('Belum Dipilih');
        $('#selected-friend-' + selectedItem).find('.input-id').val('');
        $('#selected-friend-' + selectedItem).find('.input-name').val('');
        $('#selected-friend-' + selectedItem).find('.input-avatar').val('');
        $('#selected-friend-' + selectedItem).find('.btn-remove-friend').data('friend-id', null);

        window.friendsSelected[id] = false;
    }

    $('#modal-friends').on('submit', '.form-selected-friends', function(e){
        var form = $(e.currentTarget);
        var modal = $(e.delegateTarget);

        if(modal.data('selected') <= 0){
            e.preventDefault();
            alert('Silakan pilih minimal 1 teman!');
        }
    });

    $('#modal-friends').on('click', '.btn-more', function(e){

        ga('send', 'event', 'LoadParticipant', 'Click', 'Click');

        if(window.friendsSearching) {
            return false;
        }
        var btn = $(e.currentTarget);
        var modal = $(e.delegateTarget);

        friendsCurrentPage++;
        btn.button('loading').addClass('loading');
        showFriends(friendsCurrentPage, modal.find('.keyword').val(), function(){
            btn.button('reset').removeClass('loading');
        });
    });

    // VOUCHER REDEEM
    $('#page-voucher-pick').on('submit', '.form-redeem', function(e){
        var form = $(e.currentTarget);
        var btn = form.find('button');

        e.preventDefault();

        if(redeemAjax){
            alert('Maaf, Anda hanya bisa memilih satu voucher.');
            return false;
        }
        $('#page-voucher-pick .btn-redeem').attr('disabled', 'disabled').addClass('disabled');
        btn.button('loading');
        redeemAjax = $.ajax({
            url: form.attr('action'),
            type: 'post',
            dataType: 'json',
            data: form.serialize()
        }).done(function(response) {
            if(response.status == 1){
                $('#modal-voucher .img-voucher').attr('src', response.voucher_url);
                $('#modal-voucher').modal('show');
                $('#page-voucher-pick .voucher-list').hide();
                $('#page-voucher-pick .after-pick').removeClass('hidden');

                ga('send', 'pageview', '/vp/print-voucher');

            }
        })
        .fail(function() {
            // fail or aborted
        })
        .always(function() {
            redeemAjax = null;
            btn.button('reset');
        });

    });

    $('#page-voucher').on('click', '.btn-more', function(e){

        e.preventDefault();

        var btn  = $(e.currentTarget);
        var page = $(e.delegateTarget);
        var keyword = page.find('[name=keyword]').val();

        btn.button('loading').addClass('loading');

        loadVoucherAjax = $.ajax({
            url: btn.attr('href') + '?page=' + (voucherCurrentPage + 1),
            type: 'get',
            dataType: 'json',
            data: {keyword: keyword}
        }).done(function(response) {
            if(response.status == 1){
                if(response.html != ''){
                    btn.button('reset').removeClass('loading');
                    voucherCurrentPage++;
                    page.find('.voucher-list .items').append(response.html);
                }else{
                    btn.html('No More Data').remove();
                    alert('Tidak ada lagi data');
                }
            }
        })
        .fail(function() {
            // fail or aborted
        })
        .always(function() {
            loadVoucherAjax = null;
        });

    });

});

function showModal(fbResponse, url){

    ga('send', 'pageview', '/vp/choose-friend');

    $('#modal-friends').modal('show');
    $('#modal-friends .btn-more').button('loading').addClass('loading');

    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: fbResponse.authResponse,
    }).done(function(response) {
        if(response.status == 1){
            window.isLogin = true;
            window.usedFriends = response.used_friends;
            if(response.token_limit_exceeded){
                $('#modal-friends').modal('hide');
                $('#modal-token-exceeded').modal('show');
                window.isTokenExceeded = true;
                $('#countdown').countdown(response.next_date, function (event) {
                    $(this).html(event.strftime('<span>%-H</span> jam <span>%-M</span> menit <span>%-S</span> detik lagi'));
                }).on('finish', function(){
                    window.location.reload();
                });;

                $('#modal-token-exceeded').on('hidden.bs.modal', function(){
                    window.location.reload();
                });

            }else{
                showFriends(1, '', function(){
                    $('#modal-friends .btn-more').button('reset').removeClass('loading');
                });
            }
        }else{
            $('#modal-friends').modal('hide');
            alert('Oops, gagal login');
        }
    })
    .fail(function() {
        alert( "error" );
    })
    .always(function() {

    });
}

function showFriends(page, keyword, onDone){

    if(friends.length == 0) {
        FB.api("me/taggable_friends", handleFriends);

        function handleFriends(response){
            if (response && response.data) {
                for(var i=0; i<response.data.length; i++){
                    window.friends.push(response.data[i]);
                }

                if (response.paging.next){
                    FB.api(response.paging.next, handleFriends);
                } else {

                    window.friendsFiltered = paginate(filterByKeyword(window.friends, keyword), page);
                    appendFriends(window.friendsFiltered, page, keyword);
                    if($.isFunction(onDone)){
                        onDone();
                    }
                    window.friendsLoaded = true;
                }

            }
        }
    } else {

        window.friendsFiltered = paginate(filterByKeyword(window.friends, keyword), page);
        appendFriends(window.friendsFiltered, page, keyword);
        if($.isFunction(onDone)){
            onDone();
        }
    }


    //friendsAjax = $.ajax({
    //    url: $('#modal-friends').data('url'),
    //    type: 'post',
    //    dataType: 'json',
    //    data: {page:page, keyword:keyword}
    //}).done(function(response) {
    //
    //    if(page == 1){
    //        $('#modal-friends #section-friends').html('');
    //    }
    //
    //    if(response.html == ""){
    //        if (page == 1){
    //            alert('Teman tidak ditemukan atau kamu tidak mengizinkan aplikasi ini untuk melihat daftar temanmu.');
    //        } else {
    //            alert('Tidak ada lagi teman');
    //        }
    //    }else{
    //        $('#modal-friends #section-friends').append(response.html);
    //        $('#modal-friends .btn-select-friend').tooltip({container:'body'});
    //
    //        var scrollHeight = $('#section-friends').prop('scrollHeight') + 'px';
    //        $('#section-friends').slimScroll({scrollTo : scrollHeight });
    //
    //    }
    //
    //    if($.isFunction(onDone)){
    //        onDone();
    //    }
    //
    //})
    //.fail(function() {
    //    // fail or aborted
    //})
    //.always(function() {
    //    friendsAjax = null;
    //});
}

function loadSelection(callback) {
    $.ajax({
        url: $('#modal-loading-selection').data('url'),
        type: 'post',
        dataType: 'json',
    }).done(function (response) {
        if(response.status == 1) {
            $('#page-voucher-pick .voucher-list').append(response.html);
        } else {
            alert(response.message);
        }

        if ($.isFunction(callback)) {
            callback(response);
        }

    })
    .fail(function () {
        // fail or aborted
    })
    .always(function () {
    });
}

function filterByKeyword(data, keyword) {

    var result = new Array;
    var keyword = keyword.toLowerCase();

    if(keyword != "") {
        for(var i=0;i<data.length;i++) {
            var name = data[i].name.toLowerCase();
            if(name.indexOf(keyword) != -1) {
                result.push(data[i]);
            }
        }
    } else {
        result = data;
    }

    result.sort(function(a, b){
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    });

    return result;
}

function paginate(data, page) {
    var start = (page - 1) * window.friendsPerPage;
    var result = data.slice(start, start + window.friendsPerPage);
    return result;
}

function appendFriends(data, page, keyword) {

    if (page == 1) {
        $('#modal-friends #section-friends').html('');
    }
    $('#box-friend-empty').addClass('hidden');

    if(data.length == 0) {
        if (page == 1) {
            $('#box-friend-empty').removeClass('hidden');
            //alert('Teman tidak ditemukan atau kamu tidak mengizinkan aplikasi ini untuk melihat daftar temanmu.');
        } else {
            alert('Tidak ada lagi teman');
            $('#modal-friends .btn-more').remove();
        }
        return false;
    }

    var items = $('<div class="row items">');



    for(var i=0;i<data.length;i++) {
        var item = $('<div class="col-md-6 col-sm-6 col-xs-12 item"></div>').attr('id', 'friend-item-' + data[i].id);
        var inner = $('<div class="inner"></div>')
        var table = $('<table>');
        var tr = $('<tr>');
        var button = $('<button data-toggle="tooltip" data-placement="top" title="PILIH">')
            .addClass('btn btn-metro btn-selection btn-select-friend')
            .data('id', data[i].id)
            .data('name', data[i].name)
            .data('avatar', data[i].picture.data.url);

        if(window.friendsSelected[data[i].id] === true) {
            button.addClass('selected');
        }

        if(window.usedFriends.indexOf(data[i].name) != -1) {
            button.addClass('used');
            item.addClass('used');
            button.attr('title', 'SUDAH PERNAH DIPAKE');
        }

        var avatar = $('<img>').addClass('img-circle img-avatar').attr('src', data[i].picture.data.url);
        var name = $('<h5>').addClass('el').html(data[i].name);

        var left = $('<td>').addClass('action').append(button);
        var center = $('<td>').addClass('avatar').append(avatar);
        var wrapper = $('<div style="width:160px">').append(name);
        var right = $('<td>').addClass('name').append(wrapper);
        tr.append(left).append(center).append(right);
        table.append(tr);
        inner.append(table);
        item.append(inner);
        item.css('opacity', 0);
        items.append(item);
        item.css('opacity', 1);
    }

    $('#modal-friends #section-friends').append(items);
    $('#modal-friends .btn-select-friend').tooltip({container:'body'});

    var scrollHeight = $('#section-friends').prop('scrollHeight') + 'px';
    $('#section-friends').slimScroll({scrollTo : scrollHeight });
}

/*! Copyright (c) 2011 Piotr Rochala (http://rocha.la)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version: 1.3.2
 *
 */
(function(f){jQuery.fn.extend({slimScroll:function(g){var a=f.extend({width:"auto",height:"250px",size:"7px",color:"#000",position:"right",distance:"1px",start:"top",opacity:0.4,alwaysVisible:!1,disableFadeOut:!1,railVisible:!1,railColor:"#333",railOpacity:0.2,railDraggable:!0,railClass:"slimScrollRail",barClass:"slimScrollBar",wrapperClass:"slimScrollDiv",allowPageScroll:!1,wheelStep:20,touchScrollStep:200,borderRadius:"7px",railBorderRadius:"7px"},g);this.each(function(){function u(d){if(r){d=d||
window.event;var c=0;d.wheelDelta&&(c=-d.wheelDelta/120);d.detail&&(c=d.detail/3);f(d.target||d.srcTarget||d.srcElement).closest("."+a.wrapperClass).is(b.parent())&&m(c,!0);d.preventDefault&&!k&&d.preventDefault();k||(d.returnValue=!1)}}function m(d,f,g){k=!1;var e=d,h=b.outerHeight()-c.outerHeight();f&&(e=parseInt(c.css("top"))+d*parseInt(a.wheelStep)/100*c.outerHeight(),e=Math.min(Math.max(e,0),h),e=0<d?Math.ceil(e):Math.floor(e),c.css({top:e+"px"}));l=parseInt(c.css("top"))/(b.outerHeight()-c.outerHeight());
e=l*(b[0].scrollHeight-b.outerHeight());g&&(e=d,d=e/b[0].scrollHeight*b.outerHeight(),d=Math.min(Math.max(d,0),h),c.css({top:d+"px"}));b.scrollTop(e);b.trigger("slimscrolling",~~e);v();p()}function C(){window.addEventListener?(this.addEventListener("DOMMouseScroll",u,!1),this.addEventListener("mousewheel",u,!1)):document.attachEvent("onmousewheel",u)}function w(){s=Math.max(b.outerHeight()/b[0].scrollHeight*b.outerHeight(),D);c.css({height:s+"px"});var a=s==b.outerHeight()?"none":"block";c.css({display:a})}
function v(){w();clearTimeout(A);l==~~l?(k=a.allowPageScroll,B!=l&&b.trigger("slimscroll",0==~~l?"top":"bottom")):k=!1;B=l;s>=b.outerHeight()?k=!0:(c.stop(!0,!0).fadeIn("fast"),a.railVisible&&h.stop(!0,!0).fadeIn("fast"))}function p(){a.alwaysVisible||(A=setTimeout(function(){a.disableFadeOut&&r||x||y||(c.fadeOut("slow"),h.fadeOut("slow"))},1E3))}var r,x,y,A,z,s,l,B,D=30,k=!1,b=f(this);if(b.parent().hasClass(a.wrapperClass)){var n=b.scrollTop(),c=b.parent().find("."+a.barClass),h=b.parent().find("."+
a.railClass);w();if(f.isPlainObject(g)){if("height"in g&&"auto"==g.height){b.parent().css("height","auto");b.css("height","auto");var q=b.parent().parent().height();b.parent().css("height",q);b.css("height",q)}if("scrollTo"in g)n=parseInt(a.scrollTo);else if("scrollBy"in g)n+=parseInt(a.scrollBy);else if("destroy"in g){c.remove();h.remove();b.unwrap();return}m(n,!1,!0)}}else{a.height="auto"==g.height?b.parent().height():g.height;n=f("<div></div>").addClass(a.wrapperClass).css({position:"relative",
overflow:"hidden",width:a.width,height:a.height});b.css({overflow:"hidden",width:a.width,height:a.height});var h=f("<div></div>").addClass(a.railClass).css({width:a.size,height:"100%",position:"absolute",top:0,display:a.alwaysVisible&&a.railVisible?"block":"none","border-radius":a.railBorderRadius,background:a.railColor,opacity:a.railOpacity,zIndex:90}),c=f("<div></div>").addClass(a.barClass).css({background:a.color,width:a.size,position:"absolute",top:0,opacity:a.opacity,display:a.alwaysVisible?
"block":"none","border-radius":a.borderRadius,BorderRadius:a.borderRadius,MozBorderRadius:a.borderRadius,WebkitBorderRadius:a.borderRadius,zIndex:99}),q="right"==a.position?{right:a.distance}:{left:a.distance};h.css(q);c.css(q);b.wrap(n);b.parent().append(c);b.parent().append(h);a.railDraggable&&c.bind("mousedown",function(a){var b=f(document);y=!0;t=parseFloat(c.css("top"));pageY=a.pageY;b.bind("mousemove.slimscroll",function(a){currTop=t+a.pageY-pageY;c.css("top",currTop);m(0,c.position().top,!1)});
b.bind("mouseup.slimscroll",function(a){y=!1;p();b.unbind(".slimscroll")});return!1}).bind("selectstart.slimscroll",function(a){a.stopPropagation();a.preventDefault();return!1});h.hover(function(){v()},function(){p()});c.hover(function(){x=!0},function(){x=!1});b.hover(function(){r=!0;v();p()},function(){r=!1;p()});b.bind("touchstart",function(a,b){a.originalEvent.touches.length&&(z=a.originalEvent.touches[0].pageY)});b.bind("touchmove",function(b){k||b.originalEvent.preventDefault();b.originalEvent.touches.length&&
(m((z-b.originalEvent.touches[0].pageY)/a.touchScrollStep,!0),z=b.originalEvent.touches[0].pageY)});w();"bottom"===a.start?(c.css({top:b.outerHeight()-c.outerHeight()}),m(0,!0)):"top"!==a.start&&(m(f(a.start).position().top,null,!0),a.alwaysVisible||c.hide());C()}});return this}});jQuery.fn.extend({slimscroll:jQuery.fn.slimScroll})})(jQuery);
/*!
 * The Final Countdown for jQuery v2.0.4 (http://hilios.github.io/jQuery.countdown/)
 * Copyright (c) 2014 Edson Hilios
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){"use strict";function b(a){if(a instanceof Date)return a;if(String(a).match(g))return String(a).match(/^[0-9]*$/)&&(a=Number(a)),String(a).match(/\-/)&&(a=String(a).replace(/\-/g,"/")),new Date(a);throw new Error("Couldn't cast `"+a+"` to a date object.")}function c(a){return function(b){var c=b.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);if(c)for(var e=0,f=c.length;f>e;++e){var g=c[e].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/),i=new RegExp(g[0]),j=g[1]||"",k=g[3]||"",l=null;g=g[2],h.hasOwnProperty(g)&&(l=h[g],l=Number(a[l])),null!==l&&("!"===j&&(l=d(k,l)),""===j&&10>l&&(l="0"+l.toString()),b=b.replace(i,l.toString()))}return b=b.replace(/%%/,"%")}}function d(a,b){var c="s",d="";return a&&(a=a.replace(/(:|;|\s)/gi,"").split(/\,/),1===a.length?c=a[0]:(d=a[0],c=a[1])),1===Math.abs(b)?d:c}var e=100,f=[],g=[];g.push(/^[0-9]*$/.source),g.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source),g.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source),g=new RegExp(g.join("|"));var h={Y:"years",m:"months",w:"weeks",d:"days",D:"totalDays",H:"hours",M:"minutes",S:"seconds"},i=function(b,c,d){this.el=b,this.$el=a(b),this.interval=null,this.offset={},this.instanceNumber=f.length,f.push(this),this.$el.data("countdown-instance",this.instanceNumber),d&&(this.$el.on("update.countdown",d),this.$el.on("stoped.countdown",d),this.$el.on("finish.countdown",d)),this.setFinalDate(c),this.start()};a.extend(i.prototype,{start:function(){null!==this.interval&&clearInterval(this.interval);var a=this;this.update(),this.interval=setInterval(function(){a.update.call(a)},e)},stop:function(){clearInterval(this.interval),this.interval=null,this.dispatchEvent("stoped")},pause:function(){this.stop.call(this)},resume:function(){this.start.call(this)},remove:function(){this.stop(),f[this.instanceNumber]=null,delete this.$el.data().countdownInstance},setFinalDate:function(a){this.finalDate=b(a)},update:function(){return 0===this.$el.closest("html").length?void this.remove():(this.totalSecsLeft=this.finalDate.getTime()-(new Date).getTime(),this.totalSecsLeft=Math.ceil(this.totalSecsLeft/1e3),this.totalSecsLeft=this.totalSecsLeft<0?0:this.totalSecsLeft,this.offset={seconds:this.totalSecsLeft%60,minutes:Math.floor(this.totalSecsLeft/60)%60,hours:Math.floor(this.totalSecsLeft/60/60)%24,days:Math.floor(this.totalSecsLeft/60/60/24)%7,totalDays:Math.floor(this.totalSecsLeft/60/60/24),weeks:Math.floor(this.totalSecsLeft/60/60/24/7),months:Math.floor(this.totalSecsLeft/60/60/24/30),years:Math.floor(this.totalSecsLeft/60/60/24/365)},void(0===this.totalSecsLeft?(this.stop(),this.dispatchEvent("finish")):this.dispatchEvent("update")))},dispatchEvent:function(b){var d=a.Event(b+".countdown");d.finalDate=this.finalDate,d.offset=a.extend({},this.offset),d.strftime=c(this.offset),this.$el.trigger(d)}}),a.fn.countdown=function(){var b=Array.prototype.slice.call(arguments,0);return this.each(function(){var c=a(this).data("countdown-instance");if(void 0!==c){var d=f[c],e=b[0];i.prototype.hasOwnProperty(e)?d[e].apply(d,b.slice(1)):null===String(e).match(/^[$A-Z_][0-9A-Z_$]*$/i)?(d.setFinalDate.call(d,e),d.start()):a.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi,e))}else new i(this,b[0],b[1])})}});
/*! bootstrap-progressbar v0.8.3 | Copyright (c) 2012-2014 Stephan Groß | MIT license | http://www.minddust.com */
!function(t){"use strict";var e=function(n,a){this.$element=t(n),this.options=t.extend({},e.defaults,a)};e.defaults={transition_delay:300,refresh_speed:50,display_text:"none",use_percentage:!0,percent_format:function(t){return t+"%"},amount_format:function(t,e){return t+" / "+e},update:t.noop,done:t.noop,fail:t.noop},e.prototype.transition=function(){var n=this.$element,a=n.parent(),s=this.$back_text,r=this.$front_text,i=this.options,o=parseInt(n.attr("data-transitiongoal")),h=parseInt(n.attr("aria-valuemin"))||0,d=parseInt(n.attr("aria-valuemax"))||100,f=a.hasClass("vertical"),p=i.update&&"function"==typeof i.update?i.update:e.defaults.update,u=i.done&&"function"==typeof i.done?i.done:e.defaults.done,c=i.fail&&"function"==typeof i.fail?i.fail:e.defaults.fail;if(isNaN(o))return void c("data-transitiongoal not set");var l=Math.round(100*(o-h)/(d-h));if("center"===i.display_text&&!s&&!r){this.$back_text=s=t("<span>").addClass("progressbar-back-text").prependTo(a),this.$front_text=r=t("<span>").addClass("progressbar-front-text").prependTo(n);var g;f?(g=a.css("height"),s.css({height:g,"line-height":g}),r.css({height:g,"line-height":g}),t(window).resize(function(){g=a.css("height"),s.css({height:g,"line-height":g}),r.css({height:g,"line-height":g})})):(g=a.css("width"),r.css({width:g}),t(window).resize(function(){g=a.css("width"),r.css({width:g})}))}setTimeout(function(){var t,e,c,g,_;f?n.css("height",l+"%"):n.css("width",l+"%");var x=setInterval(function(){f?(c=n.height(),g=a.height()):(c=n.width(),g=a.width()),t=Math.round(100*c/g),e=Math.round(h+c/g*(d-h)),t>=l&&(t=l,e=o,u(n),clearInterval(x)),"none"!==i.display_text&&(_=i.use_percentage?i.percent_format(t):i.amount_format(e,d,h),"fill"===i.display_text?n.text(_):"center"===i.display_text&&(s.text(_),r.text(_))),n.attr("aria-valuenow",e),p(t,n)},i.refresh_speed)},i.transition_delay)};var n=t.fn.progressbar;t.fn.progressbar=function(n){return this.each(function(){var a=t(this),s=a.data("bs.progressbar"),r="object"==typeof n&&n;s||a.data("bs.progressbar",s=new e(this,r)),s.transition()})},t.fn.progressbar.Constructor=e,t.fn.progressbar.noConflict=function(){return t.fn.progressbar=n,this}}(window.jQuery);