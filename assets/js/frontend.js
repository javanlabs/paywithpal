var friends = new Array;
var friendsFiltered = new Array;
var friendsPerPage = 6;
var friendsCurrentPage = 1;
var friendsAjax = null;
var friendsSearchTimer;
var friendsLoaded = false;
var isLogin = false;
var redeemAjax = null;
var friendsSelected = new Array;
var isTokenExceeded = false;
var currentKeyword = '';

var loadVoucherAjax = null;
var voucherCurrentPage = 1;

$(function(){

    // google analytics tracking
    $(document).on('click', '.btn-logo-metro', function() {
        ga('send', 'event', 'Logo', 'Click', 'Click');
    });
    $(document).on('click', '.btn-print-voucher', function() {
        ga('send', 'event', 'Print', 'Click', 'Voucher');
    });
    $(document).on('click', '.btn-close-voucher', function() {
        ga('send', 'event', 'Button', 'Close', 'Print');
    });

    $('.scrollable').each(function(idx, elm){
        $(elm).slimScroll({
            height: $(elm).data('scroll-height'),
            railVisible: true,
            alwaysVisible: true,
            allowPageScroll: true
        });
    });


    window.friendsSearchTimer = window.setInterval(function(){

        if(!window.friendsLoaded) {
            return false;
        }

        var form = $('#form-search-friends');
        var keyword = form.find('.keyword').val();
        if(keyword != window.currentKeyword) {
           window.currentKeyword = keyword;
           window.friendsCurrentPage = 1;
           form.addClass('loading');
           form.find('.btn-more').button('loading');
            window.friendsFiltered = paginate(filterByKeyword(window.friends, keyword), 1);
            appendFriends(window.friendsFiltered, 1, keyword);
       }
    },800);


    $("#modal-friends").on('submit', '.form-search-friends', function (e) {
        e.preventDefault();
    });

    $('#modal-friends').on('click', '.btn-select-friend', function(e){
        e.preventDefault();

        if(e.handled !== true)
        {
            // put your payload here.
            // this next line *must* be within this if statement
            e.handled = true;

            var btn = $(e.currentTarget);
            var modal = $(e.delegateTarget);
            var id = btn.data('id');

            var selectedItem = modal.data('selected');

            if(btn.hasClass('used')) {
                return false;
            }

            if(btn.hasClass('selected')) {
                $('#selected-friend-' + btn.data('selected-index') + ' .btn-remove-friend').trigger('click');
                return false;
            }

            if(selectedItem > 2) {
                return false;
            }

            var used = $('#selected-friend-' + selectedItem + ' .input-id').val() != '';
            while (used && selectedItem < 2) {
                selectedItem++;
                used = $('#selected-friend-' + selectedItem + ' .input-id').val() != '';
            }

            modal.data('selected', selectedItem + 1);
            btn.addClass('selected');
            btn.data('selected-index', selectedItem);

            var avatar = btn.data('avatar');
            var name = btn.data('name');

            $('#selected-friend-' + selectedItem).find('.avatar').attr('src', avatar);
            $('#selected-friend-' + selectedItem).find('.name').html(name);

            $('#selected-friend-' + selectedItem).find('.input-id').val(id);
            $('#selected-friend-' + selectedItem).find('.input-name').val(name);
            $('#selected-friend-' + selectedItem).find('.input-avatar').val(avatar);
            $('#selected-friend-' + selectedItem).find('.btn-remove-friend').data('friend-id', id);

            window.friendsSelected[id] = true;
        }

        return false;
    });

    $('#modal-friends').on('click', '.btn-remove-friend', function(e){
        e.preventDefault();

        var btn = $(e.currentTarget);
        var modal = $(e.delegateTarget);
        var id = btn.data('id');

        modal.data('selected', Math.min(modal.data('selected'), id));

        removeSelectedFriend(id);
    });

    function removeSelectedFriend(selectedItem) {
        var id = $('#selected-friend-' + selectedItem).find('.input-id').val();
        $('#friend-item-' + id).find('.btn-select-friend').removeClass('selected');

        $('#selected-friend-' + selectedItem).find('.avatar').attr('src', 'data:image/gif;base64,R0lGODdhZABkAIAAAMzMzJaWliwAAAAAZABkAAACc4SPqcvtD6OctNqLs968+w+G4kiW5omm6sq27gvH8kzX9o3n+s73/g8MCofEovGITCqXzKbzCY1Kp9Sq9YrNarfcrvcLDovH5LL5jE6r1+y2+w2Py+f0uv2Oz+v3/L7/DxgoOEhYaHiImKi4yNjo+AhpWAAAOw==');
        $('#selected-friend-' + selectedItem).find('.name').html('Belum Dipilih');
        $('#selected-friend-' + selectedItem).find('.input-id').val('');
        $('#selected-friend-' + selectedItem).find('.input-name').val('');
        $('#selected-friend-' + selectedItem).find('.input-avatar').val('');
        $('#selected-friend-' + selectedItem).find('.btn-remove-friend').data('friend-id', null);

        window.friendsSelected[id] = false;
    }

    $('#modal-friends').on('submit', '.form-selected-friends', function(e){
        var form = $(e.currentTarget);
        var modal = $(e.delegateTarget);

        if(modal.data('selected') <= 0){
            e.preventDefault();
            alert('Silakan pilih minimal 1 teman!');
        }
    });

    $('#modal-friends').on('click', '.btn-more', function(e){

        ga('send', 'event', 'LoadParticipant', 'Click', 'Click');

        if(window.friendsSearching) {
            return false;
        }
        var btn = $(e.currentTarget);
        var modal = $(e.delegateTarget);

        friendsCurrentPage++;
        btn.button('loading').addClass('loading');
        showFriends(friendsCurrentPage, modal.find('.keyword').val(), function(){
            btn.button('reset').removeClass('loading');
        });
    });

    // VOUCHER REDEEM
    $('#page-voucher-pick').on('submit', '.form-redeem', function(e){
        var form = $(e.currentTarget);
        var btn = form.find('button');

        e.preventDefault();

        if(redeemAjax){
            alert('Maaf, Anda hanya bisa memilih satu voucher.');
            return false;
        }
        $('#page-voucher-pick .btn-redeem').attr('disabled', 'disabled').addClass('disabled');
        btn.button('loading');
        redeemAjax = $.ajax({
            url: form.attr('action'),
            type: 'post',
            dataType: 'json',
            data: form.serialize()
        }).done(function(response) {
            if(response.status == 1){
                $('#modal-voucher .img-voucher').attr('src', response.voucher_url);
                $('#modal-voucher').modal('show');
                $('#page-voucher-pick .voucher-list').hide();
                $('#page-voucher-pick .after-pick').removeClass('hidden');

                ga('send', 'pageview', '/vp/print-voucher');

            }
        })
        .fail(function() {
            // fail or aborted
        })
        .always(function() {
            redeemAjax = null;
            btn.button('reset');
        });

    });

    $('#page-voucher').on('click', '.btn-more', function(e){

        e.preventDefault();

        var btn  = $(e.currentTarget);
        var page = $(e.delegateTarget);
        var keyword = page.find('[name=keyword]').val();

        btn.button('loading').addClass('loading');

        loadVoucherAjax = $.ajax({
            url: btn.attr('href') + '?page=' + (voucherCurrentPage + 1),
            type: 'get',
            dataType: 'json',
            data: {keyword: keyword}
        }).done(function(response) {
            if(response.status == 1){
                if(response.html != ''){
                    btn.button('reset').removeClass('loading');
                    voucherCurrentPage++;
                    page.find('.voucher-list .items').append(response.html);
                }else{
                    btn.html('No More Data').remove();
                    alert('Tidak ada lagi data');
                }
            }
        })
        .fail(function() {
            // fail or aborted
        })
        .always(function() {
            loadVoucherAjax = null;
        });

    });

});

function showModal(fbResponse, url){

    ga('send', 'pageview', '/vp/choose-friend');

    $('#modal-friends').modal('show');
    $('#modal-friends .btn-more').button('loading').addClass('loading');

    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: fbResponse.authResponse,
    }).done(function(response) {
        if(response.status == 1){
            window.isLogin = true;
            window.usedFriends = response.used_friends;
            if(response.token_limit_exceeded){
                $('#modal-friends').modal('hide');
                $('#modal-token-exceeded').modal('show');
                window.isTokenExceeded = true;
                $('#countdown').countdown(response.next_date, function (event) {
                    $(this).html(event.strftime('<span>%-H</span> jam <span>%-M</span> menit <span>%-S</span> detik lagi'));
                }).on('finish', function(){
                    window.location.reload();
                });;

                $('#modal-token-exceeded').on('hidden.bs.modal', function(){
                    window.location.reload();
                });

            }else{
                showFriends(1, '', function(){
                    $('#modal-friends .btn-more').button('reset').removeClass('loading');
                });
            }
        }else{
            $('#modal-friends').modal('hide');
            alert('Oops, gagal login');
        }
    })
    .fail(function() {
        alert( "error" );
    })
    .always(function() {

    });
}

function showFriends(page, keyword, onDone){

    if(friends.length == 0) {
        FB.api("me/taggable_friends", handleFriends);

        function handleFriends(response){
            if (response && response.data) {
                for(var i=0; i<response.data.length; i++){
                    window.friends.push(response.data[i]);
                }

                if (response.paging.next){
                    FB.api(response.paging.next, handleFriends);
                } else {

                    window.friendsFiltered = paginate(filterByKeyword(window.friends, keyword), page);
                    appendFriends(window.friendsFiltered, page, keyword);
                    if($.isFunction(onDone)){
                        onDone();
                    }
                    window.friendsLoaded = true;
                }

            }
        }
    } else {

        window.friendsFiltered = paginate(filterByKeyword(window.friends, keyword), page);
        appendFriends(window.friendsFiltered, page, keyword);
        if($.isFunction(onDone)){
            onDone();
        }
    }


    //friendsAjax = $.ajax({
    //    url: $('#modal-friends').data('url'),
    //    type: 'post',
    //    dataType: 'json',
    //    data: {page:page, keyword:keyword}
    //}).done(function(response) {
    //
    //    if(page == 1){
    //        $('#modal-friends #section-friends').html('');
    //    }
    //
    //    if(response.html == ""){
    //        if (page == 1){
    //            alert('Teman tidak ditemukan atau kamu tidak mengizinkan aplikasi ini untuk melihat daftar temanmu.');
    //        } else {
    //            alert('Tidak ada lagi teman');
    //        }
    //    }else{
    //        $('#modal-friends #section-friends').append(response.html);
    //        $('#modal-friends .btn-select-friend').tooltip({container:'body'});
    //
    //        var scrollHeight = $('#section-friends').prop('scrollHeight') + 'px';
    //        $('#section-friends').slimScroll({scrollTo : scrollHeight });
    //
    //    }
    //
    //    if($.isFunction(onDone)){
    //        onDone();
    //    }
    //
    //})
    //.fail(function() {
    //    // fail or aborted
    //})
    //.always(function() {
    //    friendsAjax = null;
    //});
}

function loadSelection(callback) {
    $.ajax({
        url: $('#modal-loading-selection').data('url'),
        type: 'post',
        dataType: 'json',
    }).done(function (response) {
        if(response.status == 1) {
            $('#page-voucher-pick .voucher-list').append(response.html);
        } else {
            alert(response.message);
        }

        if ($.isFunction(callback)) {
            callback(response);
        }

    })
    .fail(function () {
        // fail or aborted
    })
    .always(function () {
    });
}

function filterByKeyword(data, keyword) {

    var result = new Array;
    var keyword = keyword.toLowerCase();

    if(keyword != "") {
        for(var i=0;i<data.length;i++) {
            var name = data[i].name.toLowerCase();
            if(name.indexOf(keyword) != -1) {
                result.push(data[i]);
            }
        }
    } else {
        result = data;
    }

    result.sort(function(a, b){
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    });

    return result;
}

function paginate(data, page) {
    var start = (page - 1) * window.friendsPerPage;
    var result = data.slice(start, start + window.friendsPerPage);
    return result;
}

function appendFriends(data, page, keyword) {

    if (page == 1) {
        $('#modal-friends #section-friends').html('');
    }
    $('#box-friend-empty').addClass('hidden');

    if(data.length == 0) {
        if (page == 1) {
            $('#box-friend-empty').removeClass('hidden');
            //alert('Teman tidak ditemukan atau kamu tidak mengizinkan aplikasi ini untuk melihat daftar temanmu.');
        } else {
            alert('Tidak ada lagi teman');
            $('#modal-friends .btn-more').remove();
        }
        return false;
    }

    var items = $('<div class="row items">');



    for(var i=0;i<data.length;i++) {
        var item = $('<div class="col-md-6 col-sm-6 col-xs-12 item"></div>').attr('id', 'friend-item-' + data[i].id);
        var inner = $('<div class="inner"></div>')
        var table = $('<table>');
        var tr = $('<tr>');
        var button = $('<button data-toggle="tooltip" data-placement="top" title="PILIH">')
            .addClass('btn btn-metro btn-selection btn-select-friend')
            .data('id', data[i].id)
            .data('name', data[i].name)
            .data('avatar', data[i].picture.data.url);

        if(window.friendsSelected[data[i].id] === true) {
            button.addClass('selected');
        }

        if(window.usedFriends.indexOf(data[i].name) != -1) {
            button.addClass('used');
            item.addClass('used');
            button.attr('title', 'SUDAH PERNAH DIPAKE');
        }

        var avatar = $('<img>').addClass('img-circle img-avatar').attr('src', data[i].picture.data.url);
        var name = $('<h5>').addClass('el').html(data[i].name);

        var left = $('<td>').addClass('action').append(button);
        var center = $('<td>').addClass('avatar').append(avatar);
        var wrapper = $('<div style="width:160px">').append(name);
        var right = $('<td>').addClass('name').append(wrapper);
        tr.append(left).append(center).append(right);
        table.append(tr);
        inner.append(table);
        item.append(inner);
        item.css('opacity', 0);
        items.append(item);
        item.css('opacity', 1);
    }

    $('#modal-friends #section-friends').append(items);
    $('#modal-friends .btn-select-friend').tooltip({container:'body'});

    var scrollHeight = $('#section-friends').prop('scrollHeight') + 'px';
    $('#section-friends').slimScroll({scrollTo : scrollHeight });
}
